function showCC() {
    document.getElementById("acimpro_cc_type_set").disabled = false;
    document.getElementById("acimpro_cc_type_set").className = "required-entry validate-cc-type-select";

    document.getElementById('payment_form_set_acimpro').style.display = 'block';
    document.getElementById('select_payment_form_acimpro').style.display = 'none';
    document.getElementById("acimpro_cc_type").className = "disabled";
    document.getElementById("acimpro_cc_number").className = "disabled";
    document.getElementById("acimpro_expiration").className = "disabled";
    document.getElementById("acimpro_expiration_yr").className = "disabled";
    document.getElementById("acimpro_cc_cid").className = "disabled";
    document.getElementById("acimpro_cc_type").disabled = true;
    document.getElementById("acimpro_cc_number").disabled = true;
    document.getElementById("acimpro_expiration").disabled = true;
    document.getElementById("acimpro_expiration_yr").disabled = true;
    document.getElementById("acimpro_cc_cid").disabled = true;
    document.getElementById("placecard").disabled = true;
    }
function showAlreadyPaymethod() {
    document.getElementById("acimpro_cc_type_set").disabled = true;
    document.getElementById("acimpro_cc_type_set").className = "";

    document.getElementById('select_payment_form_acimpro').style.display = 'block';
    document.getElementById('payment_form_set_acimpro').style.display = 'none';
    document.getElementById("acimpro_cc_type").className = "required-entry validate-cc-type-select";
    document.getElementById("acimpro_cc_number").className = "input-text validate-cc-number validate-cc-type";
    document.getElementById("acimpro_expiration").className = "month validate-cc-exp required-entry";
    document.getElementById("acimpro_expiration_yr").className = "year required-entry";
    document.getElementById("acimpro_cc_cid").className = "input-text cvv required-entry validate-cc-cvn";
    document.getElementById("acimpro_cc_type").disabled = false;
    document.getElementById("acimpro_cc_number").disabled = false;
    document.getElementById("acimpro_expiration").disabled = false;
    document.getElementById("acimpro_expiration_yr").disabled = false;
    document.getElementById("acimpro_cc_cid").disabled = false;
    document.getElementById("placecard").disabled = false;
    }