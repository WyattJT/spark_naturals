
    function addSalesOrderViewInfoRow(label,value,rowId, extraClass) {
        var infoBox = getSalesOrderViewInfoBox();
        if (infoBox === false) {
            return false;
        }

        (function ($){
            if (arguments.length <3) {
                rowId = 'info_row_'+(Math.random()).toString(36).substring(2)+(Math.random()).toString(36).substring(2);
            }
            if (arguments.length < 4) {
                extraClass = '';
            }

            var row = $('<tr />').attr('id',rowId);

            var labelCell = $('<td />').attr('class','label');
            var labelElm = $('<label />').html(label);
            labelCell.html(labelElm);

            var valueCell = $('<td />').attr('class','value '+extraClass);
            var valueElm = $('<strong />').html(value);

            valueCell.prepend(valueElm);

            row.prepend(labelCell);
            row.append(valueCell);

            infoBox.find('table tbody').append(row);
        })(jQuery);

        return true;
    }

    function getSalesOrderViewInfoBox() {
        var elm = jQuery('#sales_order_view_tabs_order_info_content');
        if (elm.length) {
            elm = elm.children(':first-child');
            if (elm.length) {
                elm = elm.children('div.box-left:first');
            }
        }

        if (elm.length) {
            return elm;
        } else {
            return false;
        }
    }