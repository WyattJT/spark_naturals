<?php
class Aumd_Morecc_Block_Profiles extends Mage_Core_Block_Template
{
	
	 public function __construct()
    { 
        parent::__construct(); 
        $this->setTemplate('morecc/profiles.phtml');
 
        $profiles = Mage::getResourceModel('morecc/morecc_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('email', Mage::getSingleton('customer/session')->getCustomer()->getEmail()) 
			->addFieldToFilter('number', array('neq' => "")) 
            ->setOrder('created_time', 'desc')
        ; 
 
        $this->setProfiles($profiles);

        Mage::app()->getFrontController()->getAction()->getLayout()->getBlock('root')->setHeaderTitle(Mage::helper('morecc')->__('Saved Cards'));
    }

    protected function _prepareLayout()
    {
		$this->getLayout()->getBlock('head')->setTitle('View Cards');
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'morecc.profiles.history.pager')
            ->setCollection($this->getProfiles());
        $this->setChild('pager', $pager);
        $this->getProfiles()->load();
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
	 
     public function getMorecc()     
     { 
        if (!$this->hasData('morecc')) {
            $this->setData('morecc', Mage::registry('morecc'));
        }
        return $this->getData('morecc');
        
    } 
}