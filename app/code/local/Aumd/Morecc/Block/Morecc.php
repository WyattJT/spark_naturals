<?php 
class Aumd_Morecc_Block_Morecc extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		$this->getLayout()->getBlock('head')->setTitle('Create My Profile');
		return parent::_prepareLayout();
    }
    
     public function getMorecc()     
     { 
        if (!$this->hasData('morecc')) {
            $this->setData('morecc', Mage::registry('morecc'));
        }
        return $this->getData('morecc');
        
    }
	
	public function getDefaultCountryId()
	{
		return 'US';
	}
	
	public function getRegionCollection()
    {
        if (!$this->_regionCollection) {
            $this->_regionCollection = Mage::getModel('directory/region')->getResourceCollection()
                ->addCountryFilter($this->getDefaultCountryId())
                ->load();
        }
        return $this->_regionCollection;
    }
	
	public function getCountryCollection()
    {
        if (!$this->_countryCollection) {
            $this->_countryCollection = Mage::getSingleton('directory/country')->getResourceCollection()
                ->loadByStore();
        }
        return $this->_countryCollection;
    }
	
	 public function getCountryOptions()
    {
        $options    = false;
        $useCache   = Mage::app()->useCache('config');
        if ($useCache) {
            $cacheId    = 'DIRECTORY_COUNTRY_SELECT_STORE_' . Mage::app()->getStore()->getCode();
            $cacheTags  = array('config');
            if ($optionsCache = Mage::app()->loadCache($cacheId)) {
                $options = unserialize($optionsCache);
            }
        }

        if ($options == false) {
            $options = $this->getCountryCollection()->toOptionArray();
            if ($useCache) {
                Mage::app()->saveCache(serialize($options), $cacheId, $cacheTags);
            }
        }
        return $options;
    }
	
	public function getCountryHtmlSelect()
	{
		$type = 'bill';
		 
        $countryId = Mage::helper('core')->getDefaultCountry();
		
	   $select = $this->getLayout()->createBlock('core/html_select')
        ->setName($type.'[country_id]')
        ->setId($type.':country_id')
        ->setTitle(Mage::helper('checkout')->__('Country'))
        ->setClass('validate-select')
        ->setValue($countryId)
        ->setOptions($this->getCountryOptions());
		
		return $select->getHtml();
	}
	
}