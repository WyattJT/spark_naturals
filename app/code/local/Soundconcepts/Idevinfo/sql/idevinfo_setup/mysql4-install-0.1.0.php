<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('idevinfo')};
CREATE TABLE {$this->getTable('idevinfo')} (
  `idevinfo_id` int(11) unsigned NOT NULL auto_increment,
  `mage_customer` varchar(255) NULL,
  `idev_affiliate_id` varchar(255) NULL,
  `referred_by` varchar(255) NULL,
  `coupon_code` varchar(255) NULL,
  PRIMARY KEY (`idevinfo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 