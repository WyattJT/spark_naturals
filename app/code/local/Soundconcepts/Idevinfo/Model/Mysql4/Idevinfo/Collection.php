<?php

class Soundconcepts_Idevinfo_Model_Mysql4_Idevinfo_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('idevinfo/idevinfo');
    }
}