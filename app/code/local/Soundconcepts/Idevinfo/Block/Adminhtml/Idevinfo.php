<?php
class Soundconcepts_Idevinfo_Block_Adminhtml_Idevinfo extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_idevinfo';
    $this->_blockGroup = 'idevinfo';
    $this->_headerText = Mage::helper('idevinfo')->__('View / Edit Customer Affiliate Information');
    $this->_addButtonLabel = Mage::helper('idevinfo')->__('Add Entry');
    parent::__construct();
  }
}