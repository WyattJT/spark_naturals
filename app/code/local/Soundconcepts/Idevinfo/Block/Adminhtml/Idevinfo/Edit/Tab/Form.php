<?php

class Soundconcepts_Idevinfo_Block_Adminhtml_Idevinfo_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('idevinfo_form', array('legend'=>Mage::helper('idevinfo')->__('Item information')));
     
      $fieldset->addField('mage_customer', 'text', array(
          'label'     => Mage::helper('idevinfo')->__('Customer ID'),
          'required'  => false,
          'name'      => 'mage_customer',
      ));
     
      $fieldset->addField('idev_affiliate_id', 'text', array(
          'label'     => Mage::helper('idevinfo')->__('Affiliate ID'),
          'required'  => false,
          'name'      => 'idev_affiliate_id',
      ));
     
      $fieldset->addField('referred_by', 'text', array(
          'label'     => Mage::helper('idevinfo')->__('Referred By'),
          'required'  => false,
          'name'      => 'referred_by',
      ));
     
      $fieldset->addField('coupon_code', 'text', array(
          'label'     => Mage::helper('idevinfo')->__('Coupon Code'),
          'required'  => false,
          'name'      => 'coupon_code',
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getIdevinfoData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getIdevinfoData());
          Mage::getSingleton('adminhtml/session')->setIdevinfoData(null);
      } elseif ( Mage::registry('idevinfo_data') ) {
          $form->setValues(Mage::registry('idevinfo_data')->getData());
      }
      return parent::_prepareForm();
  }
}