<?php

class Soundconcepts_Idevinfo_Block_Adminhtml_Idevinfo_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'idevinfo';
        $this->_controller = 'adminhtml_idevinfo';
        
        $this->_updateButton('save', 'label', Mage::helper('idevinfo')->__('Save Info'));
        $this->_updateButton('delete', 'label', Mage::helper('idevinfo')->__('Delete Info'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('idevinfo_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'idevinfo_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'idevinfo_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('idevinfo_data') && Mage::registry('idevinfo_data')->getId() ) {
            return Mage::helper('idevinfo')->__("Edit Info '%s'", $this->htmlEscape(Mage::registry('idevinfo_data')->getTitle()));
        } else {
            return Mage::helper('idevinfo')->__('Add Customer / Affiliate Info');
        }
    }
}