<?php

class Soundconcepts_Idevinfo_Block_Adminhtml_Idevinfo_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('idevinfo_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('idevinfo')->__('Edit Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('idevinfo')->__('Edit Information'),
          'title'     => Mage::helper('idevinfo')->__('Edit Information'),
          'content'   => $this->getLayout()->createBlock('idevinfo/adminhtml_idevinfo_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}