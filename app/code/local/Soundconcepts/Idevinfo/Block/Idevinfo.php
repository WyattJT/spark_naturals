<?php
class Soundconcepts_Idevinfo_Block_Idevinfo extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getIdevinfo()     
     { 
        if (!$this->hasData('idevinfo')) {
            $this->setData('idevinfo', Mage::registry('idevinfo'));
        }
        return $this->getData('idevinfo');
        
    }
}