<?php

/**
 * Product:       Xtento_ProductExport (1.8.2)
 * ID:            /6mH2gvGXgpi829LJqtG51mLAF7Bbbfhc0UbRb0kLDo=
 * Packaged:      2016-11-03T18:44:26+00:00
 * Last Modified: 2016-09-07T18:36:27+02:00
 * File:          app/code/local/Xtento/ProductExport/Model/Export/Data/Review/General.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_ProductExport_Model_Export_Data_Review_General extends Xtento_ProductExport_Model_Export_Data_Product_General
{
    public function getConfiguration()
    {
        return array(
            'name' => 'General review information',
            'category' => 'Review',
            'description' => 'Export extended review information.',
            'enabled' => true,
            'apply_to' => array(Xtento_ProductExport_Model_Export::ENTITY_REVIEW),
        );
    }

    public function getExportData($entityType, $collectionItem)
    {
        // Set return array
        $returnArray = array();
        $this->_writeArray = & $returnArray; // Write directly on category level
        // Fetch fields to export
        $review = $collectionItem->getReview();

        // Timestamps of creation/update
        if ($this->fieldLoadingRequired('created_at_timestamp')) $this->writeValue('created_at_timestamp', Mage::helper('xtento_productexport/date')->convertDateToStoreTimestamp($review->getCreatedAt()));

        // Which line is this?
        $this->writeValue('line_number', $collectionItem->_currItemNo);
        $this->writeValue('count', $collectionItem->_collectionSize);

        // Export information
        $this->writeValue('export_id', (Mage::registry('product_export_log')) ? Mage::registry('product_export_log')->getId() : 0);

        foreach ($review->getData() as $key => $value) {
            if ($key == 'entity_id') {
                continue;
            }
            if (!$this->fieldLoadingRequired($key)) {
                continue;
            }
            $this->writeValue($key, $value);
        }

        // Add rating
        $voteValues = array();
        foreach ($review->getRatingVotes() as $vote) {
            $voteValues[] = $vote->getValue();
        }
        $averageRating = 0;
        if (count($voteValues) > 0) {
            $averageRating = array_sum($voteValues) / count($voteValues);
        }
        $this->writeValue('product_rating', $averageRating);

        // Review link
        $reviewLink = Mage::getUrl('review/product/view', array('id' => $review->getReviewId(), '_store' => $this->getStoreId()));
        $this->writeValue('review_link', $reviewLink);

        $originalWriteArray = & $this->_writeArray;
        // Add product information
        $productId = $review->getEntityPkValue();
        if ($productId > 0) {
            $product = Mage::getModel('catalog/product')->load($productId);
            if ($product->getId()) {
                if ($this->getStoreId()) {
                    $product->setStoreId($this->getStoreId());
                }
                $this->_writeArray = & $returnArray['product'];
                $this->_exportProductData($product, $this->_writeArray);
                $this->writeValue('entity_id', $product->getId());
                $this->_writeArray = & $originalWriteArray;
            }
        }

        // Done
        return $returnArray;
    }
}