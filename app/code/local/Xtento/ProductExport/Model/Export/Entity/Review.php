<?php

/**
 * Product:       Xtento_ProductExport (1.8.2)
 * ID:            /6mH2gvGXgpi829LJqtG51mLAF7Bbbfhc0UbRb0kLDo=
 * Packaged:      2016-11-03T18:44:26+00:00
 * Last Modified: 2016-09-06T22:44:13+02:00
 * File:          app/code/local/Xtento/ProductExport/Model/Export/Entity/Review.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_ProductExport_Model_Export_Entity_Review extends Xtento_ProductExport_Model_Export_Entity_Abstract
{
    protected $_entityType = Xtento_ProductExport_Model_Export::ENTITY_REVIEW;

    protected function _construct()
    {
        $collection = Mage::getModel('review/review')->getResourceCollection()
            ->addReviewsTotalCount();

        $this->_collection = $collection;
        parent::_construct();
    }

    public function runExport()
    {
        if ($this->getProfile()) {
            if ($this->getProfile()->getStoreIds()) {
                $this->_collection->addStoreFilter($this->getProfile()->getStoreIds());
            }
        }
        $this->_collection->addRateVotes();
        return $this->_runExport();
    }

    public function setCollectionFilters($filters)
    {
        if (is_array($filters)) {
            foreach ($filters as $filter) {
                foreach ($filter as $attribute => $filterArray) {
                    $this->_collection->addFieldToFilter($attribute, $filterArray);
                }
            }
        }
        return $this->_collection;
    }
}