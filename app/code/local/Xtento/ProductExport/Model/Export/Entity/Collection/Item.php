<?php

/**
 * Product:       Xtento_ProductExport (1.8.2)
 * ID:            /6mH2gvGXgpi829LJqtG51mLAF7Bbbfhc0UbRb0kLDo=
 * Packaged:      2016-11-03T18:44:26+00:00
 * Last Modified: 2016-09-06T22:35:59+02:00
 * File:          app/code/local/Xtento/ProductExport/Model/Export/Entity/Collection/Item.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_ProductExport_Model_Export_Entity_Collection_Item extends Varien_Object
{
    private $_collectionItem;

    public function __construct($collectionItem, $entityType, $currItemNo, $collectionCount)
    {
        $this->_collectionItem = $collectionItem;
        $this->_collectionSize = $collectionCount;
        $this->_currItemNo = $currItemNo;
        if ($entityType == Xtento_ProductExport_Model_Export::ENTITY_PRODUCT) {
            $this->setProduct($collectionItem);
        }
        if ($entityType == Xtento_ProductExport_Model_Export::ENTITY_REVIEW) {
            $this->setReview($collectionItem);
        }
        if ($entityType == Xtento_ProductExport_Model_Export::ENTITY_CATEGORY) {
            $this->setCategory($collectionItem);
        }
    }

    public function getObject() {
        return $this->_collectionItem;
    }
}