<?php

/**
 * Product:       Xtento_ProductExport (1.8.2)
 * ID:            /6mH2gvGXgpi829LJqtG51mLAF7Bbbfhc0UbRb0kLDo=
 * Packaged:      2016-11-03T18:44:26+00:00
 * Last Modified: 2013-02-10T18:06:03+01:00
 * File:          app/code/local/Xtento/ProductExport/Block/Adminhtml/Tools/Export.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_ProductExport_Block_Adminhtml_Tools_Export extends Mage_Adminhtml_Block_Template
{
    public function getProfiles()
    {
        $profileCollection = Mage::getModel('xtento_productexport/profile')->getCollection();
        $profileCollection->getSelect()->order('name ASC');
        return $profileCollection;
    }

    public function getDestinations()
    {
        $destinationCollection = Mage::getModel('xtento_productexport/destination')->getCollection();
        $destinationCollection->getSelect()->order('name ASC');
        return $destinationCollection;
    }
}