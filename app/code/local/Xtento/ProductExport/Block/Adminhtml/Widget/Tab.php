<?php

/**
 * Product:       Xtento_ProductExport (1.8.2)
 * ID:            /6mH2gvGXgpi829LJqtG51mLAF7Bbbfhc0UbRb0kLDo=
 * Packaged:      2016-11-03T18:44:26+00:00
 * Last Modified: 2013-02-10T18:04:46+01:00
 * File:          app/code/local/Xtento/ProductExport/Block/Adminhtml/Widget/Tab.php
 * Copyright:     Copyright (c) 2016 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_ProductExport_Block_Adminhtml_Widget_Tab extends Mage_Adminhtml_Block_Widget_Form
{
    protected function getFormMessages()
    {
        $formMessages = array();
        return $formMessages;
    }

    protected function _toHtml()
    {
        if ($this->getRequest()->getParam('ajax')) {
            return parent::_toHtml();
        }
        return $this->_getFormMessages() . parent::_toHtml();
    }

    protected function _getFormMessages()
    {
        $html = '<div id="messages"><ul class="messages">';
        foreach ($this->getFormMessages() as $formMessage) {
            $html .= '<li class="' . $formMessage['type'] . '-msg"><ul><li><span>' . $formMessage['message'] . '</span></li></ul></li>';
        }
        $html .= '</ul></div>';
        return $html;
    }
}