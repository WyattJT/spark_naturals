<?php
/**
 * @category    Levementum
 * @package     Wholesale
 * @file        ${FILE}
 * @author      mgontijo@levementum.com
 * @date        3/4/15 6:23 PM
 */
class Levementum_Wholesale_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * Recording after order save a flag of first order min in customer
     * in order to use it later in business rules
     *
     * @param Varien_Event_Observer $observer
     */
    public function levementumWholsaleSalesOrderSaveAfter(Varien_Event_Observer $observer)
    {
        $order    = $observer->getEvent()->getOrder();
        $customer = Mage::getModel('customer/customer')
                ->load($order->getCustomerId());

        if (
               $order->getCustomerGroupId() == Levementum_Wholesale_Helper_Data::CUSTOMER_GROUP_WHOLESALE
            && !$customer->getFirstWholesaleOrderDate()
            && $order->getSubtotal() >= Mage::getStoreConfig('levementum_wholesale/configuration/first_order_min')
        ) {
            $customer
                ->setFirstWholesaleOrderDate($order->getCreatedAt())
                ->save();
        }
    }

    /**
     * In cart, if customer is from a group called "Wholesales" so it has conditions,
     * which needs match or a message will appear to customer
     *
     * @param Varien_Event_Observer $observer
     */
    public function controllerActionPredispatchCheckoutCartIndex(Varien_Event_Observer $observer)
    {
        if ($this->isQuoteMatchingWholesaleRules()) {
            $this->_throwNotMatchingConditions();
        }
    }

    /**
     * In custom routers this call is called.
     * It redirects to cart if quote doesn't match with wholesale rules
     *
     * @param Varien_Event_Observer $observer
     */
    public function redirectToCart(Varien_Event_Observer $observer)
    {
        if ($this->isQuoteMatchingWholesaleRules()) {
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
            Mage::app()->getResponse()->sendResponse();
            exit;
        }
    }

    /**
     * Check if quote is matching wholesale rules
     *
     * @return boolean
     */
    public function isQuoteMatchingWholesaleRules()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        if ($quote->getCustomerGroupId() != Levementum_Wholesale_Helper_Data::CUSTOMER_GROUP_WHOLESALE) {
            return false;
        }

        $customer = Mage::getModel('customer/customer')->load($quote->getCustomerId());

        /**
         * - It hasn't to be populated "first wholesale order date" and smaller than "first_order_min"
         * OR
         * - It has to be populated "first wholesale order date" and smaller than "subsequent_order_min"
         */
        if (
            (
                   !$customer->getFirstWholesaleOrderDate()
                && $quote->getSubtotal() < Mage::getStoreConfig('levementum_wholesale/configuration/first_order_min')
            )
            ||
            (
                   $customer->getFirstWholesaleOrderDate()
                && $quote->getSubtotal() < Mage::getStoreConfig('levementum_wholesale/configuration/subsequent_order_min')
            )
        ) {
            return true;
        }

        return false;
    }

    protected function _throwNotMatchingConditions()
    {
        $helper = Mage::helper('core');

        Mage::getSingleton('checkout/session')->addError(
            sprintf(
                Mage::getStoreConfig('levementum_wholesale/configuration/error_message'),
                $helper->currency(Mage::getStoreConfig('levementum_wholesale/configuration/first_order_min'), true, false),
                $helper->currency(Mage::getStoreConfig('levementum_wholesale/configuration/subsequent_order_min'), true, false)
            )
        );
    }
}
