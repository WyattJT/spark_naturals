<?php
/**
 * @category    Levementum
 * @package     Wholesale
 * @file        ${FILE}
 * @author      mgontijo@levementum.com
 * @date        3/4/15 6:23 PM
 */

/** @var Mage_Customer_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('customer', 'first_wholesale_order_date', array(
    'type'       => 'datetime',
    'label'      => 'First Wholesale Order Date',
    'input'      => 'date',
    'default'    => null,
    'required'   => false,
    'visible'    => true,
    'sort_order' => 110,
    'position'   => 110,
));

$installer->endSetup();
