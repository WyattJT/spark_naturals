<?php
/**
 * @category    Levementum
 * @package     Wholesale
 * @file        ${FILE}
 * @author      mgontijo@levementum.com
 * @date        3/4/15 6:23 PM
 */
class Levementum_Wholesale_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CUSTOMER_GROUP_WHOLESALE = 2;
}
