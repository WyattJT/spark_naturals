<?php

/**
 * jjuleff@levementum.com 2015-09-01
 */
class Levementum_GoogleTagManager_Block_Googletagmanager extends Mage_Core_Block_Template
{
    public function _construct()
    {
        if (!$this->getTemplate()) {
            $this->setTemplate('levementum/googletagmanager.phtml');
        }
    }

    //gets our data from our order item (if applicable) and adds it to the checkout_success google tag manager template.
    public function getDataLayer()
    {
        $return = "''";
        //on checkout page
        if (Mage::app()->getFrontController()->getAction()->getFullActionName() == "checkout_onepage_success") {

            $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
            //have something valid to work with.
            if ($orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                if (!is_object($order)) {
                    Mage::log("Onepage Success Called but Last Order Not available For Tag Manager", null, 'levementum_google_tag_manager.log', true);

                    return $return;
                }

                //using baseurl as affiliate source cause it's better than "Default Store View"
                $baseUrl = Mage::getBaseUrl();
                $baseUrl = explode('//', $baseUrl);
                $baseUrl = array_pop($baseUrl);
                $baseUrl = explode('/', $baseUrl);
                $baseUrl = array_shift($baseUrl);

                //basic dataLayer Datafields
                $return = array();
                $return['transactionId']          = $order->getIncrementId();
                $return['transactionAffiliation'] = $baseUrl;
                $return['transactionTotal']       = $order->getGrandTotal();
                $return['transactionTax']         = $order->getTaxAmount();
                $return['transactionShipping']    = $order->getShippingAmount();
                //Adds the transaction products to the data layer.
                $return['transactionProducts']    = $this->getTransactionProducts($order);
                //collapses the data layer into a string we can feed into a script tag somewhere.
                $return                           = $this->collapseToJs($return, true);
            }
        }

        return $return;
    }

    /** @vars $product Mage_Catalog_Model_Product */
    //gets order items and formats
    public function getTransactionProducts($order)
    {
        $return = array();

        foreach ($order->getAllItems() as $item) {
            $catName     = $this->getCategoryName($item->getProductId());
            $itemDetails = array(
                'sku'      => $item->getSku(),
                'name'     => $item->getName(),
                'category' => $catName,
                'price'    => $item->getPrice(),
                'quantity' => $item->getQtyOrdered()
            );
            $return[]    = $itemDetails;
        }

        return $return;
    }

    //what a mess just to get the category name from the product id.
    public function getCategoryName($productId)
    {
        $product     = Mage::getModel('catalog/product')->load($productId);
        $categoryIds = $product->getCategoryIds();

        if (empty($categoryIds) && $product->getTypeId() == "simple") {
            $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($product->getId());
            if (!$parentIds) {
                $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
            }
            if (count($parentIds)) {
                $parentId    = array_shift($parentIds);
                $categoryIds = Mage::getModel('catalog/product')->load($parentId)->getCategoryIds();
            }
        }
        $catName = false;
        if (is_array($categoryIds) && !empty($categoryIds)) {
            $catId   = array_shift($categoryIds);
            $catName = Mage::getModel('catalog/category')->load($catId)->getName();
        } elseif (is_numeric($categoryIds) && !empty($categoryIds)) {
            $catName = Mage::getModel('catalog/category')->load($categoryIds)->getName();
        }
        if (empty($catName)) {
            $catName = "Cat Not Set";
        }

        return $catName;
    }

    public function collapseToJs($array, $includeSquareBrackets = false, $listMode = false, $sanity = 0)
    {
        //just stop
        if ($sanity > 100) {
            Mage::log("Sanity Error Nested Array Past 100", null, 'levementum_google_tag_manager.log', true);

            return '';
        }
        if (is_array($array)) {
            $return = array();
            foreach ($array as $key => $value) {
                if (is_numeric($key) && is_array($value)) {
                    $value    = $this->collapseToJs($value, false, false, ++$sanity);
                    $return[] = $value;
                    $listMode = true;
                } elseif (is_array($value)) {
                    $value    = $this->collapseToJs($value, false, false, ++$sanity);
                    $return[] = "'" . $key . "': [" . $value . "]";
                } elseif (is_numeric($value)) {
                    $return[] = "'" . $key . "': " . floatval($value) . "";
                } else {
                    $return[] = "'" . $key . "': '" . $value . "'";
                }
            }
            if ($listMode) {
                $return = implode(",", $return);
            } else {
                $return = "{\n" . implode(",\n", $return) . "\n}";
            }
            if ($includeSquareBrackets) {
                $return = '[' . $return . ']';
            }

            return $return;
        } else {
            $return = (string)$array;
            if ($includeSquareBrackets) {
                $return = '[' . $return . ']';
            }

            return $return;
        }
    }
}