<?php
/**
 * @namespace   Levementum
 * @module      Sales
 * @author      Ian Coast
 * @email       icoast@levementum.com
 * @date        12/16/2015 11:51 AM
 * @brief         
 */

/**
 * Class Levementum_Sales_Model_SalesRule_Quote_Freeshipping
 */
class Levementum_Sales_Model_SalesRule_Quote_Freeshipping extends Mage_SalesRule_Model_Quote_Freeshipping {
    /**
     * Get all items except nominals
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return array
     */
    protected function _getAddressItems(Mage_Sales_Model_Quote_Address $address)
    {
        /**
         * Change icoast@levementum.com on 12/16/2015 at 11:51 AM
         * Description: You cannot have nominal and non-nominal items.  freeshipping is a valid method.
         */
//        return $address->getAllNonNominalItems();
        return $address->getAllItems();
    }
}