<?php
/**
 * @namespace   Levementum
 * @module      Sales
 * @file        ${FILE_NAME}
 * @author      mgontijo@levementum.com
 * @date        4/15/15 7:24 PM
 */

class Levementum_Sales_Block_Adminhtml_Sales_Order_Grid
    extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->getColumn('real_order_id')->setFilterIndex('main_table.increment_id');
        $this->getColumn('store_id')->setFilterIndex('main_table.store_id');
        $this->getColumn('created_at')->setFilterIndex('main_table.created_at');
        $this->getColumn('base_grand_total')->setFilterIndex('main_table.base_grand_total');
        $this->getColumn('grand_total')->setFilterIndex('main_table.grand_total');
        $this->getColumn('status')->setFilterIndex('main_table.status');

        $options = Mage::getResourceModel('customer/group_collection')
            ->toOptionHash();

        $this->addColumnAfter('customer_group_id', array(
            'header'  => Mage::helper('sales')->__('Customer Group'),
            'width'   => '80px',
            'type'    => 'options',
            'index'   => 'customer_group_id',
            'filter_index'   => 'order.customer_group_id',
            'options' => $options,
        ), 'shipping_name');

        $this->sortColumnsByOrder();
        return $this;
    }

    public function setCollection($collection)
    {
        $collection->join(
            array('order' => 'sales/order'),
            'main_table.entity_id = order.entity_id',
            array('customer_group_id')
        );

        parent::setCollection($collection);
    }
}
