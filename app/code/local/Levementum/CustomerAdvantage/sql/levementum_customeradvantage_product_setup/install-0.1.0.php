<?php
/**
 * @category    Levementum
 * @package     Levementum_
 * @file        ${FILE_NAME}
 * @auther      jjuleff@levementum.com
 * @date        9/2/14 2:11 PM
 * @brief
 * @details
 */
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'advantage_price', array(
	'group' => 'Prices',
	'attribute_set' => 'Default',
	'type'              => 'varchar',
	'backend'           => '',
	'frontend'          => '',
	'label'             => 'Spark Advantage Price',
	'input'             => 'text',
	'class'             => '',
	'source'            => '',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible'           => true,
	'required'          => false,
	'user_defined'      => true,
	'default'           => '0',
	'searchable'        => false,
	'filterable'        => true,
	'comparable'        => false,
	'visible_on_front'  => true,
	'unique'            => false,
	'apply_to'          => '',
	'is_configurable'   => false,
	'note' => 'How much will Spark Advantage Customers pay for the product?'
));

$entityTypeId = (int)$installer->getEntityTypeId('catalog_product');

$attributeSet = Mage::getModel('eav/entity_attribute_set')
	->setEntityTypeId($entityTypeId)
	->setAttributeSetName('Advantage Membership')
;

if ($attributeSet->validate())
{
	$attributeSet
		->save()
		->initFromSkeleton($entityTypeId)
		->save()
	;
}

$installer->addAttribute('catalog_product', 'advantage_membership', array(
	'group' => 'Advantage Membership',
	'attribute_set' => 'Advantage Membership',
	'type'              => 'int',
	'backend'           => '',
	'frontend'          => '',
	'label'             => 'Spark Advantage Membership',
	'input'             => 'boolean',
	'class'             => '',
	'source'            => '',
	'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
	'visible'           => true,
	'required'          => false,
	'user_defined'      => true,
	'default'           => '0',
	'searchable'        => false,
	'filterable'        => false,
	'comparable'        => false,
	'visible_on_front'  => true,
	'unique'            => false,
	'apply_to'          => '',
	'is_configurable'   => false,
	'note' => 'Purchasing the product will purchase a Sparks Advantage Membership?'
));



$installer->endSetup();