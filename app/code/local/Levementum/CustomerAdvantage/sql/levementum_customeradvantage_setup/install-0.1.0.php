<?php
/**
 * @category    Levementum
 * @package     Levementum_
 * @file        ${FILE_NAME}
 * @auther      jjuleff@levementum.com
 * @date        9/2/14 2:11 PM
 * @brief
 * @details
 */
/* @var $installer Mage_Customer_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup','core_setup');

$setup->addAttribute('customer', 'customer_advantage', array(
	'type'      => 'int',
	'label'     => 'Spark Advantage',
	'input'     => 'boolean',
	'backend'   => 'customer/attribute_backend_data_boolean',
	'sort_order'  => 40,
	'position' => 900,
	'default'  => 0,
	'global' => 1,
	'visible' => 1,
	'used_defined' => 0
));

$setup->addAttribute('customer', 'customer_advantage_start', array(
	'type'     => 'datetime',
	'label'    => 'Spark Advantage Start Date',
	'sort_order' => 41,
	'position' => 901,
	'required' => false,
	'input'    => 'date',
	'global' => 1,
	'visible' => 1,
	'used_defined' => 0
));

$setup->addAttribute('customer', 'customer_advantage_end', array(
	'type'     => 'datetime',
	'label'    => 'Spark Advantage End Date',
	'sort_order' => 42,
	'position' => 902,
	'required' => false,
	'input'    => 'date',
	'global' => 1,
	'visible' => 1,
	'used_defined' => 0
));

$attrSetId = Mage::getModel ( 'customer/customer' )->getResource ()->getEntityType ()->getDefaultAttributeSetId ();

$setup->addAttributeGroup ( 'customer' , $attrSetId , 'Spark Advantage' , 35 );
$setup->addAttributeToSet ( 'customer' , $attrSetId , 'Spark Advantage' , 'customer_advantage' , 10 );
$setup->addAttributeToSet ( 'customer' , $attrSetId , 'Spark Advantage' , 'customer_advantage_start' , 20 );
$setup->addAttributeToSet ( 'customer' , $attrSetId , 'Spark Advantage' , 'customer_advantage_end' , 30 );

Mage::getSingleton('eav/config')
    ->getAttribute('customer','customer_advantage')
    ->setData('used_in_forms',array(
	    'adminhtml_customer',
    ))
    ->save();
Mage::getSingleton('eav/config')
    ->getAttribute('customer','customer_advantage_start')
    ->setData('used_in_forms',array(
	    'adminhtml_customer',
    ))
    ->save();
Mage::getSingleton('eav/config')
    ->getAttribute('customer','customer_advantage_end')
    ->setData('used_in_forms',array(
	    'adminhtml_customer',
    ))
    ->save();

$installer->endSetup();