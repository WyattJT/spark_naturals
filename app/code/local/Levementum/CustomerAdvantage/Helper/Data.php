<?php

/**
 * @category    Levementum
 * @package     Levementum_
 * @file        ${FILE_NAME}
 * @auther      jjuleff@levementum.com
 * @date        9/2/14 2:11 PM
 * @brief
 * @details
 */
class Levementum_CustomerAdvantage_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_productMembershipList = false;

    public function isEnabled()
    {
        return (Mage::getStoreConfig('levementum_customeradvantage/customer_advantage/enabled')
            && $this->isCustomerEnrolled());
    }

    public function isCustomerEnrolled()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }

        if (!Mage::getModel('customer/session')->isLoggedIn()) {
            return false;
        }
        $customer = Mage::getModel('customer/customer')
            ->load(
                Mage::getSingleton('customer/session')
                    ->getCustomerId()
            );
        if ($customer->getData('customer_advantage')) {
            return true;
        }

        return false;
    }

    public function isShippingEnabled()
    {
        return Mage::getStoreConfig('carriers/advantage_shipping/active');
    }

    public function isFreeShippingEnabled()
    {
        return Mage::getStoreConfig('carriers/advantage_shipping/advantage_free');
    }

    public function isFlatShippingEnabled()
    {
        $flatRate = $this->getFlatShippingRate();
        if (empty($flatRate)) {
            return false;
        }

        return Mage::getStoreConfig('carriers/advantage_shipping/advantage_flatrate');
    }

    public function getFlatShippingRate()
    {
        return Mage::getStoreConfig('carriers/advantage_shipping/flat_rate_shipping_rate');
    }

    public function getMembershipProduct($productId)
    {
        return Mage::getModel('catalog/product')->getResource()->getAttributeRawValue(
            $productId, 'advantage_membership', Mage::app()->getStore()
        );
    }

    public function getMembershipProductList()
    {
        if (empty($this->_productMembershipList)) {
            $this->_productMembershipList = array();
            $collection                   = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToFilter('advantage_membership', array('eq' => 1));
            foreach ($collection as $product) {
                $this->_productMembershipList[] = $product->getId();
            }
        }

        return $this->_productMembershipList;
    }

    public function getPrimePath()
    {
        return 'checkout/cart/add';
    }

    public function getPrimeParams()
    {
        $helper     = Mage::helper('checkout/cart');
        $collection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('advantage_membership', array('eq' => 1));

        $url = '';
        /** @var $product Mage_Catalog_Model_Product */
        foreach ($collection as $product) {
            $product->load($product->getId());

            return array(
                'product'                     => $product->getEntityId(),
                Mage_Core_Model_Url::FORM_KEY => Mage::getSingleton('core/session')->getFormKey()
            );
        }
    }
}