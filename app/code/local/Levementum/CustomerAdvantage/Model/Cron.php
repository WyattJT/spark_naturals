<?php /**
 * @category    Levementum
 * @package     Levementum_
 * @file        Cron.php
 * @auther      jjuleff@levementum.com
 * @date        9/4/14 3:06 PM
 * @brief       
 * @details     
 */

class Levementum_CustomerAdvantage_Model_Cron extends Mage_Core_Model_Abstract
{
	protected $_helper = false;

	public function getHelper ()
	{
		if (empty($this->_helper))
		{
			$this->_helper = Mage::helper('levementum_customeradvantage');
		}
		return $this->_helper;
	}
	public function aggregateCustomerAdvantage ( $customerId = false )
	{

		$profiles = Mage::getModel('aw_sarp2/profile')->getCollection()
			->addFieldToselect(array('customer_id','details'))
			->addFieldToFilter('status',array('neq' => 'active'))
		;
		if ($customerId)
		{
			$profiles->addFieldToFilter('customer_id', array('eq' => $customerId));
		}

		//get all customers without
		$customerIds = array();
		foreach ($profiles as $profile)
		{
			if (!$cid = $this->_processProfile( $profile ))
			{
				continue;
			}
			$customerIds[$cid] = 0;
		}

		$profiles->clear();
		$profiles->getSelect()->reset('where');
		$profiles
			->addFieldToselect(array('customer_id','details'))
			->addFieldToFilter('status', array('eq' => 'active'))
		;
		if ($customerId)
		{
			$profiles->addFieldToFilter('customer_id', array('eq' => $customerId));
		}

		foreach ($profiles as $profile)
		{
			if (!$cid = $this->_processProfile( $profile ))
			{
				continue;
			}

			$customerIds[$cid] = 1;
		}

		$this->_processCustomers($customerIds);
	}

	protected function _processProfile ( $profile )
	{
		$details = unserialize($profile->getDetails());
		$productId = $details['subscription']['general']['product_id'];

		//the helper gets all the products that have the membership flag active (creates a membership)
		if (!in_array($productId, $this->getHelper()->getMembershipProductList()))
		{
			return false;
		}
		return $details['customer']['entity_id'];

	}

	protected function _processCustomers ( $customerIds )
	{
		foreach ($customerIds as $customerId => $membershipFlag)
		{
			$customer = Mage::getModel('customer/customer')
			    ->load($customerId);

			//no point in saving the customer object if we aren't changing anything.
			if (($customer->getCustomerAdvantage() && $membershipFlag)
				|| (!$customer->getCustomerAdvantage() && !$membershipFlag))
			{
				continue;
			}

			if ($membershipFlag)
			{
				$advantageStart = $customer->getCustomerAdvantageStart();
				if (empty($advantageStart))
				{
					$customer->setData('customer_advantage_start', Mage::getModel('core/date')->timestamp());
				}
				$customer->setData('customer_advantage_end', '');
			}
			else
			{
				$customer->setData('customer_advantage_end', Mage::getModel('core/date')->timestamp());
			}

			$customer->setData('customer_advantage', $membershipFlag);
			$customer->save();
		}
	}
}