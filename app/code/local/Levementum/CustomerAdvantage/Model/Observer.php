<?php
/**
 * @category    Levementum
 * @package     Levementum_
 * @file        Observer.php
 * @auther      jjuleff@levementum.com
 * @date        9/2/14 7:35 PM
 * @brief       
 * @details     
 */

class Levementum_CustomerAdvantage_Model_Observer extends Mage_Core_Model_Abstract
{


	public function adminhtmlCustomerPrepareSave ( $observer )
	{
		if (!Mage::helper("levementum_customeradvantage")->isEnabled())
		{
			return;
		}
		$customer = $observer->getCustomer();
		$params = $observer->getRequest()->getParams();
		$account = $params['account'];

		if (isset($account['customer_advantage']) && $account['customer_advantage'])
		{
			if (!isset($account['customer_advantage_start']) || empty($account['customer_advantage_start']))
			{
				$customer->setData('customer_advantage_start', Mage::getModel('core/date')->timestamp());
			}
			$customer->setData('customer_advantage_end', '');
		}
		else
		{
			if (!isset($account['customer_advantage_end']) || empty($account['customer_advantage_end']))
			{
				$customer->setData('customer_advantage_end', Mage::getModel('core/date')->timestamp());
			}
		}
	}

	public function catalogProductPrepareSave ( $observer )
	{
		$product = $observer->getProduct();
		if (!Mage::helper("levementum_customeradvantage")->isEnabled())
		{
			return;
		}
		$params = $observer->getRequest()->getParams();
		$productData = $params['product'];
		if (isset($productData['advantage_discount_percent']) && $productData['advantage_discount_percent'])
		{
			$product->setData('advantage_discount_amount', 0);
		}
	}

	public function applyAdvantageDiscount ( $observer )
	{
		$helper = Mage::helper('levementum_customeradvantage');
		if (!$helper->isEnabled())
		{
			return;
		}

		$item = $observer->getQuoteItem();
		$this->_processItem($item);
	}

	public function applyAdvantageDiscounts ( $observer )
	{
		$helper = Mage::helper('levementum_customeradvantage');
		if (!$helper->isEnabled())
		{
			return;
		}

		$items = $observer->getCart()->getQuote()->getAllVisibleItems();

		foreach ($items as $item)
		{
			$this->_processItem($item);
		}
	}

	protected function _processItem ( $item )
	{
		if ($item->getParentItem()) {
			$item = $item->getParentItem();
		}
        //this bit removes prime membership from cart if customer has current membership.
        $helper = Mage::helper('levementum_customeradvantage');
        if ($helper->isCustomerEnrolled())
        {
            $AdvantageProducts = $helper->getMembershipProductList();
            if (in_array($item->getProductId(),$AdvantageProducts))
            {
                Mage::helper('checkout/cart')->getCart()->removeItem($item->getId())->save();
                Mage::getSingleton('checkout/session')->addError('You already have an active Membership on this account. You cannot purchase another one.');
            }
        }

		$advantagePrice = $item->getProduct()->getResource()->getAttributeRawValue($item->getProductId(),'advantage_price',Mage::app()->getStore());
		if (empty($advantagePrice) || $advantagePrice <= 0)
		{
			return;
		}

		$item->setCustomPrice($advantagePrice);
		$item->setOriginalCustomPrice($advantagePrice);
		$item->getProduct()->setIsSuperMode(true);
	}

	public function applyAdvantageOnCheckout ( $observer )
	{
		$customerSession = Mage::getSingleton('customer/session');
		if ($customerSession->isLoggedIn())
		{
			//this will run the aggregate function filtering only our specific customer.
			Mage::getModel('levementum_customeradvantage/cron')
			    ->aggregateCustomerAdvantage($customerSession->getCustomerId())
			;
		}
	}
}