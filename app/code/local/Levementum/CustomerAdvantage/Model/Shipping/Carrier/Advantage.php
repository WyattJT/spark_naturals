<?php
/**
 * @category    Levementum
 * @package     Levementum_
 * @file        Advantage.php
 * @auther      jjuleff@levementum.com
 * @date        9/3/14 12:31 AM
 * @brief       
 * @details     
 */

class Levementum_CustomerAdvantage_Model_Shipping_Carrier_Advantage
	extends Mage_Shipping_Model_Carrier_Abstract
	implements Mage_Shipping_Model_Carrier_Interface
{
	protected $_code = 'advantage_shipping';

	public function collectRates ( Mage_Shipping_Model_Rate_Request $request )
	{
		$helper = Mage::helper('levementum_customeradvantage');
		if (!$helper->isEnabled() || !$helper->isShippingEnabled() || (!$helper->isFreeShippingEnabled() && !$helper->isFlatShippingEnabled()))
		{
			return false;
		}
		$result = Mage::getModel('shipping/rate_result');
		if ($helper->isFreeShippingEnabled())
		{
			$result->append(
			       $this->_getRate('advantage_shipping','Advantage Shipping','advantage_free','Advantage Free', '')
			);
		}
		if ($helper->isFlatShippingEnabled())
		{
			$result->append(
			       $this->_getRate('advantage_shipping','Advantage Shipping','advantage_flatrate','Advantage FlatRate', $helper->getFlatShippingRate())
			);
		}

		return $result;
	}

	public function getAllowedMethods ( )
	{
		$methods = array();
		$helper = Mage::helper('levementum_customeradvantage');
		if ($helper->isEnabled() && $helper->isShippingEnabled())
		{
			if ($helper->isFreeShippingEnabled())
			{
				$methods['advantage_free'] = "Advantage Free Shipping";
			}
			if ($helper->isFlatShippingEnabled())
			{
				$methods['advantage_flatrate'] = "Advantage FlatRate";
			}
		}

		return $methods;
	}

	protected function _getRate ($code, $title, $method, $methodTitle, $price)
	{
		$rate = Mage::getModel('shipping/rate_result_method');
		$rate->setCarrier($code);
		$rate->setCarrierTitle($title);
		$rate->setMethod($method);
		$rate->setMethodTitle($methodTitle);
		$rate->setPrice($price);
		$rate->setCost(0);
		return $rate;
	}
}