<?php
/* Created Automatically by Plosiv Cms Permission Helper. https://w.plosiv.com
If this script saves you time please consider visitng my website and making a donation.
Small amounts accepted and appreciated.
Maybe I have saved you time. Maybe you want to buy me a beer for that ^__^
Assumes the following model.
@var $installer Mage_Core_Model_Resource_Setup
*/
$installer = $this;

$installer->startSetup();

$blockNames = array(
	"catalogextensions/bestsellers_home_list",
	"catalogextensions/featured_home_list",
	"catalog/product_list"
);
if (!empty($blockNames)) {
    foreach ($blockNames as $blockName) {
        $block = Mage::getModel('admin/block')->load($blockName, 'block_name');
        $block->setData('block_name', $blockName);
        $block->setData('is_allowed', 1);
        $block->save();
    }
}

$varNames = array(
//No Vars Specified... You can edit this if you wish.
);
if (!empty($varNames)) {
    foreach ($varNames as $varName) {
        $variable = Mage::getModel('admin/variable')->load($varName, 'variable_name');
        $variable->setData('variable_name', $varName);
        $variable->setData('is_allowed', 1);
        $variable->save();
    }
}

$installer->endSetup();