<?php
/**
 * @namespace    Levementum
 * @module       Catalog
 * @author       afabian@levementum.com
 * @date         6/24/14 4:39 PM
 * @brief
 * @details
 */

/**
 * Class Levementum_Catalog_Helper_Data
 */
class Levementum_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isSkuOotmc($sku)
    {
        return in_array($sku, array('OOTMC', 'OOTMC2','OOTM-3-24.99', 'POOTM3'));
    }
}