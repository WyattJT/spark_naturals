<?php
/**
 * @namespace   Levementum
 * @module      Catalog
 * @file        data-upgrade-0.0.2-0.0.3.php
 * @author      icoast@levementum.com
 * @date        12/15/2014 1:06 PM
 * @brief       
 * @details     
 */

/** @var Mage_Catalog_Model_Resource_Setup $this */
$this->setConfigData('catalog/custom_options/year_range','2014,2015');