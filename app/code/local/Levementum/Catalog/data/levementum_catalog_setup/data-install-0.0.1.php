<?php
/**
 * @namespace    Levementum
 * @module       Catalog
 * @file         data-install-1.0.0.php
 * @author       afabian@levementum.com
 * @date         6/24/14 4:56 PM
 * @brief        Script to move categories and then re-associate products to a different website.
 * @details      Moving categores from one store structure to another.
 */

/** STEP 1: moves all categories to the correct location */
$category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', 'SparkNaturals.com');

/** @var Mage_Catalog_Model_Category $sparkNaturals*/
$sparkNaturals = Mage::getModel('catalog/category')->load($category->getFirstItem()->getData('entity_id'));
$childrenCategories = $sparkNaturals->getAllChildren(false);
$childrenCategories = explode(',',$childrenCategories);

/** @var $category Mage_Catalog_Model_Category */
foreach($childrenCategories as $key => $child){
    if($child == $sparkNaturals->getId()){
        continue;
    }
    $category = Mage::getModel('catalog/category')->load($child);
    Mage::log($category->getName(),null,'levementum.log',true);
    $category->move(2, 0);
}

/** STEP 2: moves all products into the correct website */

$website = array(
    0 => "1"
);

$productCollection  = Mage::getModel('catalog/product')->getCollection()->addWebsiteFilter(2)->setStoreId(1)->addAttributeToSelect('name');

echo count($productCollection);

foreach($productCollection as $product){
    $product->setWebsiteIds($website);

    $product->save();
}

