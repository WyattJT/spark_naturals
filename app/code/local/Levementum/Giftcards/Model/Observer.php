<?php

/**
 * Created by PhpStorm.
 * User: jarrod
 * Date: 11/11/14
 * Time: 11:08 PM
 */
class Levementum_Giftcards_Model_Observer extends Mage_Core_Model_Abstract
{
    public function salesQuoteAddressDiscountItem($observer)
    {
        $item = $observer->getItem();
        if ($item->getProductType() == 'giftcards') {
            $item->setNoDiscount(1);
            $item->setDiscountAmount(0);
            $item->setBaseDiscountAmount(0);
            $item->setRowTotalWithDiscount($item->getRowTotal());
            $item->setBaseRowTotalWithDiscount($item->getRowTotal());
            Mage::getSingleton('core/session')->setGiftCardInCart(1);
        }
        $addMessage = false;
        $message = 'Discounts cannot be applied to Giftcards.';
        if (Mage::getSingleton('core/session')->getGiftCardInCart()) {
            $items      = Mage::getSingleton('core/session')->getMessages()->getItems('notice');
            $addMessage = true;
            foreach ($items as $notice) {
                if ($notice->getCode() == $message) {
                    $addMessage = false;
                    break;
                }
            }
        }
        if ($addMessage) {
            Mage::getSingleton('core/session')->addNotice($message);
        }
    }

    public function salesQuoteRemoveItem($observer)
    {
        $item = $observer->getQuoteItem();
        if ($item->getProductType() == 'giftcards') {
            Mage::getSingleton('core/session')->unsGiftCardInCart();
        }
    }
}