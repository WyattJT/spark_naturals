<?php
/**
 * jjuleff@levementum.com
 * 2015-02-28
 */

class Levementum_DiscountPage_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

}