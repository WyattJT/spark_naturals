<?php
/**
 * Created by PhpStorm.
 * User: jarrod
 * Date: 2/18/15
 * Time: 7:25 AM
 */ 
class Levementum_DiscountPage_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $_collection;

    public function getCollection()
    {
        if (is_null($this->_collection))
        {
            /** @var Mage_Catalog_Model_Product _collection */
            $this->_collection =
                Mage::getResourceModel('catalog/product_collection')
                    ->getCollection()
                    ->addFieldToFilter('include_in_bargain', array('eq' => 1))
            ;
        }
        return $this->_collection;
    }

    public function isEnabled()
    {
        return Mage::getStoreConfig('levementum_discountpage/discounts/enabled');
    }

    public function getTitle()
    {
        return Mage::getStoreConfig('levementum_discountpage/discounts/title');
    }

    public function useDiscounts()
    {
        return ($this->isEnabled() && !$this->isCollectionEmpty());
    }

    public function isCollectionEmpty()
    {
        if (is_object($this->getCollection()))
        {
            return ($this->getCollection()->count() <= 0);
        }
        return (count($this->getCollection()) <= 0);
    }
}