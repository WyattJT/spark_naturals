<?php

/**
 * jjuleff@levementum.com
 * 2015-02-28
 * Adds menu items.
 */
class Levementum_DiscountPage_Model_Menu_Observer extends Mage_Core_Model_Abstract
{
    public $helper;

    public function getHelper()
    {
        if (is_null($this->helper)) {
            $this->helper = Mage::helper('levementum_discountpage');
        }

        return $this->helper;
    }

    public function checkForDiscounts(Varien_Event_Observer $observer)
    {
        //if our check for discount is true, we want to add the bargain item name.
        if ($this->getHelper()->isEnabled()) {
            $this->_addToTopmenu($observer);
        }
    }

    public function _addToTopmenu(Varien_Event_Observer $observer)
    {
        $menu = $observer->getMenu();
        $tree = $menu->getTree();


        $name = $this->getHelper()->getTitle();
        $id   = "discounts";
        $url  = Mage::getUrl('discounts');


        $node = new Varien_Data_Tree_Node(array(
            'name' => $name,
            'id'   => $id,
            'url'  => $url, // point somewhere
        ), 'id', $tree, $menu);

        $menu->addChild($node);
    }

    public function filterCollection(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('levementum_discountpage')->isEnabled()) {
            return;
        }

        $now = date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp(time()));
        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */

        //gets all bargain stuff.
        if (Mage::app()->getFrontController()->getAction()->getFullActionName() != "discounts_index_index") {
            //removes all bargain exclusions.
            $collection = $observer->getEvent()->getCollection();
            $collection->addAttributeToFilter('only_on_sale', array('eq' => 0));
        }
    }
}