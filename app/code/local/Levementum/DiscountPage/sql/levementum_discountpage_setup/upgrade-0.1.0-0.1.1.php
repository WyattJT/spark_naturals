<?php
/**
 * Created by PhpStorm.
 * User: jarrod
 * Date: 2/18/15
 * Time: 7:25 AM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
/** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
$collection = Mage::getModel('catalog/product')->getCollection();
foreach ($collection as $product)
{
    $product = $product->load($product->getId());
    $product->setData('only_on_sale', 0);
    $product->getResource()->saveAttribute($product,'only_on_sale');
}
Mage::app()->setUpdateMode(true);

$installer->endSetup();