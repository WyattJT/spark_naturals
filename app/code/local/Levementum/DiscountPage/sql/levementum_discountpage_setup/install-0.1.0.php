<?php
/**
 * Created by PhpStorm.
 * User: jarrod
 * Date: 2/18/15
 * Time: 7:25 AM
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'include_in_bargain', array(
        'group'         => 'Prices',
        'label'         => 'Include In Bargain Category',
        'input'         => 'select',
        'source'        => 'eav/entity_attribute_source_boolean',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'       => true,
        'required'      => false,
        'user_defined'  => false,
        'default'       => 0,
        'apply_to'      => '',
        'input_renderer'   => '',
        'visible_on_front' => false,
        'used_in_product_listing' => true,
        'sort_order' => 200
    ));
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'only_on_sale', array(
        'group'         => 'Prices',
        'label'         => 'Exclude From Normal Category',
        'input'         => 'select',
        'source'        => 'eav/entity_attribute_source_boolean',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'       => true,
        'required'      => false,
        'user_defined'  => false,
        'default'       => 0,
        'apply_to'      => '',
        'visible_on_front' => false,
        'used_in_product_listing' => true,
        'sort_order' => 210
    ));
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'on_sale_from', array(
        'group'         => 'Prices',
        'label'         => 'Include In Bargain Category After:',
        'input'         => 'date',
        'source'        => 'eav/entity_attribute_source_boolean',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'       => true,
        'required'      => false,
        'user_defined'  => false,
        'default'       => '',
        'apply_to'      => '',
        'visible_on_front' => false,
        'used_in_product_listing' => true,
        'sort_order' => 220
    ));
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'on_sale_to', array(
        'group'         => 'Prices',
        'label'         => 'Include In Bargain Category Until:',
        'input'         => 'date',
        'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'       => true,
        'required'      => false,
        'user_defined'  => false,
        'default'       => '',
        'apply_to'      => '',
        'visible_on_front' => false,
        'used_in_product_listing' => true,
        'sort_order' => 230
    ));

Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
/** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
$collection = Mage::getModel('catalog/product')->getCollection();
foreach ($collection as $product)
{
    $product = $product->load($product->getId());
    $product->setData('include_in_bargain', 0);
    $product->setData('only_on_sale', 0);
    $product->getResource()->saveAttribute($product,'include_in_bargain');
    $product->getResource()->saveAttribute($product,'only_on_sale');
}
Mage::app()->setUpdateMode(true);

$installer->endSetup();