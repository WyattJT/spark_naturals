<?php
/**
 * jjuleff@levementum.com
 * 2015-02-28
 */

class Levementum_DiscountPage_Block_Discount_View extends Mage_Catalog_Block_Product_List
{
    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {
            $now = date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp(time()));
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addAttributeToFilter('include_in_bargain', array('eq' => 1))
                ->addAttributeToFilter('on_sale_from', array( array('lteq' => $now), array('null' => 1) ) )
                ->addAttributeToFilter('on_sale_to', array( array('gteq' => $now), array('null' => 1) ) )
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addStoreFilter()
            ;
            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            $this->_productCollection = $collection;
        }

        return $this->_productCollection;
    }
}