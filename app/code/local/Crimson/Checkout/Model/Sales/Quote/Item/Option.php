<?php

/**
 * @category    Crimson
 * @package     Checkout
 * @file        ${FILE_NAME}
 * @author      mgontijo@crimsonagility.com
 * @date        1/7/15 11:34 PM
 */

class Crimson_Checkout_Model_Sales_Quote_Item_Option
    extends Mage_Sales_Model_Quote_Item_Option
{
    protected function _beforeSave()
    {
        if (!in_array($this->getCode(), array(
                'info_buyRequest',
                'recurring_profile_options',
                'additional_options',
            ))) {
            return parent::_beforeSave();
        }

        $valueData = unserialize($this->getValue());

        if ($this->getCode() == 'recurring_profile_options' && isset($valueData['start_datetime'])) {
            $date = new \DateTime($valueData['start_datetime']);
        } else if ($this->getCode() == 'info_buyRequest' && isset($valueData['recurring_profile_start_datetime'])) {
            $date = new \DateTime($valueData['recurring_profile_start_datetime']);
        } else {
            foreach ($valueData as $item) {
                if (isset($item['label']) && $item['label'] == 'Start Date') {
                    $date = new \DateTime($item['value']);
                }
            }
        }

        if (!isset($date)) {
            return parent::_beforeSave();
        }

//        $date = new \DateTime('1999-12-29 00:00:00');

        if (in_array($date->format('d'), array(
            29, 30, 31
        ))) {
            do {
                $newDate = $date->add(new DateInterval('P1D'));
            } while ($newDate->format('d') != '01');

            if ($this->getCode() == 'recurring_profile_options') {
                $valueData['start_datetime'] = $newDate->format('Y-m-d H:i:s');
            } else if ($this->getCode() == 'info_buyRequest') {
                $valueData['recurring_profile_start_datetime'] = $newDate->format('Y-m-d H:i:s');
            } else {
                foreach ($valueData as &$item) {
                    if ($item['label'] == 'Start Date') {
                        $item['value'] = $newDate->format('Y-m-d H:i:s');
                    }
                }
            }
        }

        $valueData = serialize($valueData);

        $this->setValue($valueData);

        return parent::_beforeSave();
    }
}
