<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Idev.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 4:05 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Affiliate_Block_Checkout_Success_Idev
 * @factory crimson_affiliate/checkout_success_idev
 */
class Crimson_Affiliate_Block_Checkout_Success_Idev extends Crimson_Affiliate_Block_Checkout_Success_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('crimson/affiliate/checkout/success/idev.phtml');
    }

    /**
     * Don't print out idev if we have an SAS order.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->getAffiliateHelper()->getIsIdevEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }
}