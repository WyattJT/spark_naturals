<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Sas.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 4:05 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Affiliate_Block_Checkout_Success_Sas
 * @factory crimson_affiliate/checkout_success_sas
 */
class Crimson_Affiliate_Block_Checkout_Success_Sas extends Crimson_Affiliate_Block_Checkout_Success_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('crimson/affiliate/checkout/success/sas.phtml');
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return string
     */
    public function getSasPixelUrl($order)
    {
        return Mage::helper('crimson_affiliate/sas_data')->getSasPixelUrl($order);
    }

    public function _toHtml()
    {
        if (!$this->getAffiliateHelper()->getSasHelper()->getIsEnabled()) {
            return '';
        }

        return parent::_toHtml();
    }
}