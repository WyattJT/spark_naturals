<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Success.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 4:05 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Affiliate_Block_Checkout_Success_Abstract
 */
abstract class Crimson_Affiliate_Block_Checkout_Success_Abstract extends Mage_Checkout_Block_Onepage_Success
{
    /**
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if (!$this->_getData('order')) {
            $order   = Mage::getModel('sales/order');
            $orderId = $this->getOrderId();
            if (!$orderId) {
                $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
            }

            if ($orderId) {
                $order->loadByIncrementId($orderId);
            }

            $this->setData('order',$order);
        }

        return $this->_getData('order');
    }

    protected function _toHtml()
    {
        if (!$this->getOrder()->getId()) {
            return '';
        }

        return parent::_toHtml();
    }

    public function getAffiliateHelper()
    {
        return Mage::helper('crimson_affiliate');
    }

    public function isSasOrder($order)
    {
        $this->getAffiliateHelper()->getSasHelper()->isSasOrder($order);
    }
}