<?php
/**
 * @namespace   Crimson
 * @module      ${MODULE}
 * @file        install-0.1.0.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 3:43 PM
 * @brief
 * @details
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();

/** @var Varien_Db_Adapter_Pdo_Mysql $connection */
$connection = $installer->getConnection();

$tables = array(
    'sales/order',
    'sales/quote',
);

foreach ($tables as $table) {
    $tableName = $installer->getTable($table);
    $connection->addColumn(
        $tableName, Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY, array(
            'type'     => Varien_Db_Ddl_Table::TYPE_TEXT,
            'length'   => 255,
            'nullable' => true,
            'comment'  => 'Share a sale pixel tracking value.'
        )
    );

    $idxName = $installer->getIdxName($table, Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY);
    $connection->addIndex($tableName, $idxName, Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY);
}

$installer->endSetup();