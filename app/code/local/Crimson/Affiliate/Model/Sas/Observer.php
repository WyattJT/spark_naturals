<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Observer.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 3:07 PM
 * @brief       SAS = Share a sale observer
 * @details
 */

/**
 * Class Crimson_Affiliate_Model_Sas_Observer
 */
class Crimson_Affiliate_Model_Sas_Observer
{
    protected function _isSasEnabled()
    {
        return Mage::helper('crimson_affiliate/sas_data')->getIsEnabled();
    }

    public function checkoutSubmitAllAfter(Varien_Event_Observer $observer)
    {
        if (!$this->_isSasEnabled()) {
            return;
        }

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();

        $sasPixelTracking = Mage::helper('crimson_affiliate/sas_data')->getSasPixelTracking($quote);

        //not on the cookie or quote, guess we aren't tracking.  exit.
        if (!$sasPixelTracking) {
            return;
        }

        /** @var Mage_Sales_Model_Order[] $orders */
        $orders = $observer->getOrders();

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if (is_object($order)) {
            $orders = array($order);
        }

        foreach ($orders as $order) {
            $order->setData(Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY, $sasPixelTracking);
            $order->getResource()->saveAttribute($order, Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY);
        }
    }

    public function controllerActionPredispatch(Varien_Event_Observer $observer)
    {
        if (!$this->_isSasEnabled()) {
            return;
        }

        /** @var Mage_Core_Controller_Varien_Action $controllerAction */
        $controllerAction = $observer->getControllerAction();
        if (!($sasValue = $controllerAction->getRequest()->getQuery(
            Crimson_Affiliate_Helper_Sas_Data::SAS_PARAM_KEY, false
        ))
        ) {
            return;
        }

        Mage::helper('crimson_affiliate/sas_data')->setSasCookie($sasValue);
    }

    /**
     * Before we save quote, set sas data on cookie.
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesQuoteSaveBefore(Varien_Event_Observer $observer)
    {
        if (!$this->_isSasEnabled()) {
            return;
        }

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();
        if ($quote->getData(Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY)) {
            return;
        }

        if (!($sasValue = Mage::helper('crimson_affiliate/sas_data')->getSasCookie())) {
            return;
        }

        $quote->setData(Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY,$sasValue);
    }
}