<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Request.php
 * @author      mgontijo@crimsonagility.com
 * @date        3/3/2016 4:35 PM
 */

class Crimson_Affiliate_Model_Core_Url_Rewrite_Request extends Mage_Core_Model_Url_Rewrite_Request
{
    protected function _sendRedirectHeaders($url, $isPermanent = false)
    {
        if (strpos($url, '?') !== false) {
            $url = $url . '&' . $_SERVER['QUERY_STRING'];
        } else {
            $url = $url . '?' . $_SERVER['QUERY_STRING'];
        }

        parent::_sendRedirectHeaders($url, $isPermanent);
    }
}
