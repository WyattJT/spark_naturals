<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Data.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 3:05 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Affiliate_Helper_Data
 */
class Crimson_Affiliate_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return Crimson_Affiliate_Helper_Sas_Data
     */
    public function getSasHelper()
    {
        return Mage::helper('crimson_affiliate/sas_data');
    }

    public function getIsIdevEnabled()
    {
        return Mage::getStoreConfigFlag('crimson_affiliate/idev/enabled');
    }

}