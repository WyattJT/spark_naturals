<?php
/**
 * @namespace   Crimson
 * @module      Affiliate
 * @file        Data.php
 * @author      icoast@crimsonagility.com
 * @date        10/16/2015 3:05 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Affiliate_Helper_Sas_Data
 */
class Crimson_Affiliate_Helper_Sas_Data extends Crimson_Affiliate_Helper_Data
{
    /** share a sale (SAS) cookie duration in days. */
    const SAS_COOKIE_DURATION = 90;
    const SAS_COOKIE_NAME = 'sas_track';
    const SAS_PARAM_KEY = 'sasptid';

    //column name sas pixel tracking is stored in on tables.
    // do not change without changing config.xml and relevant tables (quote & order).
    const SAS_DATA_KEY = 'sas_pixel_tracking';

    public function getIsEnabled()
    {
        return Mage::getStoreConfigFlag('crimson_affiliate/shareasale/enabled');
    }

    public function getSasMerchantId()
    {
        return Mage::getStoreConfig('crimson_affiliate/shareasale/merchant_id');
    }

    /**
     * @param string|int|Mage_Sales_Model_Order $order
     * @param bool $passedIncrementId
     *
     * @return bool
     */
    public function isSasOrder($order, $passedIncrementId = false)
    {

        if (!($order instanceof Mage_Sales_Model_Order)) {
            if ($passedIncrementId) {
                $order = Mage::getModel('sales/order')->loadByIncrementId($order);
            } else {
                $order = Mage::getModel('sales/order')->load($order);
            }
        }

        //if it's not null, we have an SAS order.
        return !is_null($order->getData(self::SAS_DATA_KEY));
    }

    public function setSasCookie($cookieValue)
    {
        $cookieName = self::SAS_COOKIE_NAME;
        $expiration = self::SAS_COOKIE_DURATION * 3600 * 24;

        Mage::app()->getCookie()->set($cookieName, $cookieValue, $expiration);

        $quote = Mage::helper('checkout')->getQuote();
        $quote->setData(Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY, $cookieValue);
        if ($quote->getId()) {
            $resource = $quote->getResource();
            $resource->getReadConnection()->update(
                $resource->getMainTable(),
                array(
                    Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY => $cookieValue
                ), $resource->getReadConnection()->quoteInto($resource->getIdFieldName().'=?',$quote->getId())
            );
        }

        return $this;
    }

    /**
     * Use this to return what is store in the cookie.
     *
     * @return string|bool - false on failure
     */
    public function getSasCookie()
    {
        return Mage::app()->getCookie()->get(self::SAS_COOKIE_NAME);
    }

    /**
     * Use this to get the finalized sas pixel tracking value.
     *
     * @param null|Mage_Sales_Model_Quote $quote
     *
     * @return bool|string
     */
    public function getSasPixelTracking($quote = null)
    {
        $sasPixelTracking = $this->getSasCookie();

        //we don't have a cookie, let's see if we have one on the quote.
        if (!$sasPixelTracking) {
            if (!($quote instanceof Mage_Sales_Model_Quote)) {
                $quote = Mage::helper('checkout')->getQuote();
            }
            if (!$quote->getId()) {
                return false;
            }
            $sasPixelTracking = $quote->getData(Crimson_Affiliate_Helper_Sas_Data::SAS_DATA_KEY);
        }

        //not on the cookie or quote, guess we aren't.  exit.
        if (!$sasPixelTracking) {
            return false;
        }

        return $sasPixelTracking;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return string
     */
    public function getSasPixelUrl($order)
    {
        $subtotal       = $order->getSubtotal();
        $discount       = $order->getDiscountAmount();
        $affiliateTotal = ($subtotal + $discount);

        $url = 'https://shareasale.com/sale.cfm?tracking=%1$s&amount=%2$s&transtype=sale&merchantID=%3$s';
        $url = sprintf(
            $url,
            $order->getIncrementId(), //1
            $affiliateTotal, //2
            $this->getSasMerchantId() //3
        );

        $code = $order->getCouponCode();
        if ($code) {
            $url = Mage::helper('core/url')->addRequestParam($url, array('couponcode' => $code));
        }

        //autovoid if we do not have SAS cookie or want to prioritize IDEV (TBD on IDEV);
        $sasPixelTracking = $order->getData(self::SAS_DATA_KEY);
        if (!$sasPixelTracking) {
            $url = Mage::helper('core/url')->addRequestParam($url, array('autovoid' => 1));
        }

        return $url;
    }
}