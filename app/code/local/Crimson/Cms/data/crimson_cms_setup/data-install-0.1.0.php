<?php
/**
 * @namespace   Crimson
 * @module      Cms
 * @author      Ryan Simmons
 * @email       rsimmons@crimsonagility.com
 * @date        9/29/2016 2:24 PM
 * @brief
 */

$installer = $this;

$installer->startSetup();

$cmsBlock = Mage::getModel('cms/block')->load('social_block');

$blockContent = <<<EOT
<div class="social-btns">
    <div class="imgs"><a href="https://twitter.com/sparknaturals"><img src="{{media url="wysiwyg/SparkNaturals/btn_tw.png"}}" alt="" /></a></div>
    <div class="imgs"><a href="https://www.facebook.com/SparkNaturals"><img src="{{media url="wysiwyg/SparkNaturals/btn_fb.png"}}" alt="" /></a></div>
    <div class="imgs"><a href="http://instagram.com/sparknaturals"><img src="{{media url="wysiwyg/SparkNaturals/btn_instagram.png"}}" alt="" /></a></div>
    <div class="imgs"><a href="http://www.amazon.com/sparknaturals"><img src="{{media url="wysiwyg/SparkNaturals/SN_btn_amazon.png"}}" alt="" /></a></div>
</div>
<div class="social-newsletter"><span class="social-newsletter-join">Join our email list</span> {{block type="newsletter/subscribe" template="newsletter/subscribe.phtml"}}</div>
EOT;


$cmsBlock->setContent($blockContent);

$cmsBlock->save();

$installer->endSetup();

