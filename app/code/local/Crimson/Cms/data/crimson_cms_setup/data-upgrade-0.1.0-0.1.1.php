<?php
/**
 * @namespace   Crimson
 * @module      Cms
 * @author      Ryan Simmons
 * @email       rsimmons@crimsonagility.com
 * @date        9/29/2016 2:24 PM
 * @brief
 */

$installer = $this;

$installer->startSetup();

$query = "UPDATE `core_config_data` SET `value` = 'ra-58c2d7eb06aea541' WHERE `path` = 'plugins_general/general/pubid';";
$installer->run($query);

$installer->endSetup();

