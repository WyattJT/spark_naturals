<?php
class Crimson_Rakuten_Block_Checkout_Onepage_Review_Rakuten extends Crimson_Affiliate_Block_Checkout_Success_Abstract
{
    public function __construct(array $args)
    {
//        die(PHP_EOL.'<hr/>File: '.__FILE__.' on Line: '.__LINE__.PHP_EOL);
        parent::__construct($args);
    }

    public function getPixelUrl($order)
    {
        $url = Mage::helper('crimson_rakuten/data')->getPixelUrl($order);
        return $url;
    }
}
