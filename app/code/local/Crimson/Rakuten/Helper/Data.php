<?php
/**
 * @namespace   Crimson
 * @module      Rakuten
 * @file        Data.php
 * @author      dadams@crimsonagility.com
 * @date        10/10/2016
 * @brief
 * @details
 */

/**
 * Class Crimson_Rakuten_Helper_Sas_Data
 */
class Crimson_Rakuten_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $order;

    protected function getMerchantId()
    {
        //        Mage::getStoreConfig('crimson_affiliate/shareasale/merchant_id')
        return '42099';
    }

    protected function getBaseUrl()
    {
        return 'https://track.linksynergy.com/ep?';
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return string
     */
    protected function getItemData($order)
    {
        $data = [];
        // Process Items
        /** @var Mage_Sales_Model_Resource_Order_Item_Collection $items */
        $items = $order->getItemsCollection();
        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($items as $item) {
            $element = [
                'sku' => $item->getSku(),
                'qty' => $item->getQtyOrdered(),
                'amt' => $item->getRowTotal() * 100,
            ];
            $data[]  = $element;
        }

        // Check for Discount
        if ($order->getDiscountAmount())
        {
            $element = [
                'sku' => 'Discount',
                'qty' => 0,
                'amt' => $order->getDiscountAmount() * 100,
            ];
            $data[]  = $element;
        }

        // Make Lists
        $list = ['skulist' => '', 'qlist' => '', 'amtlist' => ''];
        foreach ($data as $dat) {
            if ($list['skulist']) {
                $list['skulist'] .= '|';
            }
            $list['skulist'] .= urlencode($dat['sku']);
            if ($list['qlist']) {
                $list['qlist'] .= '|';
            }
            $list['qlist'] .= urlencode((int)$dat['qty']);
            if ($list['amtlist']) {
                $list['amtlist'] .= '|';
            }
            $list['amtlist'] .= urlencode($dat['amt']);
        }

        return $list;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return string
     */
    public function getPixelUrl($order)
    {
        $url = $this->getBaseUrl();
        $url .= "mid=" . $this->getMerchantId();
        $url .= "&ord=" . $order->getIncrementId();

        $list = $this->getItemData($order);
        $url .= "&skulist=" . $list['skulist'];
        $url .= "&qlist=" . $list['qlist'];
        $url .= "&amtlist=" . $list['amtlist'];

        $url .= "&cur=" . $order->getBaseCurrencyCode();
        $url .= "&img=1";

        return $url;
    }
}