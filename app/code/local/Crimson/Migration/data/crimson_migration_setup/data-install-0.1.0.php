<?php
/**
 * @namespace   Crimson
 * @module      Migration
 * @author      Ian Coast
 * @email       icoast@crimsonagility.com
 * @date        12/11/2015 5:20 PM
 * @brief
 */

/*
 * Begin new recurring profile product
 */
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$originalOotmcProductId = Mage::getResourceSingleton('catalog/product')->getIdBySku('OOTMC');
$originalOotmcProduct = Mage::getModel('catalog/product')->load($originalOotmcProductId);
$originalOotmcProduct->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE);
$originalOotmcProduct->setUrlKey($originalOotmcProduct->getUrlKey().'-old');
$originalOotmcProduct->setData('save_rewrites_history',false);
$originalOotmcProduct->save();

$rewrite = Mage::getModel('core/url_rewrite')->loadByIdPath('product/'.$originalOotmcProductId);
$rewrite->delete();

$originalOotmcProduct = Mage::getModel('catalog/product')->load($originalOotmcProductId);
$ootmcProduct = $originalOotmcProduct->duplicate();

$ootmcProduct = Mage::getModel('catalog/product')->load($ootmcProduct->getId());
$ootmcProduct
    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
    ->setSku('OOTMC2');

$ootmcProduct->setIsRecurring(1)
    ->setData('recurring_profile',array(
        'start_date_is_editable' => 1,
        'suspension_threshold' => 3,
        'bill_failed_later' => 1,
        'period_unit' => 'month',
        'period_frequency' => 1,
        'init_may_fail' => false,

    ));
$ootmcProduct->setData('stock_data',array(
    'manage_stock' => 0,
    'use_config_manage_stock' => 0,
));
$ootmcProduct->setUrlKey('oil-of-the-month-club');
$ootmcProduct->save();

$rewrite = Mage::getModel('core/url_rewrite')->loadByIdPath('product/'.$ootmcProduct->getId());
$rewrite->setRequestPath('oil-of-the-month-club.html');
$rewrite->save();

Mage::getSingleton('catalog/product_status')->updateProductStatus($ootmcProduct->getId(),0,Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

/*
 * End new recurring profile product
 */