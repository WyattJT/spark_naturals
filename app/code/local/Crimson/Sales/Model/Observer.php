<?php

/**
 * @category    Crimson
 * @package     Sales
 * @file        ${FILE_NAME}
 * @author      mgontijo@crimsonagility.com
 * @date        4/15/16 4:44 PM
 */

class Crimson_Sales_Model_Observer
{
    public function recurringProfileBilled(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();
        $billed_count = $observer->getBilledcount();
        $items = $order->getAllItems();
        
        foreach ($items as $item) {
            /*$item->setDescription(sprintf('Orders processed for this profile: %d', $billed_count));*/
            $item->setDescription(sprintf('(Month %d)', $billed_count));
            $item->save();
        }
        
        /*
         * 
         * EDK - This code met a legacy requirement which was superceded by the above modification
         * 
        $resource = Mage::getSingleton('core/resource');
        $readConn = $resource->getConnection('core_read');

        $subQuery = $readConn->select()
            ->from(
                $resource->getTableName('sales/recurring_profile_order'),
                array('profile_id'))
            ->where('order_id = ?', $order->getId());

        $select = $readConn->select()
            ->from(
                $resource->getTableName('sales/recurring_profile_order'),
                array('COUNT(*)'))
            ->where('profile_id = ?', $subQuery);

        $total = $readConn->fetchOne($select);

        if ($total) {
            $historyItem = $order->addStatusHistoryComment(sprintf('Orders processed for this profile: %d', $total));
            $historyItem->setIsVisibleOnFront(true);
            $historyItem->save();
        }
        */
    }
}
