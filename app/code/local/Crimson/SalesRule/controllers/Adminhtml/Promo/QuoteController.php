<?php
require_once "Mage/Adminhtml/controllers/Promo/QuoteController.php";
class Crimson_SalesRule_Adminhtml_Promo_QuoteController extends Mage_Adminhtml_Promo_QuoteController{

    public function saveCustomCouponAction(){

        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        $result = array();
        $this->_initRule();
        /** @var $rule Mage_SalesRule_Model_Rule */
        $rule = Mage::registry('current_promo_quote_rule');
        $code = $this->getRequest()->getParams()['code'];

        if (!$rule->getId()) {
            $result['error'] = Mage::helper('salesrule')->__('Rule is not defined');
        }
        elseif (!$code) {
            $result['error'] = Mage::helper('salesrule')->__('Code not Provided');
        }
        else {
            try{
            $coupon = Mage::getModel('salesrule/coupon');
            $now = $coupon->getResource()->formatDate(
                Mage::getSingleton('core/date')->gmtTimestamp()
            );
            $expirationDate = $rule->getToDate();
            if ($expirationDate instanceof Zend_Date) {
                $expirationDate = $expirationDate->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
            }

                $coupon->setId(null)
                ->setRuleId($rule->getId())
                ->setUsageLimit($rule->getUsesPerCoupon())
                ->setUsagePerCustomer($rule->getUsesPerCustomer())
                ->setExpirationDate($expirationDate)
                ->setCreatedAt($now)
                ->setType(Mage_SalesRule_Helper_Coupon::COUPON_TYPE_SPECIFIC_AUTOGENERATED)
                ->setCode($code)
                ->save();

                $this->_getSession()->addSuccess(Mage::helper('salesrule')->__('Coupon successfully saved'));
                $this->_initLayoutMessages('adminhtml/session');
                $result['messages']  = $this->getLayout()->getMessagesBlock()->getGroupedHtml();
            } catch (Mage_Core_Exception $e) {
                $result['error'] = $e->getMessage();
            } catch (Exception $e) {
                $result['error'] = Mage::helper('salesrule')->__('An error occurred while saving coupon. Please review the log and try again.');
                Mage::logException($e);
            }
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }



}
				