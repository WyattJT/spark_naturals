<?php
$installer = $this;
$installer->startSetup();

$conf = new Mage_Core_Model_Config();

$data = '<h3>SHIPPING TERMS & CONDITIONS</h3>
<p>Orders can only be shipped to addresses in the United States and Canada. Canada shipments ship First Class International via USPS. Domestic Shipments USPS Ground is our Standard and Preferred shipping method. Faster shipping methods are available upon checkout. Shipment tracking is available for all shipments. If you do not receive an email with a tracking number, please contact our shipping department at sparknaturalsshipping@gmail.com. You may cancel your order at any time prior to processing simply by sending an email to customerservices@sparknaturals.com. Unfortunately, we are not able to cancel orders once they have been shipped. Please refer to our Returns Policy for easy</p>

<h3>RETURNS AND REFUNDS POLICY</h3>
<p>A full refund will be issued for any unopened products returned within 30 days of purchase. For any opened bottles, a 25% restocking fee will be added. Returns must be authorized by our customer service department. Customers is responsible for packaging and mailing back to Spark Naturals within 30 days of purchase. SN is not responsible for lost of damage of returned items. There are no refunds or store credit on any return after 30 days of purchase.  *Shipping fees are non-refundable. If customer received free shipping and returns an item a shipping fee of $5 will be deducted from refund due to customer.
Diffusers
There are no returns or store credit for opened / used diffusers. All seals must be intact, including the original shrink-wrap. All diffusers are covered with a one-year manufacture warranty.</p>

<h3>DAMAGED OR DEFECTIVE ITEMS</h3>
<p>Customers have 30 days from the order date to report any damage during shipping, mis-shipped items or product defects, including accessories.
Spark Naturals is not responsible if customer accidently spills oil/s</p>


<h3>MISSING OR LOST SHIPMENTS</h3>
<p>If package is marked as "delivered" by assigned mail carrier, Spark Naturals is not responsible for replacing lost items or package.</p>

<h3>INCORRECT SHIPPING ADDRESS</h3>
<p>If package is returned to the warehouse due to the wrong shipping address, a $5 reshipping fee must be paid before the package can be resent. This also applies to Oil of the Month Club shipments. If package was delivered to the wrong address do to an error at checkout, the customer must pay for the entire order to be replaced.</p>';

$conf->saveConfig('onestepcheckout/terms_conditions/term_html', $data, 'default', 0);


$installer->endSetup();