<?php
$installer = $this;
$installer->startSetup();

// For creating for a store
//$store = Mage::app()->getStore('store_code');

// ===== CMS Static Blocks =====
$data = array(
    'title' => 'Contest Rules',
    'identifier' => 'contest_rules',
    'stores' => array(0), // Array with store ID's
    'content' => '<div class="content-rules">
<h3>Content rules</h3>
<p>There is no purchase necessary to win. Void where prohibited by law. Odds of winning are based on the number of eligible entries received. Contestants must be 18 years of age or older to participate.</p>
<p>By entering this contest you give us permission to contact you about Spark Naturals and our 100% Pure Essential Oils.</p>
<ul>
<li>Contestants are eligible to win only once during a 30 day period.</li>
<li>Prizes have no cash value, are non-transferable, and substitute prizes are not available.</li>
<li>All Federal, State, and local laws apply. Winners are liable for any federal, state and local taxes. Prizes are non-exchangeable, non-transferable and non-redeemable for cash.</li>
</ul>
</div>',
    'is_active' => 1
);

// Check if static block already exists:
//$block = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($data['identifier']);
//echo $block->getId();
$block = Mage::getModel('cms/block')->load($data['identifier'])->isObjectNew();
if ($block) {
    // Create static block:
    Mage::getModel('cms/block')->setData($data)->save();
}

 // ===== CMS Static Blocks =====
$data = array(
    'title' => 'Web form left note',
    'identifier' => 'webform_left_note',
    'stores' => array(0), // Array with store ID's
    'content' => '<div class="popup-logo"><img src="{{media url=\'webform/popup-logo.jpg\'}}" alt="" /></div>
<div class="win-list">
<h2>Previous Winners</h2>
<ul>
<li>March 2014,<em>John r, <br />Rochetster, MN</em></li>
<li>March 2014,<em>John r, <br />Rochetster, MN</em></li>
<li>March 2014,<em>John r, <br />Rochetster, MN</em></li>
<li>March 2014,<em>John r, <br />Rochetster, MN</em></li>
<li>March 2014,<em>John r, <br />Rochetster, MN</em></li>
</ul>
</div>',
    'is_active' => 1
);

// Check if static block already exists:
//$block = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($data['identifier']);
$block = Mage::getModel('cms/block')->load($data['identifier'])->isObjectNew();
if ($block) {
    // Create static block:
    Mage::getModel('cms/block')->setData($data)->save();
}

 // ===== CMS Static Blocks =====
$data = array(
    'title' => 'Home banner',
    'identifier' => 'home_banner',
    'stores' => array(0), // Array with store ID's
    'content' => '<div class="home-banner">
    <a class="inline" href="#inline_content">
    <img class="for-desktop" src="{{media url=\'homebanner/SN_BNR_Contest_1024x90.jpg\'}}" alt="home banner" />
    </a><a class="inline" href="#inline_content">
    <img class="for-mobile" src="{{media url=\'homebanner/SN_BNR_Contest_460x250.jpg\'}}" alt="home banner mobile" />
    </a></div>',
    'is_active' => 1
);

// Check if static block already exists:
//$block = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($data['identifier']);
$block = Mage::getModel('cms/block')->load($data['identifier'])->isObjectNew();

if ($block) {
    // Create static block:
    Mage::getModel('cms/block')->setData($data)->save();
}

$installer->endSetup();