<?php
$installer = $this;
$installer->startSetup();

/** @var Mage_Catalog_Model_Resource_Setup $model */
$model = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$message = 'For iDev commissions to work, SKU\'s can only contain a-z, 0-9, hyphen and underscore. Any other characters, including spaces, periods, commas, etc will not work.';
$model->updateAttribute('catalog_product', 'sku', 'note', $message);

$installer->endSetup();