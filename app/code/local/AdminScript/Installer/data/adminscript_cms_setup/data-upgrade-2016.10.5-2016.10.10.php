<?php
/**
 * @namespace   Crimson
 * @module      ${MODULE}
 * @author      Peter Talavera
 * @email       ptalavera@crimsonagility.com
 * @date        10/10/2016 9:19 AM
 * @brief
 */
$installer = $this;
$installer->startSetup();

$storeId    = 2;
$category = Mage::getModel('catalog/category');
$category->setStoreId($storeId);
$category->setName('Sale Products');
$category->setUrlKey('sale-products');
$category->setIsActive(1);
$category->setDisplayMode('PRODUCTS');
$category->setIncludeMenu(0);
$parentId = 2;
$parentCategory = Mage::getModel('catalog/category')->load($parentId);
$category->setPath($parentCategory->getPath());
$category->save();


$store = Mage::app()->getStore('default');

$page = Mage::getModel('cms/page');
$page->setStoreId($store->getId());
$page->load('home','identifier');

$content = <<<EOT
<div>{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="21"}}</div>
<div class="home-products">
<div class="top-sellers">
<h2>SALE PRODUCTS</h2>
<p>{{block type="catalog/product_list" category_id="25" template="catalog/product/list_tabs.phtml"}}</p>
</div>
<div class="featured-products">
<h2>FEATURED PRODUCTS</h2>
<p>{{block type="catalog/product_list" category_id="14" template="catalog/product/list_tabs.phtml"}}</p>
</div>
</div>
<p>{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="29"}}</p>
<div class="social">{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="24"}}</div>
EOT;

$cmsPage = Array (
    'title' => 'SparkNaturals.com',
    'identifier' => 'home',
    'content' => $content,
    'is_active' => 1,
    'stores' => array($store->getId())
);
if ($page->getId()) {
    $page->setContent($cmsPage ['content']);
    $page->setTitle($cmsPage ['title']);
    $page->save();
} else {
    Mage::getModel('cms/page')->setData($cmsPage)->save();
}


$installer->endSetup();