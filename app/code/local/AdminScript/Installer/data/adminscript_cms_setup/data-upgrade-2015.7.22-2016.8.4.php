<?php
$installer = $this;
$installer->startSetup();

//$model =  new Mage_Catalog_Model_Resource_Setup();
$model = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$model->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'sample_file', array(
    'group' => 'General',    
    'attribute_set' => 'Default', // Your custom Attribute set    
    'label' => 'Sample file',
    'input' => 'jvs_file',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,    
    'visible_on_front' => true,
    'required' => false,
));


$installer->endSetup();