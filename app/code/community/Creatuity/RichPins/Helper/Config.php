<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Helper_Config extends Mage_Core_Helper_Abstract {

    const XPATH_ENABLED = 'richpins/general/enabled';
    const XPATH_BUTTON_EXISTS = 'richpins/general/button_exists';
    const XPATH_METHOD = 'richpins/general/method';

    /**
     * Returns value of "Enabled" configuration field
     * 
     * @return bool
     */
    public function isExtensionEnabled() {
        return Mage::getStoreConfig(self::XPATH_ENABLED);
    }

    /**
     * Returns value of "I already have Pin It button on the product page" configuration field
     * 
     * @return bool
     */
    public function buttonExists() {
        return Mage::getStoreConfig(self::XPATH_BUTTON_EXISTS);
    }
    
    /**
     * Returns value of "Method" configuration field
     * 
     * @return string
     */
    public function getSelectedMethod() {
        return Mage::getStoreConfig(self::XPATH_METHOD);
    }
    
}
