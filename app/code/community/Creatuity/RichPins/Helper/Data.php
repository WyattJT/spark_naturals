<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Displays Pinterest button block for the particular product
     * 
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function displayPinterestButton(Mage_Catalog_Model_Product $product) {
        return $this->_createPinterestButton($product)->toHtml();
    }

    /**
     * Returns Pinterest button block for the particular product
     * 
     * @param Mage_Catalog_Model_Product $product
     * @return Creatuity_RichPins_Block_Pinterest_Button
     */
    protected function _createPinterestButton(Mage_Catalog_Model_Product $product) {
        $block = Mage::app()->getLayout()->createBlock('richpins/pinterest_catalog_button');
        $block->setProduct($product);
        return $block;
    }

}
