<?php

class Creatuity_RichPins_Model_Adminhtml_System_Config_Source_Method {

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return array(
            array('value' => 'tags', 'label' => Mage::helper('richpins')->__('Open Graph Tags')),
            array('value' => 'oembed', 'label' => Mage::helper('richpins')->__('oEmbed')),
        );
    }

}
