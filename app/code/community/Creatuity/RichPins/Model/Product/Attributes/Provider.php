<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Model_Product_Attributes_Provider extends Varien_Object {

    protected $_definition;

    public function __construct(array $data = array()) {
        if (empty($data['definition'])) {
            $this->_definition = Mage::getSingleton('richpins/product_attributes_definition');
        } else {
            $this->_definition = $data['definition'];
        }
        parent::__construct($data);
    }

    public function getValues($area, $group, Varien_Object $params) {
        $values = array();

        if ($params === null) {
            $params = new Varien_Object();
        }

        $params->setArea($area);
        $params->setGroup($group);

        $defs = $this->_getMatchingDefinitions($area, $group);
        foreach ($defs as $def) {
            if ($this->_isDefinitionEnabled($def)) {
                $values[$def['code']] = $this->_getValue($def, $params);
            }
        }
        return $values;
    }

    protected function _getValue($definition, $params) {
        $model = $definition['value_callback'][0];
        $modelObj = $definition['value_callback_mode'] != 'singleton' ? Mage::getModel($model) : Mage::getSingleton($model);

        $method = $definition['value_callback'][1];

        $callback = array($modelObj, $method);
        if (!is_callable($callback)) {
            Mage::throwException(sprintf('Defined callback for "%s" definition is not correct.', $definition['id']));
        }

        return call_user_func($callback, $params);
    }

    protected function _getMatchingDefinitions($area, $group) {
        $allDefinitions = $this->_definition->getAttributesDefinition();

        $defs = array();
        foreach ($allDefinitions as $id => $definition) {
            if (isset($definition['group']) && $definition['group'] == $group) {

                $areaSettings = array();
                $parentSettings = array();

                if (isset($definition['parent'])) {
                    $parentSettings = $allDefinitions[$definition['parent']];

                    if (isset($parentSettings['areas'][$area])) {
                        $areaSettings = $parentSettings['areas'][$area];
                    }
                }
                $def = $definition + $parentSettings + $areaSettings;
                $def['id'] = $id;
                $defs[] = array_diff_key($def, array_flip(
                                array(
                                    'is_virtual',
                                    'parent',
                                    'areas',
                        )));
            }
        }
        return $defs;
    }

    protected function _isDefinitionEnabled($definition) {
        return (isset($definition['enabled']) && $definition['enabled'])
                || (isset($definition['enabled_config_path']) && Mage::getStoreConfig($definition['enabled_config_path']));
    }

}
