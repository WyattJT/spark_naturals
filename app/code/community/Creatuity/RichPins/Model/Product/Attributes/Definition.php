<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Model_Product_Attributes_Definition extends Varien_Object {

    const XPATH_SHARE_SITE_NAME = 'richpins/general/share_site_name';
    const XPATH_SHARE_URL = 'richpins/general/share_url';
    const XPATH_SHARE_PRODUCT_DESCRIPTION = 'richpins/general/share_product_description';
    const XPATH_SHARE_STOCK_AVAILABILITY = 'richpins/general/share_stock_availability';

    public function getAttributesDefinition() {
        return array(
            'title' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:title',
                        'enabled' => true,
                    ),
                    'oembed' => array(
                        'code' => 'title',
                        'enabled' => true,
                    ),
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getProductName'),
                'value_callback_mode' => 'singleton',
            ),
            
            'title_simple' => array(
                'parent' => 'title',
                'group' => 'simple_product',
            ),
            
            'title_parent' => array(
                'parent' => 'title',
                'group' => 'parent_product',
            ),
            
            'title_child' => array(
                'parent' => 'title',
                'group' => 'child_product',
            ),
            
            'price_amount' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:price:amount',
                        'enabled' => true,
                    ),
                    'oembed' => array(
                        'code' => 'price',
                        'enabled' => true,
                    )
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getProductPriceAmount'),
                'value_callback_mode' => 'singleton',
            ),
            
            'price_amount_simple' => array(
                'parent' => 'price_amount',
                'group' => 'simple_product',
            ),
            
            'price_amount_child' => array(
                'parent' => 'price_amount',
                'group' => 'child_product',
            ),
            
            'price_amount_child_configurable' => array(
                'parent' => 'price_amount',
                'group' => 'child_product_type_configurable',
                'value_callback' => array('richpins/product_attributes_values', 'getConfigurableProductPriceAmount'),
            ),
            
            'currency' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:price:currency',
                        'enabled' => true,
                    ),
                    'oembed' => array(
                        'code' => 'currency_code',
                        'enabled' => true,
                    ),
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getProductPriceCurrency'),
                'value_callback_mode' => 'singleton',
            ),
            
            'currency_simple' => array(
                'parent' => 'currency',
                'group' => 'simple_product',
            ),
            
            'currency_child' => array(
                'parent' => 'currency',
                'group' => 'child_product',
            ),
            
            'site_name' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:site_name',
                        'enabled_config_path' => self::XPATH_SHARE_SITE_NAME,
                    ),
                    'oembed' => array(
                        'code' => 'provider_name',
                        'enabled_config_path' => self::XPATH_SHARE_SITE_NAME,
                    ),
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getSiteName'),
                'value_callback_mode' => 'singleton',
            ),
            
            'site_name_simple' => array(
                'parent' => 'site_name',
                'group' => 'simple_product',
            ),
            
            'site_name_main' => array(
                'parent' => 'site_name',
                'group' => 'main_product_info',
            ),
            
            'url' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:url',
                        'enabled_config_path' => self::XPATH_SHARE_URL,
                    ),
                    'oembed' => array(
                        'code' => 'url',
                        'enabled' => true,
                    ),
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getProductUrl'),
                'value_callback_mode' => 'singleton',
            ),
            
            'url_simple' => array(
                'parent' => 'url',
                'group' => 'simple_product',
            ),
            
            'url_main' => array(
                'parent' => 'url',
                'group' => 'main_product_info',
            ),
            
            'description' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:description',
                        'enabled_config_path' => self::XPATH_SHARE_PRODUCT_DESCRIPTION,
                    ),
                    'oembed' => array(
                        'code' => 'description',
                        'enabled_config_path' => self::XPATH_SHARE_PRODUCT_DESCRIPTION,
                    ),
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getProductDescription'),
                'value_callback_mode' => 'singleton',
            ),
            
            'description_simple' => array(
                'parent' => 'description',
                'group' => 'simple_product',
            ),
            
            'description_parent' => array(
                'parent' => 'description',
                'group' => 'parent_product',
            ),
            
            'description_child' => array(
                'parent' => 'description',
                'group' => 'child_product',
            ),
            
            'availability' => array(
                'is_virtual' => true,
                'areas' => array(
                    'metatag' => array(
                        'code' => 'og:availability',
                        'enabled_config_path' => self::XPATH_SHARE_STOCK_AVAILABILITY,
                    ),
                    'oembed' => array(
                        'code' => 'availability',
                        'enabled_config_path' => self::XPATH_SHARE_STOCK_AVAILABILITY,
                    ),
                ),
                'value_callback' => array('richpins/product_attributes_values', 'getStockAvailability'),
                'value_callback_mode' => 'singleton',
            ),
            
            'availability_simple' => array(
                'parent' => 'availability',
                'group' => 'simple_product',
            ),
            
            'availability_child' => array(
                'parent' => 'availability',
                'group' => 'child_product',
            ),
        );
    }

}