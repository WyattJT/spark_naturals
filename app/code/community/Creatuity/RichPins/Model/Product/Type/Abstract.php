<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
abstract class Creatuity_RichPins_Model_Product_Type_Abstract extends Varien_Object {

    abstract protected function _getValues();
    
    public function getJson() {
        return $this->_toJson($this->_getValues());
    }

    protected function _toJson($values) {
        return Mage::helper('core')->jsonEncode($values);
    }

}
