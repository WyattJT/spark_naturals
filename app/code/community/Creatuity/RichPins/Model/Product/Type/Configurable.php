<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Model_Product_Type_Configurable extends Creatuity_RichPins_Model_Product_Type_Multiple {

    protected function _getAttributesArray() {
        $attributes = array();
        $options = array();

        foreach ($this->_getAllowProducts() as $product) {
            $productId = $product->getId();

            foreach ($this->_getAllowAttributes() as $attribute) {
                $productAttribute = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }
                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
            }
        }

        foreach ($this->_getAllowAttributes() as $attribute) {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();
            $info = array(
                'id' => $productAttribute->getId(),
                'code' => $productAttribute->getAttributeCode(),
                'label' => $productAttribute->getLabel(),
                'options' => array(),
            );

            $optionPrices = array();
            $prices = $attribute->getPrices();
            if (is_array($prices)) {
                foreach ($prices as $value) {
                    if (!$this->_validateAttributeValue($attributeId, $value, $options)) {
                        continue;
                    }
                    $configurablePrice = $this->_preparePrice($value['pricing_value'], $value['is_percent']);
                    if (isset($options[$attributeId][$value['value_index']])) {
                        $productsIndex = $options[$attributeId][$value['value_index']];
                    } else {
                        $productsIndex = array();
                    }

                    $info['options'][] = array(
                        'id' => $value['value_index'],
                        'price' => $configurablePrice,
                        'products' => $productsIndex,
                    );
                    $optionPrices[] = $configurablePrice;
                }
            }
            if ($this->_validateAttributeInfo($info)) {
                $attributes[$attributeId] = $info;
            }
        }

        return $attributes;
    }

    protected function _getAllowProducts() {
        $products = array();
        $skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
        $allProducts = $this->getProduct()->getTypeInstance(true)
                ->getUsedProducts(null, $this->getProduct());
        foreach ($allProducts as $product) {
            if ($product->isSaleable() || $skipSaleableCheck) {
                $products[] = $product;
            }
        }
        return $products;
    }

    protected function _getAllowAttributes() {
        return $this->getProduct()->getTypeInstance(true)->getConfigurableAttributes($this->getProduct());
    }

    protected function _validateAttributeValue($attributeId, &$value, &$options) {
        if (isset($options[$attributeId][$value['value_index']])) {
            return true;
        }
        return false;
    }

    protected function _preparePrice($price, $isPercent = false) {
        if ($isPercent && !empty($price)) {
            $price = $this->getProduct()->getFinalPrice() * $price / 100;
        }
        return $this->_convertPrice($price, true);
    }

    protected function _validateAttributeInfo(&$info) {
        if (count($info['options']) > 0) {
            return true;
        }
        return false;
    }

    protected function _convertPrice($price, $round = false) {
        if (empty($price)) {
            return 0;
        }
        $price = Mage::app()->getStore()->convertPrice($price);
        if ($round) {
            $price = Mage::app()->getStore()->roundPrice($price);
        }
        return $price;
    }

    protected function _getChildProductValues($child) {
        $specificForConfigurableProductsValues = $this->getProvider()->getValues(
                'oembed', 'child_product_type_configurable', new Varien_Object(array(
                    'product' => $child,
                    'attributes_array' => $this->_getAttributesArray(),
                    'parent_price' => $this->getProduct()->getFinalPrice()
                ))
        );

        return $specificForConfigurableProductsValues +
                $this->getProvider()->getValues(
                        'oembed', 'child_product', new Varien_Object(array('product' => $child))
        );
    }

    protected function _getChildren() {
        return Mage::getModel('catalog/product_type_configurable')
                        ->getUsedProducts(null, $this->getProduct());
    }

}
