<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Model_Product_Type_Factory {

    /**
     *
     * @param Mage_Catalog_Model_Product $product
     * @return Creatuity_RichPins_Model_Product_Type_Abstract
     */
    public function createProductType($product) {
        $params = array(
            'product' => $product,
            'provider' => Mage::getModel('richpins/product_attributes_provider'),
        );
        $modelClassName = 'richpins/product_type_' . $product->getTypeId();
        $model = Mage::getModel($modelClassName, $params);
        
        return $model instanceof Creatuity_RichPins_Model_Product_Type_Abstract ?
            $model : Mage::getModel('richpins/product_type_simple', $params);
    }

}
