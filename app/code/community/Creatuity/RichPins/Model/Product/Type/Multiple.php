<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
abstract class Creatuity_RichPins_Model_Product_Type_Multiple extends Creatuity_RichPins_Model_Product_Type_Abstract {

    protected function _getValues() {
        $params = new Varien_Object(array('product' => $this->getProduct()));
        $result = $this->getProvider()->getValues('oembed', 'main_product_info', $params);
        $productItem = $this->getProvider()->getValues('oembed', 'parent_product', $params);

        $childItems = array();
        $childProducts = $this->_getChildren();

        foreach ($childProducts as $child) {
            $childItems[] = $this->_getChildProductValues($child);
        }
        $productItem['offers'] = $childItems;
        $result['products'][] = $productItem;

        return $result;
    }

    abstract protected function _getChildren();

    abstract protected function _getChildProductValues($child);
}
