<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Model_Product_Type_Grouped extends Creatuity_RichPins_Model_Product_Type_Multiple {

    protected function _getChildProductValues($child) {
        return $this->getProvider()->getValues(
                        'oembed', 'child_product', new Varien_Object(array('product' => $child))
        );
    }

    protected function _getChildren() {
        return $this->getProduct()->getTypeInstance(true)
                        ->getAssociatedProducts($this->getProduct());
    }

}
