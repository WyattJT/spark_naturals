<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_OembedController extends Mage_Core_Controller_Front_Action
{
    const MAGENTO_OLD_REWRITE_APPROACH_VERSION = '1.13';
    
    
    public function indexAction()
    {

        if ($this->_getConfigHelper()->getSelectedMethod() != 'oembed') {
            $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found', true);
            $this->getResponse()->setHeader('Status', '404 File not found', true);

            $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultNoRoute');
            }
        } else {
            if (Mage::app()->getRequest()->has('url')) {
                $url = Mage::app()->getRequest()->getParam('url');

                $product = $this->_getProductFromUrl($this->_prepareUrl($url));

                if ($product->getId()) {
                    $json = Mage::getModel('richpins/product_type_factory')->createProductType($product)->getJson();
                    $this->getResponse()->setHeader('Content-Type', 'application/json');
                    $this->getResponse()->setBody($json);
                } else {
                    $this->getResponse()->setBody('oEmbed URL not recognized type');
                    $this->getResponse()->setHttpResponseCode(404);
                }
            } else {
                $this->getResponse()->setBody("Expected param 'url'.");
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
    }
    
    protected function _prepareUrl($url)
    {
        $search = array(
            Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB),
            'index.php/',
        );
        $url = str_replace($search, '', $url);
        return $this->_isOldRewrite() ? $url : $this->_getSystemPaths($url);
    }

    protected function _getProductFromUrl($url)
    {
        return Mage::getModel('catalog/product')->load( 
            $this->_isOldRewrite() ? $this->_productIdByOldRewrite($url) : $this->_productIdByNewRewrite($url) 
        );
    }
    
    protected function _productIdByOldRewrite($url)
    {
        /* @var $rewrite Mage_Core_Model_Url_Rewrite */
        $rewrite = Mage::getModel('core/url_rewrite');
        $rewrite->setStoreId(Mage::app()->getStore()->getId())
            ->loadByRequestPath($url);
        
        return $rewrite->getProductId();
    }
    
    protected function _productIdByNewRewrite($url)
    {
        /* @var $rewrite Enterprise_UrlRewrite_Model_Url_Rewrite */
        $rewrite = Mage::getModel('enterprise_urlrewrite/url_rewrite');
        $rewrite->loadByRequestPath($url, Mage::app()->getStore()->getId());

        $rewriteRedirectTypeId = class_exists('Enterprise_UrlRewrite_Model_Redirect') 
            && defined('Enterprise_UrlRewrite_Model_Redirect::URL_REWRITE_ENTITY_TYPE')
            ? Enterprise_UrlRewrite_Model_Redirect::URL_REWRITE_ENTITY_TYPE : 1;
        
        $rewriteProductTypeId = class_exists('Enterprise_Catalog_Model_Product') 
            && defined('Enterprise_Catalog_Model_Product::URL_REWRITE_ENTITY_TYPE')
            ? Enterprise_Catalog_Model_Product::URL_REWRITE_ENTITY_TYPE : 3;
        
        $productId = null;
        switch ( $rewrite->getEntityType() ) {
            case $rewriteRedirectTypeId:
                /* @var $redirect Enterprise_UrlRewrite_Model_Redirect */
                $redirect = Mage::getModel('enterprise_urlrewrite/redirect');
                $redirect->load( $rewrite->getValueId() );
                $productId = $redirect->getProductId();
            break;
        
            case $rewriteProductTypeId:
                $productId = $this->_enterpriseProductResource()->getProductIdByRewrite(
                    $rewrite->getId(), Mage::app()->getStore()->getId()
                );
            break;
        }
        
        return $productId;
    }
    
    protected function _isOldRewrite()
    {
        return version_compare(Mage::getVersion(), self::MAGENTO_OLD_REWRITE_APPROACH_VERSION, '<');
    }
    
    /**
     * @return array
     */
    protected function _getSystemPaths($requestPath)
    {
        $parts = explode('/', $requestPath);
        $suffix = array_pop($parts);
        if (false !== strrpos($suffix, '.')) {
            $suffix = substr($suffix, 0, strrpos($suffix, '.'));
        }
        $paths = array('request' => $requestPath, 'suffix' => $suffix);
        if (count($parts)) {
            $paths['whole'] = implode('/', $parts) . '/' . $suffix;
        }
        return $paths;
    }

    /**
     *
     * @return Creatuity_RichPins_Helper_Config
     */
    protected function _getConfigHelper()
    {
        return Mage::helper('richpins/config');
    }

    /**
     * @return Enterprise_Catalog_Model_Resource_Product
     */
    protected function _enterpriseProductResource()
    {
        return Mage::getResourceSingleton('enterprise_catalog/product');
    }
}