<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
abstract class Creatuity_RichPins_Block_Abstract extends Mage_Core_Block_Template {

    const XPATH_ENABLED = 'richpins/general/enabled';
    const XPATH_BUTTON_EXISTS = 'richpins/general/button_exists';
    const XPATH_METHOD = 'richpins/general/method';
    
    protected function _isEnabled() {
        return $this->_getConfigHelper()->isExtensionEnabled();
    }
    
    /**
     *
     * @return Creatuity_RichPins_Helper_Config
     */
    protected function _getConfigHelper() {
        return Mage::helper('richpins/config');
    }
}
