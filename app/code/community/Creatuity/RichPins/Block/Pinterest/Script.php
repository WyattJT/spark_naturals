<?php

/**
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Block_Pinterest_Script extends Creatuity_RichPins_Block_Abstract {

    protected function _construct() {
        if ($this->_isEnabled()) {
            $this->setTemplate('creatuity/richpins/pinterest/script.phtml');
        }
    }

    protected function _isEnabled() {
        return parent::_isEnabled() && !$this->_getConfigHelper()->buttonExists();
    }

}
