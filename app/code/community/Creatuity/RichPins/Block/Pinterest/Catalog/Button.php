<?php

/**
 * Block for Pinterest button on catalog page
 * 
 * @category       Creatuity
 * @package        Creatuity_RichPins
 * @copyright      Copyright (c) 2013 Creatuity Corp
 * @license        http://creatuity.com/license/
 */
class Creatuity_RichPins_Block_Pinterest_Catalog_Button extends Creatuity_RichPins_Block_Pinterest_Button {

    /**
     * Get URL of product's image
     * 
     * @return string 
     */
    public function getProductMedia() {
        if ($this->getProduct()
                && $this->getProduct()->getId()) {
            return($this->helper('catalog/image')->init($this->getProduct(), 'thumbnail')->resize(265));
        }
        return '';
    }


}
