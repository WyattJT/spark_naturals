<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Helper_Routing extends Mage_Core_Helper_Abstract 
{
    
    public function routes() 
    {
        return array(
            array(
                'name' => 'Product Details',
                'http_method' => 'GET',
                'regexp' => '^\/?products\/(?P<item_group_id>\S+)\/?$',
                'action_class' => 'buyablepins/endpoint_action_productDetails',
            ),
            array(
                'name' => 'Cart Creation',
                'http_method' => 'POST',
                'regexp' => '^\/?carts\/?$',
                'action_class' => 'buyablepins/endpoint_action_cartCreate',
            ),
            array(
                'name' => 'Get Cart',
                'http_method' => 'GET',
                'regexp' => '^\/?carts\/(?P<cart_id>[\w\-\.\_\~\:\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=]+)\/?$',
                'action_class' => 'buyablepins/endpoint_action_getCart',
            ),
            array(
                'name' => 'Cart Update',
                'http_method' => 'PATCH',
                'regexp' => '^\/?carts\/(?P<cart_id>[\w\-\.\_\~\:\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=]+)\/?$',
                'action_class' => 'buyablepins/endpoint_action_cartUpdate',
            ),
            array(
                'name' => 'Update Partial Shipping Address',
                'http_method' => 'GET',
                'regexp' => '^\/?carts\/(?P<cart_id>[\w\-\.\_\~\:\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=]+)\/available_shipping_options\/?$',
                'action_class' => 'buyablepins/endpoint_action_pullShippingOptions',
            ),
            array(
                'name' => 'Payment Pay',
                'http_method' => 'POST',
                'regexp' => '^\/?carts\/(?P<cart_id>[\w\-\.\_\~\:\?\#\[\]\@\!\$\&\'\(\)\*\+\,\;\=]+)\/pay\/?$',
                'action_class' => 'buyablepins/endpoint_action_pay',
            ),

            array(
                'name' => 'Payment Vault',
                'http_method' => 'POST',
                'regexp' => '^\/?vault\/?$',
                'action_class' => 'buyablepins/endpoint_action_vault',
            ),
        );
    }

    
}
