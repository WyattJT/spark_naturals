<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Helper_ResponseCodes extends Mage_Core_Helper_Abstract 
{
    const SUCCESS_GENERAL = 'general';
    const SUCCESS_CART_CREATE = 'cart_create';
    
    const ERROR_CART_ERROR = 'cart_error';
    const ERROR_CART_INFO_ERROR = 'cart_info_error';
    const ERROR_CART_ALREADY_PAID = 'cart_already_paid';
    const ERROR_CART_NOT_FOUND = 'cart_not_found';
    const ERROR_CART_EXPIRED = 'cart_expired';
    
    const ERROR_CART_COUPON_CODE_IS_INVALID = 'coupon_code_is_invalid';
    
    const ERROR_CART_ITEM_ERROR = 'cart_item_error';
    const ERROR_CART_ITEM_ID_NOT_FOUND = 'cart_item_id_not_found';
    const ERROR_CART_ITEM_OUT_OF_STOCK = 'cart_item_out_of_stock';
    const ERROR_CART_ITEM_QUANTITY_INVALID = 'cart_item_quantity_invalid';
    const ERROR_CART_ITEM_QUANTITY_LIMIT_EXCEEDED = 'cart_item_quantity_limit_exceeded';
    const ERROR_CART_SHIPPING_ERROR = 'cart_shipping_error';
    const ERROR_CART_SHIPPING_DESTINATION_NOT_SUPPORTED = 'cart_shipping_destination_not_supported';
    const ERROR_CART_SHIPPING_OPTION_ID_INVALID = 'cart_shipping_option_id_invalid';
    const ERROR_CART_SHIPPING_DETAIL_INVALID = 'cart_shipping_detail_invalid';
    const ERROR_CART_SHIPPING_DETAIL_FIRST_NAME_INVALID = 'cart_shipping_detail_first_name_invalid';
    const ERROR_CART_SHIPPING_DETAIL_LAST_NAME_INVALID = 'cart_shipping_detail_last_name_invalid';
    const ERROR_CART_SHIPPING_DETAIL_PHONE_INVALID = 'cart_shipping_detail_phone_invalid';
    const ERROR_CART_SHIPPING_DETAIL_EMAIL_INVALID = 'cart_shipping_detail_email_invalid';
    const ERROR_CART_SHIPPING_DETAIL_STREET_INVALID = 'cart_shipping_detail_street_invalid';
    const ERROR_CART_SHIPPING_DETAIL_EXTRA_STREET_INVALID = 'cart_shipping_detail_extra_street_invalid';
    const ERROR_CART_SHIPPING_DETAIL_LOCALITY_INVALID = 'cart_shipping_detail_locality_invalid';
    const ERROR_CART_SHIPPING_DETAIL_REGION_INVALID = 'cart_shipping_detail_region_invalid';
    const ERROR_CART_SHIPPING_DETAIL_COUNTRY_INVALID = 'cart_shipping_detail_country_invalid';
    const ERROR_CART_SHIPPING_DETAIL_POSTAL_CODE_INVALID = 'cart_shipping_detail_postal_code_invalid';
    const ERROR_PAYMENT_ERROR = 'payment_error';
    const ERROR_PAYMENT_INFO_ERROR = 'payment_info_error';
    const ERROR_PAYMENT_INFO_CURRENCY_INVALID = 'payment_info_currency_invalid';
    const ERROR_PAYMENT_INFO_VAULT_TOKEN_INVALID = 'payment_info_vault_token_invalid';
    const ERROR_PAYMENT_INFO_CREDIT_CARD_INVALID = 'payment_info_credit_card_invalid';
    const ERROR_PAYMENT_INFO_EXP_MONTH_INVALID = 'payment_info_exp_month_invalid';
    const ERROR_PAYMENT_INFO_EXP_YEAR_INVALID = 'payment_info_exp_year_invalid';
    const ERROR_PAYMENT_INFO_APPLE_PAY_DPAN_INVALID = 'payment_info_apple_pay_dpan_invalid';
    const ERROR_PAYMENT_INFO_APPLE_PAY_CRYPTOGRAM_INVALID = 'payment_info_apple_pay_cryptogram_invalid';
    const ERROR_PAYMENT_INFO_MULTIPLE_CARDS_NOT_ACCEPPTED = 'payment_info_multiple_cards_not_accepted';
    const ERROR_PAYMENT_BILLING_ERROR = 'payment_billing_error';
    const ERROR_PAYMENT_BILLING_ADDRESS_NOT_SUPPORTED = 'payment_billing_address_not_supported';
    const ERROR_PAYMENT_BILLING_FIRST_NAME_INVALID = 'payment_billing_first_name_invalid';
    const ERROR_PAYMENT_BILLING_LAST_NAME_INVALID = 'payment_billing_last_name_invalid';
    const ERROR_PAYMENT_BILLING_PHONE_INVALID = 'payment_billing_phone_invalid';
    const ERROR_PAYMENT_BILLING_EMAIL_INVALID = 'payment_billing_email_invalid';
    const ERROR_PAYMENT_BILLING_STREET_INVALID = 'payment_billing_street_invalid';
    const ERROR_PAYMENT_BILLING_EXTRA_STREET_INVALID = 'payment_billing_extra_street_invalid';
    const ERROR_PAYMENT_BILLING_LOCALITY_INVALID = 'payment_billing_locality_invalid';
    const ERROR_PAYMENT_BILLING_REGION_INVALID = 'payment_billing_region_invalid';
    const ERROR_PAYMENT_BILLING_COUNTRY_INVALID = 'payment_billing_country_invalid';
    const ERROR_PAYMENT_BILLING_POSTOAL_CODE_INVALID = 'payment_billing_postoal_code_invalid';
    const ERROR_PAYMENT_AUTH_FAILED = 'payment_auth_failed';
    const ERROR_PAYMENT_AUTH_BRAND_NOT_ACCEPTED = 'payment_auth_brand_not_accepted';
    const ERROR_PAYMENT_AUTH_TYPE_NOT_ACCEPTED = 'payment_auth_type_not_accepted';
    const ERROR_PAYMENT_AUTH_NOT_ENOUGH_CREDIT = 'payment_auth_not_enough_credit';
    const ERROR_PAYMENT_AUTH_CVV_INVALID = 'payment_auth_cvv_invalid';
    const ERROR_PAYMENT_AUTH_CARD_DECLINED = 'payment_auth_card_declined';
    const ERROR_PAYMENT_AUTH_CARD_DECLINED_FRAUD = 'payment_auth_card_declined_fraud';
    const ERROR_PAYMENT_AUTH_CARD_EXPIRED = 'payment_auth_card_expired';
    const ERROR_PAYMENT_AUTH_APPLE_PAY_NOT_ACCEPTED = 'payment_auth_apple_pay_not_accepted';
    const ERROR_AUTH_ERROR = 'auth_error';
    const ERROR_AUTH_ACCESS_TOKEN_INVALID = 'auth_access_token_invalid';
    const ERROR_AUTH_ACCESS_TOKEN_EXPIRED = 'auth_access_token_expired';
    const ERROR_INTERNAL_SERVICE_ERROR = 'internal_service_error';
    const ERROR_SERVICE_UNAVAILABLE = 'service_unavailable';
    
    /**
     * @var array lists all possible error_enum's together with their http codes 
     */
    protected $responseCodes = array
    (
        self::SUCCESS_GENERAL => 200,
        self::SUCCESS_CART_CREATE => 201,
        self::ERROR_CART_ERROR => 400,
        self::ERROR_CART_INFO_ERROR => 400,
        self::ERROR_CART_ALREADY_PAID => 400,
        self::ERROR_CART_NOT_FOUND => 404,
        self::ERROR_CART_EXPIRED => 404,
        self::ERROR_CART_ITEM_ERROR => 400,
        self::ERROR_CART_ITEM_ID_NOT_FOUND => 400,
        self::ERROR_CART_ITEM_OUT_OF_STOCK => 400,
        self::ERROR_CART_ITEM_QUANTITY_INVALID => 400,
        self::ERROR_CART_ITEM_QUANTITY_LIMIT_EXCEEDED => 400,
        self::ERROR_CART_SHIPPING_ERROR => 400,
        self::ERROR_CART_SHIPPING_DESTINATION_NOT_SUPPORTED => 400,
        self::ERROR_CART_SHIPPING_OPTION_ID_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_FIRST_NAME_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_LAST_NAME_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_PHONE_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_EMAIL_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_STREET_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_EXTRA_STREET_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_LOCALITY_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_REGION_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_COUNTRY_INVALID => 400,
        self::ERROR_CART_SHIPPING_DETAIL_POSTAL_CODE_INVALID => 400,
        self::ERROR_PAYMENT_ERROR => 400,
        self::ERROR_PAYMENT_INFO_ERROR => 400,
        self::ERROR_PAYMENT_INFO_CURRENCY_INVALID => 400,
        self::ERROR_PAYMENT_INFO_VAULT_TOKEN_INVALID => 400,
        self::ERROR_PAYMENT_INFO_CREDIT_CARD_INVALID => 400,
        self::ERROR_PAYMENT_INFO_EXP_MONTH_INVALID => 400,
        self::ERROR_PAYMENT_INFO_EXP_YEAR_INVALID => 400,
        self::ERROR_PAYMENT_INFO_APPLE_PAY_DPAN_INVALID => 400,
        self::ERROR_PAYMENT_INFO_APPLE_PAY_CRYPTOGRAM_INVALID => 400,
        self::ERROR_PAYMENT_INFO_MULTIPLE_CARDS_NOT_ACCEPPTED => 400,
        self::ERROR_PAYMENT_BILLING_ERROR => 400,
        self::ERROR_PAYMENT_BILLING_ADDRESS_NOT_SUPPORTED => 400,
        self::ERROR_PAYMENT_BILLING_FIRST_NAME_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_LAST_NAME_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_PHONE_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_EMAIL_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_STREET_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_EXTRA_STREET_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_LOCALITY_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_REGION_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_COUNTRY_INVALID => 400,
        self::ERROR_PAYMENT_BILLING_POSTOAL_CODE_INVALID => 400,
        self::ERROR_PAYMENT_AUTH_FAILED => 400,
        self::ERROR_PAYMENT_AUTH_BRAND_NOT_ACCEPTED => 400,
        self::ERROR_PAYMENT_AUTH_TYPE_NOT_ACCEPTED => 400,
        self::ERROR_PAYMENT_AUTH_NOT_ENOUGH_CREDIT => 400,
        self::ERROR_PAYMENT_AUTH_CVV_INVALID => 400,
        self::ERROR_PAYMENT_AUTH_CARD_DECLINED => 400,
        self::ERROR_PAYMENT_AUTH_CARD_DECLINED_FRAUD => 400,
        self::ERROR_PAYMENT_AUTH_CARD_EXPIRED => 400,
        self::ERROR_PAYMENT_AUTH_APPLE_PAY_NOT_ACCEPTED => 400,
        self::ERROR_AUTH_ERROR => 401,
        self::ERROR_AUTH_ACCESS_TOKEN_INVALID => 401,
        self::ERROR_AUTH_ACCESS_TOKEN_EXPIRED => 401,
        self::ERROR_INTERNAL_SERVICE_ERROR => 500,
        self::ERROR_SERVICE_UNAVAILABLE => 503,
    );
    
    public function isValidEnum($errorEnum) 
    {
        return isset($this->responseCodes[$errorEnum]);
    }
    
    public function ensureValidEnum($errorEnum) 
    {
        if (!$this->isValidEnum($errorEnum)) 
        {
            Mage::throwException(sprintf(
                "Invalid error enum: '%s'. You can use only one of:\n%s",
                $errorEnum,
                implode(PHP_EOL, array_keys($this->responseCodes))
            ));
        }
        return $errorEnum;
    }
    
    public function httpCodeOf($errorEnum) 
    {
        $this->ensureValidEnum($errorEnum);
        return $this->responseCodes[$errorEnum];
    }
    
}
