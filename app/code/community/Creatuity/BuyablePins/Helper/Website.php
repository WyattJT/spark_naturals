<?php

class Creatuity_BuyablePins_Helper_Website extends Mage_Core_Helper_Abstract
{
    const PINTEREST_STORE_CODE = 'buyablepins_pinterest';
    
    public function getPinterestWebsite()
    {
        return Mage::app()->getWebsite(self::PINTEREST_STORE_CODE);
    }
    
    public function getPinterestStore()
    {
        return Mage::app()->getStore(self::PINTEREST_STORE_CODE);
    }
    
    public function hasMultipleWebsites() 
    {
        return count($this->getAllNonPinterestWebsites()) > 1;
    }
    
    public function getAllNonPinterestWebsites()
    {
        $frontWebsites = Mage::app()->getWebsites(false, true);
        unset($frontWebsites[self::PINTEREST_STORE_CODE]);
        return (array)$frontWebsites;
    }
    
    
    public function getAllNonPinterestStores()
    {
        $frontStores = Mage::app()->getStores(false, true);
        unset($frontStores[self::PINTEREST_STORE_CODE]);
        return (array)$frontStores;
    }
    
    public function runInAdminStore($callable, array $params = array())
    {
        return $this->runInScope(0, $callable, $params);
    }
    
    public function runInPinterestStore($callable, array $params = array())
    {
        return $this->runInScope(self::PINTEREST_STORE_CODE, $callable, $params);
    }
    
    public function runInScope($store, $callable, array $params = array())
    {
        if (!is_callable( $callable )) {
            Mage::throwException('Not a callable');
        }
        
        $storeId = $this->helper()->toStoreId($store);
        
        $info = $this->emulation()->startEnvironmentEmulation($storeId);
        
        try{
            $result = call_user_func_array($callable, $params);
            
            $this->emulation()->stopEnvironmentEmulation($info);
            
            return $result;
        } catch (Exception $e) {
            $this->emulation()->stopEnvironmentEmulation($info);
            throw $e;
        } 
    }
    
    /**
     * @return Mage_Core_Model_App_Emulation
     */
    protected function emulation()
    {
        return Mage::getSingleton('core/app_emulation');
    }

    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper()
    {
        return Mage::helper('buyablepins');
    }
}

