<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Helper_Data extends Mage_Core_Helper_Abstract 
{
    
    const EMPTY_DUMMY_VALUE = '(none)';
    
    
    protected $logFile = 'creatuity_buyablepins.log';
    
    public function log($message) 
    {
        $forceLog = $this->config()->debugMode();
        Mage::log($message, null, $this->logFile, $forceLog);
    }
    
    public function logException(Exception $e) 
    {
        Mage::logException($e);
        $this->log("\n" . $e);
    }
    
    public function generalErrorData() 
    {
        return array(
            'error_enum' => 'internal_service_error', 
            'error_message' => 'Internal Service Error',
            'error_location' => null,
        );        
    }
    
    public function ensurePostcodeIsValid($postcode, $country) 
    {
        if (in_array(strtoupper($country), array('US', 'USA')) && !is_numeric( $postcode)) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid US Postcode", 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_POSTAL_CODE_INVALID
            );
        }
        if (! preg_match( '/^[\d]+\-?[\d]+$/', $postcode )) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid Postcode", 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_POSTAL_CODE_INVALID
            );
        }
    }


    public function ensureProductQtyValid($productId, $qty) 
    {
        if (!is_numeric( $qty ) || $qty <= 0) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Quantity must be positive number", 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_QUANTITY_INVALID
            );
        }
    }
    
    public function ensureProductExists($productId, $storeId) 
    {
        if (!$this->isProductExists( $productId, $storeId )) {
            $storeCode = Mage::app()->getStore($storeId)->getCode();
            throw new Creatuity_BuyablePins_Model_Exception(
                    "Cannot find '$productId' in '$storeCode ", 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_ID_NOT_FOUND );
        }
    }
    
    public function isProductExists($productId, $storeId) 
    {
        if (empty($productId)) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Product id cannot be empty", 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_ERROR );
        }
        return $this->_newProductCollection()
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->addIdFilter($productId)
            ->getSize() > 0;
    }
    
    public function toStoreId($store) 
    {
        try{
            if ($store === null) {
                $store = $this->config()->mainStoreCode();
            }
            return Mage::app()->getStore($store)->getId();
        } catch (Mage_Core_Model_Store_Exception $e) {
            throw new Creatuity_BuyablePins_Model_Exception("Unknown store: '${store}'", 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_NOT_FOUND, null, $e);
        }
    }
    
    public function toStoreCode($store) 
    {
        try{
            return Mage::app()->getStore($store)->getCode();
        } catch (Mage_Core_Model_Store_Exception $e) {
            throw new Mage_Core_Model_Store_Exception("Unknown store: '${store}'",
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_NOT_FOUND, null, $e);
        }
    }
    
    public function gtin(Mage_Catalog_Model_Product $product)
    {
        return $this->productAttributeValue($product, $this->config()->gtinRepresentedByAttribute());
    }

    public function brand(Mage_Catalog_Model_Product $product)
    {
        return $this->productAttributeValue($product, $this->config()->brandRepresentedByAttribute());
    }

    public function description(Mage_Catalog_Model_Product $product)
    {
        return $this->htmlToText($this->productAttributeValue($product, $this->config()->descriptionRepresentedByAttribute()));
    }

    public function color(Mage_Catalog_Model_Product $product)
    {
        return $this->productAttributeValue($product, $this->config()->colorRepresentedByAttribute());
    }
    
    public function size(Mage_Catalog_Model_Product $product)
    {
        return $this->productAttributeValue($product, $this->config()->sizeRepresentedByAttribute());
    }
    
    public function productAttributeValue(Mage_Catalog_Model_Product $product, $attrCode) 
    {
        if (!$product->hasData($attrCode) && $product->getDataUsingMethod($attrCode) === null) {
            if (!$product->getId()) {
                Mage::throwException("Cannot find, neither load '$attrCode' from product");
            }
            
            $attrValue = $this->_catalogResource()
                    ->getAttributeRawValue($product->getId(), $attrCode, $product->getStoreId());
            
            $product->setData($attrCode, $attrValue);
        }
        return $product->getDataUsingMethod($attrCode);
    }
    
    public function filterEmpty(array $data) 
    {
        $ret = array();
        foreach($data as $key => $val) {
            if (!$this->isEmpty( $val )) {
                $ret[$key] = $val;
            }
        }
        return $ret;
    }
    
    public function isEmpty($val) 
    {
        if (  is_numeric( $val) ) { 
            return false;
        }
        return $val == self::EMPTY_DUMMY_VALUE || empty($val);
    }
    
    public function htmlToText($html) 
    {
        $ret = htmlspecialchars_decode($html);
        $ret = strip_tags($ret);
        $ret = html_entity_decode($ret, ENT_NOQUOTES, 'UTF-8');
        return $ret;
    }
    
    public function formatPrice($price) 
    {
        if (empty($price)) {
            return '0.00';
        }
        return sprintf('%.02f', $price);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Mage_Catalog_Model_Resource_Product
     */
    protected function _catalogResource() 
    {
        return Mage::getResourceSingleton('catalog/product');
    }
    
    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function _newProductCollection() 
    {
        return Mage::getResourceModel('catalog/product_collection');
    }
    
}
 
