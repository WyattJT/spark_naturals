<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */

/* @var $installer Creatuity_BuyablePins_Model_Resource_Setup_Sales */
$installer = $this;

$installer->installInvalidCouponsField();
