<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Exception extends Exception
{
    protected $errorEnum;
    protected $errorLocation;
    
    
    public function __construct($message, $errorEnum = Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_INTERNAL_SERVICE_ERROR, $errorLocation = null, $causedBy = null)
    {
        parent::__construct($message, 0, $causedBy);
        
        $this->errorLocation = $errorLocation;
        $this->errorEnum = $errorEnum;
    }
    
    public function getErrorEnum()
    {
        return $this->errorEnum;
    }
    
    public function getErrorLocation()
    {
        return $this->errorLocation;
    }
}
