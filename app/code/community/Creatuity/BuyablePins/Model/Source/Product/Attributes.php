<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Source_Product_Attributes
{
    
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $attributes = $this->loadProductEntityType()->getAttributeCollection();
        $attributes->setOrder('attribute_code');
        
        $options = array();
        foreach ( $attributes as $attribute ) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute */
            $attributeLabel = $attribute->getStoreLabel();
            $options[] = array(
                'value' => $attribute->getAttributeCode(),
                'label' => isset($attributeLabel)
                    ? sprintf('%s (%s)', $attribute->getAttributeCode(), $attributeLabel)
                    : sprintf('%s', $attribute->getAttributeCode())
            );
        }
        return $options;
    }
    
    /**
     * @return Mage_Eav_Model_Entity_Type
     */
    protected function loadProductEntityType()
    {
        return Mage::getModel('eav/entity_type')->loadByCode(Mage_Catalog_Model_Product::ENTITY);
    }
}