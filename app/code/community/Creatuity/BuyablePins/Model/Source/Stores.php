<?php

/**
 *
 * @category   Creatuity
 * @package    BuyablePins
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Source_Stores
{
    
    public function toOptionArray()
    {
        $options = array();
        foreach(Mage::app()->getStores(false, true) as $store) {
            $storeName = $store->getName();
            $grName = $store->getGroup()->getName();
            $webName = $store->getWebsite()->getName();
            $options[] = array(
                'value' => $store->getCode(),
                'label' => "{$webName} -> {$grName} -> {$storeName} ",
            );
        }
        return $options;
    }
    
    
    
}
