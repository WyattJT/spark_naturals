<?php

class Creatuity_BuyablePins_Model_Source_Payment_Method
{
    
    public function toOptionArray()
    {
        $allMethods = Mage::getSingleton('payment/config')->getAllMethods();
        $availableMethods = array_flip($this->config()->availablePaymentMethods());
        $methods = array_intersect_key($allMethods, $availableMethods);
        
        $options = array();
        foreach ($methods as $code => $method) {
            $options[] = array(
                'value' => $code,
                'label' => sprintf('%s (%s)', $method->getTitle(), $code)
            );
        }
        return $options;
    }
    
    /**
     * 
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config()
    {
        return Mage::getModel('buyablepins/config');
    }
}