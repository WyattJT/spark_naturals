<?php

/**
 *
 * @category   Creatuity
 * @package    BuyablePins
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Source_Nonpinterest_Websites extends Creatuity_BuyablePins_Model_Source_Stores
{
    
    public function toOptionArray()
    {
        $options = array();
        foreach($this->websites()->getAllNonPinterestWebsites() as $website) {
            $options[] = array(
                'value' => $website->getCode(),
                'label' => $website->getName(),
            );
        }
        return $options;
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Website
     */
    protected function websites() {
        return Mage::helper('buyablepins/website');
    }
    
}
