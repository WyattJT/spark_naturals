<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Action_CartCreate
    extends Creatuity_BuyablePins_Model_Endpoint_Action_CartUpdate
{
    protected function doIt(Creatuity_BuyablePins_Model_Endpoint_Request $request)
    {
        $cartId = $this->cartService()->create($request->storeId());
        
        $this->_cartUpdate($request, $cartId);
        
        return $this->createCartResponse($cartId, $request->storeId(), 
                Creatuity_BuyablePins_Helper_ResponseCodes::SUCCESS_CART_CREATE);
    }
}