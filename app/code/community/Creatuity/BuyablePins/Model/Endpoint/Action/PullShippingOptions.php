<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Action_PullShippingOptions
    extends Creatuity_BuyablePins_Model_Endpoint_Action_Cart_Abstract
{
    
    protected function doIt(Creatuity_BuyablePins_Model_Endpoint_Request $request)
    {
        $cartId = $request->parameter('cart_id');
        $storeId = $request->storeId();
        
        $this->cartService()->pullShippingOptions($cartId, $storeId );
        
        return $this->createCartResponse($cartId, $storeId);
    }
    
}