<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Action_CartUpdate
    extends Creatuity_BuyablePins_Model_Endpoint_Action_Cart_Abstract
{
    protected function doIt(Creatuity_BuyablePins_Model_Endpoint_Request $request)
    {
        $cartId = $request->parameter('cart_id');
        
        $this->_cartUpdate($request, $cartId);
        
        return $this->createCartResponse($cartId, $request->storeId());
    }
    
    protected function _cartUpdate(Creatuity_BuyablePins_Model_Endpoint_Request $request, $cartId) 
    {
        if ($request->hasData('cart/cart_items')) {
            
            $this->cartService()->itemUpdate(
                $cartId, 
                $request->data('cart/cart_items'),
                $request->storeId()
            );
        }
        
        if ($request->hasData('cart/cart_shipping_detail')) {
            $this->cartService()->addShippingAddress(
                $cartId, 
                $request->data('cart/cart_shipping_detail'),
                $request->storeId()                    
            );
        }
        
        if ($request->hasData('cart/coupons')) {
            $this->cartService()->addCoupon(
                $cartId, 
                $request->data('cart/coupons'),
                $request->storeId()                    
            );
        }
        
        if ($request->hasData('cart/shipping_option_id')) {
            $this->cartService()->setShippingOption(
                $cartId, 
                $request->data('cart/shipping_option_id'),
                $request->storeId()                    
            );
        }
        
    }
}