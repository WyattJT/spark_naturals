<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Action_ProductDetails
    extends Creatuity_BuyablePins_Model_Endpoint_Action_Catalog_Abstract
{
    
    protected function doIt(Creatuity_BuyablePins_Model_Endpoint_Request $request)
    {
        $itemGroupId = $request->parameter('item_group_id');
        
        if ( empty($itemGroupId) ) {
            throw new Creatuity_BuyablePins_Model_Exception('"item_group_id" has not been provided');
        }
        
        return $this->createCatalogResponse(array(
            'item_group_id' => $itemGroupId,
            'products' => $this->catalogService()->loadProductsByItemGroupId($itemGroupId, $request->storeId()),
        ));
    }
}