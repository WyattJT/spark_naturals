<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
abstract class Creatuity_BuyablePins_Model_Endpoint_Action_Catalog_Abstract
    extends Creatuity_BuyablePins_Model_Endpoint_Action_Abstract
{
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_ProductDetails
     */
    protected function createCatalogResponse(array $data = array())
    {
        return Mage::getModel('buyablepins/endpoint_response_productDetails', $data);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Service_Catalog
     */
    protected function catalogService()
    {
        return Mage::getSingleton('buyablepins/service_catalog');
    }
}