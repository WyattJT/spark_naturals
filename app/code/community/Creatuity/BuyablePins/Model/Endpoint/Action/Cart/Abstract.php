<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
abstract class Creatuity_BuyablePins_Model_Endpoint_Action_Cart_Abstract
    extends Creatuity_BuyablePins_Model_Endpoint_Action_Abstract
{
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_CartDetails_Abstract
     */
    protected function createCartResponse(
        $cartId, 
        $storeId, 
        $responseCode = Creatuity_BuyablePins_Helper_ResponseCodes::SUCCESS_GENERAL
    ) {
        if ( $this->quotes()->isOrderCreated($cartId, $storeId) ) {
            return Mage::getModel('buyablepins/endpoint_response_orderDetails', array(
                'http_response_code' => $responseCode,
                'source' => $this->quotes()->provideOrder($cartId, $storeId),
            ));
        } else {
            return Mage::getModel('buyablepins/endpoint_response_cartDetails', array(
                'http_response_code' => $responseCode,
                'source' => $this->quotes()->provideQuote( $cartId, $storeId )
            ));
        }
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Service_Cart
     */
    protected function cartService()
    {
        return Mage::getSingleton('buyablepins/service_cart');
    }
}