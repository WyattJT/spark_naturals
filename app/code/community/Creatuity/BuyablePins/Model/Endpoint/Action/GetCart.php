<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Action_GetCart
    extends Creatuity_BuyablePins_Model_Endpoint_Action_Cart_Abstract
{
    protected function doIt(Creatuity_BuyablePins_Model_Endpoint_Request $request)
    {
        return $this->createCartResponse(
            $request->parameter('cart_id'),
            $request->storeId()
        );
    }
}