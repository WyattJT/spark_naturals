<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Action_Pay
    extends Creatuity_BuyablePins_Model_Endpoint_Action_CartUpdate
{
    
    protected function doIt(Creatuity_BuyablePins_Model_Endpoint_Request $request) 
    {
        $cartId = $request->parameter('cart_id');
        
        $this->_validateRequest($request, $cartId);
        
        $this->_cartUpdate($request, $cartId);

        $this->cartService()->addBillingAddress(
            $cartId, $request->data('cart/payment_tokens/0/billing_detail'), $request->storeId()
        );

        $this->cartService()->createOrder(
            $cartId, $request->storeId(), $request->data('cart/payment_tokens/0')
        );
        
        return $this->createCartResponse($cartId, $request->storeId());
    }
    
    protected function _validateRequest(Creatuity_BuyablePins_Model_Endpoint_Request $request, $cartId) 
    {
        
        if ($request->isEmptyData('cart/payment_tokens')) {
            throw new Creatuity_BuyablePins_Model_Exception(
                'Missing payment tokens', 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_ERROR
            );
        }
        
        if ( $request->hasData('cart/payment_tokens/1') ) {
            throw new Creatuity_BuyablePins_Model_Exception(
                'Only single payment is supported',
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_INFO_MULTIPLE_CARDS_NOT_ACCEPPTED
            );
        }
        
        $amountPaidFirst = $request->data('cart/payment_tokens/0/amount');
        $orderTotal = $this->cartService()->getTotal($cartId, $request->storeId());
        if ($amountPaidFirst != $orderTotal ) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Amount paid $amountPaidFirst does not match order total $orderTotal", 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_ERROR
            );
        }
        
        $expYearFirst = $request->data('cart/payment_tokens/0/exp_year');
        if (is_numeric($expYearFirst) && $expYearFirst < $this->date()->gmtDate('Y')) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Card expiration year cannot be past",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_INFO_EXP_YEAR_INVALID
            );
        }
        
    }
}