<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Router extends Varien_Object
{
    
    public function __construct(array $data = array()) 
    {
        parent::__construct($data + array(
            'routes' => $this->routingHelper()->routes()
        ));
    }
    
    
    public function find($requestHttpMethod, $requestPathInfo) 
    {
        foreach ($this->getRoutes() as $route) {
            $parameters = $this->checkRoute($route['regexp'], $requestPathInfo);
            if ($route['http_method'] == $requestHttpMethod && $parameters !== false) {
                return array(
                    'action_class' => $route['action_class'],
                    'request_parameters' => $parameters,
                );
            }
        }
        throw new Creatuity_BuyablePins_Model_Exception("Unknown action: {$requestHttpMethod}:{$requestPathInfo}");
    }
    
    protected function checkRoute($regexp, $requestPathInfo) 
    {
        $matches = array();
        if (!preg_match('/' . $regexp . '/', $requestPathInfo, $matches)) {
            return false;
        }
        
        $params = array();
        foreach($matches as $key => $val) {
            if (!is_int($key)) {
                $params[$key] = $val;
            }
        }
        return $params;
    }
    
    
    /**
     * @return Creatuity_BuyablePins_Helper_Routing
     */
    protected function routingHelper() 
    {
        return Mage::helper('buyablepins/routing');
    }
    
}
