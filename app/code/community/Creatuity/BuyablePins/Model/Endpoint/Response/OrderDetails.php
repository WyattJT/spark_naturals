<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Response_OrderDetails
    extends Creatuity_BuyablePins_Model_Endpoint_Response_CartDetails_Abstract
{
    protected function dataArray()
    {
        return array_merge_recursive(parent::dataArray(), $this->orderData());
    }
    
    protected function quoteId()
    {
        return $this->getSource()->getQuoteId();
    }

    public function orderData()
    {

        $payment = $this->getSource()->getPayment();

        $currency = $this->getSource()->getOrderCurrencyCode();
        $amount = $this->getSource()->getBaseGrandTotal();

        $id = $this->getSource()->getIncrementId();

        $paymentInfo = "$currency $amount from " . $payment->getMethodInstance()->getTitle();

        $paymentInfo = array(
            'id' => $id,
            'amount' => $this->formatPrice($amount),
            'currency' =>  $currency,
            'info' => $paymentInfo,
        );

        $paymentInfoArray = array();
        $paymentInfoArray[] = $paymentInfo;

        return array(
            'cart' => array(
                'order_id' => $this->getSource()->getIncrementId(),
                'order_status' => $this->getSource()->getState() != Mage_Sales_Model_Order::STATE_NEW ? 'paid' : '',
                'payment_status' => 'success',
                'payment_info' => $paymentInfoArray,
            )
        );
    }

    /**
     * @return string
     */
    protected function getCurrencyCode()
    {
        return $this->getSource()->getBaseCurrencyCode();
    }

    /**
     * @return string
     */
    protected function getShippingMethod()
    {
        return $this->getSource()->getShippingMethod();
    }
    
    protected function itemQty( $item )
    {
        return $item->getQtyOrdered() - (int)$item->getQtyOrdered() > 0 ? (float)$item->getQtyOrdered() : (int)$item->getQtyOrdered();
    }

    protected function shippingOption()
    {
        $shippingOption = array(
            'id' => $this->getShippingMethod(),
            'price' => $this->formatPrice( $this->getSource()->getShippingAmount() ),
            'title' => $this->getSource()->getShippingDescription(),
        );
        
        $hasDiscount = false;
        if($hasDiscount) {
            $shippingOption['discount'] = $this->shippingDiscountArray();
        }
        return $shippingOption;
    }
    
    protected function getTaxAmount()
    {
        return $this->getSource()->getBaseTaxAmount();
    }
    
    protected function getShippingAmount()
    {
        return $this->getSource()->getShippingAmount();
    }

    protected function shippingOptions()
    {
        return array();
    }
    
    protected function summaryArray()
    {
        return array(
            'base_amount' => $this->formatPrice( $this->getSource()->getBaseSubtotal() ),
            'discount_amount' => $this->formatPrice( - $this->getSource()->getBaseDiscountAmount() ),
            'final_amount' => $this->formatPrice( $this->getSource()->getBaseSubtotal() + $this->getSource()->getBaseDiscountAmount()),
            'tax_amount' => $this->formatPrice( $this->getTaxAmount() ),
            'shipping_cost' => $this->formatPrice( $this->getShippingAmount() ),
            'discount_shipping_cost' => $this->formatPrice( $this->getSource()->getBaseShippingDiscountAmount() ),
            'grand_total' => $this->formatPrice( $this->getSource()->getBaseGrandTotal() ),
        );
    }
    
    
    /**
     * @return Mage_Tax_Model_Resource_Sales_Order_Tax_Collection
     */
    protected function newOrderTaxCollection() {
        return Mage::getResourceModel('tax/order_tax_collection');
    }
    

    /**
     * @return Mage_Sales_Model_Order 
     */
    public function getSource()
    {
        if (!$this->getData('source') instanceof Mage_Sales_Model_Order) {
            Mage::throwException('Missing "order"');
        }
        return $this->getData('source');
    }
}