<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Response_Error extends Creatuity_BuyablePins_Model_Endpoint_Response_Abstract
{
    
    public function __construct(array $data) 
    {
        parent::__construct($data + $this->helper()->generalErrorData());
        
        $this->markAsFail(
            $this->getErrorEnum(),
            $this->getErrorMessage(),
            $this->getErrorLocation()
        );
    }
    
    protected function dataArray() 
    {
        return null;
    }
}