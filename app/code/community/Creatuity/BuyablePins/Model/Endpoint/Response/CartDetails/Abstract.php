<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method setQuote(Mage_Sales_Model_Quote $quote);
 */
abstract class Creatuity_BuyablePins_Model_Endpoint_Response_CartDetails_Abstract
    extends Creatuity_BuyablePins_Model_Endpoint_Response_Abstract
{
    protected $rules = null;

    
    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'http_response_code' => Creatuity_BuyablePins_Helper_ResponseCodes::SUCCESS_GENERAL,
        ));
    }
    
    protected function dataArray()
    {
        $data = array(
            'cart' => array(
                'id' => $this->cartId(),
                'create_time' => strtotime($this->getSource()->getCreatedAt()),
                'update_time' => strtotime($this->getSource()->getUpdatedAt()),
                'currency' => $this->getCurrencyCode(),
                'shipping_included' => true,
                'tax_included' => true,
                'coupons' => $this->couponArray(),
                'cart_shipping_detail' => null,
                'shipping_option' => $this->shippingOption(),
                'cart_items' => $this->cartItemsArray(),
                'available_shipping_options' => $this->shippingOptions(),
                'summary' => $this->summaryArray(),
            ),
        );
        
        $this->attachShippingAddress($data);
        $this->attachShippingOptionId($data);
        
        return $data;
    }
    
    protected function attachShippingAddress(array &$data)
    {
        $shippingData = $this->shippingArray();
        if ( !$this->onlyEmptyValues($shippingData) ) {
            $data['cart']['cart_shipping_detail'] = $shippingData;
        }
    }
    
    protected function attachShippingOptionId(array &$data)
    {
        $shippingOptionId = $this->getShippingMethod();
        if ( !empty($shippingOptionId) ) {
            $data['cart']['shipping_option_id'] = $shippingOptionId;
        }
    }
    
    protected function shippingArray()
    {
        $address = $this->getSource()->getShippingAddress();
        
        if ( !$address ) {
            return null;
        }
        
        
        return $this->helper()->filterEmpty(array(
            'phone' => $address->getTelephone(),
            'first_name' => $address->getFirstname(),
            'last_name' => $address->getLastname(),
            'email' => $address->getEmail(),
            'address' => $this->helper()->filterEmpty(array(
                'locality' => $address->getCity(),
                'region' => $address->getRegionCode(),
                'country' => $address->getCountry(),
                'postal_code' => $address->getPostcode(),
                'street' => $address->getStreet1(),
                'extra_street' => $address->getStreet2(),
            ))
        ));
    }

    protected function shippingDiscountArray()
    {
        $shippingDiscount = $this->getShippingDiscount();
        if(!$shippingDiscount) {
            return null;
        }
        
        $shippingPrice = $this->getShippingOptionPriceArray();
        return array_filter(array(
            'base_amount' => $this->formatPrice($shippingPrice['base_amount']),
            'discount_amount' => $this->formatPrice($shippingDiscount),
            'final_amount' => $this->formatPrice($shippingPrice['final_amount']),
            'source_coupon' => $this->couponShippingArray(),
        ));
    }

    protected function getShippingOptionPriceArray()
    {
        $shippingDiscount = $this->getShippingDiscount();
        $shippingBase = $this->getShippingAmount();
        $shippingFinal = $shippingBase - $shippingDiscount;
        
        return array(
            'base_amount' => $this->formatPrice( $shippingBase ),
            'discount_amount' => $this->formatPrice( $shippingDiscount ),
            'final_amount' => $this->formatPrice( $shippingFinal ),
        );
    }

    protected function getShippingDiscount()
    {
        $validCoupon = $this->validCouponEntry();
        
        if(empty($validCoupon['coupon_aux_data']['appliesToShipping']) 
            || !is_numeric($validCoupon['percent_off'])) {
            return null;
        }
        
        $shippingDiscountAmount = $this->getShippingAmount() * ($validCoupon['percent_off'] / 100);
        if($shippingDiscountAmount <= 0) {
            return null;
        }
        
        return $shippingDiscountAmount;
    }
    
    protected function couponShippingArray()
    {
        if ( !$this->getSource()->getCouponCode() ) {
            return null;
        }
        
        $rule = $this->loadCouponRule();
            
        if ( $rule->getSimpleFreeShipping() || $rule->getApplyToShipping() ) {
            $coupons = array_values($this->couponArray());
            $firstCoupon = $coupons[0];
            return $firstCoupon;
        }
        return null;
    }
    
    protected function couponArray()
    {
        $validCouponEntries = array();
        
        $validCouponEntry = $this->validCouponEntry();
        if ($validCouponEntry) {
            $validCouponEntries = array($validCouponEntry);
        }
        
        return array_merge($validCouponEntries, $this->invalidCouponEntries());
    }
    
    protected function validCouponEntry() {
        $rule = $this->loadCouponRule();
        
        if ( ! $rule ) {
            return array();
        }
            
        $data['id'] = $this->getSource()->getCouponCode();

        if ($rule->getSimpleAction() == 'by_percent') {
            $data['percent_off'] = (int)$rule->getDiscountAmount();
        }elseif($rule->getSimpleAction() == 'by_fixed') {
            $data['amount_off'] = $this->formatPrice( $rule->getDiscountAmount() );
        }

        if($rule->getSimpleAction() == 'cart_fixed') {
            $data['amount_off'] = $this->formatPrice( $rule->getDiscountAmount() );
        }

        if ($rule->getFromDate()) {
            $data['start_time'] = $this->date()->gmtTimestamp($rule->getFromDate());
        }

        if ($rule->getToDate()) {
            $data['expire_time'] = $this->date()->gmtTimestamp($rule->getToDate());
        }

        $data['coupon_aux_data'] = array(
            'valid' => true,
            'applied' => true,
            'action' => $rule->getSimpleAction(),
            'discountAmount' => $this->formatPrice($rule->getDiscountAmount()),
            'appliesToShipping' => (bool)$rule->getApplyToShipping(),
        );
        return $data;
    }
    
    protected function invalidCouponEntries() {
        $coupons = array();
        
        foreach ( $this->invalidCouponCodes() as $invalidCouponCode ) {
        // Pinterest has decided - for now - these coupons shouldn't appear
        /*
            $coupons[ ] = array(
                'id' => $invalidCouponCode,
                'coupon_aux_data' => array(
                    'valid' => false,
                    'applied' => false,
                )
            );
        */
        }
        return $coupons;
    }
        
    
    protected function invalidCouponCodes() {
        $invalidCoupons = $this->getSource()->getData('buyablepins_invalid_coupons');
        if (!$invalidCoupons) {
            return array();
        }
        return explode(',', $this->getSource()->getData('buyablepins_invalid_coupons'));
    }
    

    protected function cartItemsArray()
    {
        $items = array();
        foreach ( $this->getSource()->getAllItems() as $item ) {
            $newItem = array();
            $newItem['quantity'] = $this->itemQty($item);
            $newItem['item'] = $this->productDetailsResponse()->asArray( $item->getProduct() );
            if($item->getDiscountAmount() > 0) {
                $newItem['discounts'] = $this->discountsArray($item);
            }
            $newItem['base_amount'] = $this->formatPrice( $item->getBaseRowTotal() );
            $newItem['discount_amount'] = $this->formatPrice( $item->getDiscountAmount() );
            $newItem['final_amount'] = $this->formatPrice( $item->getBaseRowTotal() - $item->getDiscountAmount() );
            $newItem['item_taxes'] = $this->itemTaxesArray($item);
            $newItem['item_tax_total'] = $this->formatPrice( $item->getBaseTaxAmount() );
            $newItem['item_total'] = $this->formatPrice( $item->getBaseRowTotal() - $item->getDiscountAmount() + $item->getBaseTaxAmount() );
            $items[] = $newItem;
        }
        return $items;
    }
    
    protected function discountsArray($item) {
        if (!$item->getDiscountAmount() ) {
            return array();
        }

        $sourceCoupon = array();
        $validCouponEntry = $this->validCouponEntry();
        if ($validCouponEntry) {
            $sourceCoupon = array(
                'source_coupon' => $validCouponEntry,
            );
        }

        if(count($sourceCoupon) == 0) {
            return array(); // if no valid coupons are found, return an empty array
        }
        return array($sourceCoupon + array(
            'base_amount' => $this->formatPrice( $item->getBaseRowTotal() ),
            'discount_amount' => $this->formatPrice( $item->getDiscountAmount() ),
            'final_amount' => $this->formatPrice( $item->getBaseRowTotal() - $item->getDiscountAmount() ),
        ));
    }

    protected function getDiscountAmountForSummary() {
        $itemDiscounts = $this->getSource()->getBaseSubtotal() - $this->getSource()->getBaseSubtotalWithDiscount();
        $newDiscountAmount = $itemDiscounts - $this->getShippingDiscount();
        return $newDiscountAmount;
}

    protected function itemTaxesArray( $item )
    {
        if (!$item->getTaxPercent() || !$item->getBaseTaxAmount() ) {
            return array();
        }

        $rate = $this->formatPrice( $item->getTaxPercent() );
        if($rate == 0) {
            return array();
        }


        // Pinterest wants rates converted from, say, 8.25 to 0.0825
        if($rate > 2) {
            $rate = $rate / 100;
        }

        // todo: use sales_order_tax_item
        return array(array(
            'base' => $this->formatPrice( $item->getBaseRowTotal() - $item->getDiscountAmount() ),
            'amount' => $this->formatPrice( $item->getBaseTaxAmount() ),
            'rate' => (string)$rate,
            'title' => 'Tax',
        ));
        
    }
    
    
    /**
     * @return Mage_SalesRule_Model_Rule
     */
    protected function loadCouponRule() {
        $couponToFind = $this->getSource()->getCouponCode();
        
        foreach($this->loadRules() as $rule) {
            if ($rule->getCouponCode() == $couponToFind) {
                return $rule;
            }
            foreach($rule->getCoupons() as $coupon) {
                if ($coupon->getcode() == $couponToFind) {
                    return $rule;
                }
            }
        }
    }


    /**
     * @return Mage_SalesRule_Model_Rule[]
     */
    protected function loadRules()
    {
        if ( $this->rules === null ) {
            $this->rules = array();
            foreach(explode(',', (string)$this->getSource()->getAppliedRuleIds()) as $ruleId) {
                $rule = Mage::getModel('salesrule/rule')
                        ->setStore($this->getSource()->getStoreId())
                        ->load($ruleId);
                if ($rule->getId()) {
                    $this->rules[] = $rule;
                }
            }
        }
        return $this->rules;
    }
    
    
    /**
     * @return array
     */
    protected function cartId()
    {
        return $this->quotes()->toCartId($this->quoteId());
    }
    
    /**
     * @return float
     */
    abstract protected function getTaxAmount();
    
    /**
     * @return float
     */
    abstract protected function getShippingAmount();
    
    /**
     * @return Mage_Core_Model_Abstract
     */
    abstract public function getSource();
    
    
    /**
     * @return int
     */
    abstract protected function quoteId();
    
    /**
     * @return string
     */
    abstract protected function getCurrencyCode();
    
    /**
     * @return string
     */
    abstract protected function getShippingMethod();
    
    /**
     * @return array
     */
    abstract protected function shippingOption();
    
    
    /**
     * @return array
     */
    abstract protected function shippingOptions();
    
    
    /**
     * @return int|float
     */
    abstract protected function itemQty($item);
    
    /**
     * @return array
     */
    abstract protected function summaryArray();
    
    
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_ProductDetails
     */
    protected function productDetailsResponse(array $data = array())
    {
        return Mage::getModel('buyablepins/endpoint_response_productDetails', $data);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_PullShippingOptions
     */
    protected function pullShippingOptionResponse(array $data = array())
    {
        return Mage::getModel('buyablepins/endpoint_response_pullShippingOptions', $data);
    }
}
