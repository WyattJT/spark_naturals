<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method int getItemGroupId();
 * @method setItemGroupId(int);
 * @method Mage_Catalog_Model_Product[] getProducts();
 * @method setProducts(Mage_Catalog_Model_Product[] $products);
 */
class Creatuity_BuyablePins_Model_Endpoint_Response_ProductDetails
    extends Creatuity_BuyablePins_Model_Endpoint_Response_Abstract
{
    
    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'products' => array(),
            'item_groupd_id' => null,
        ));
    }
    
    protected function dataArray()
    {
        return array(
            'product' => array(
                'item_group_id' => $this->getItemGroupId(),
                'items' => $this->itemsArray(),
            )
        );
    }
    
    protected function itemsArray() 
    {
        $items = array();
        foreach ( $this->getProducts() as $product ) 
        {
            $items[] = $this->asArray($product);
        }
        return $items;
    }
    
    public function asArray(Mage_Catalog_Model_Product $product)
    {
        return array(
            'item_id' => $product->getId(),
            'item_group_id' => $this->getItemGroupId() ? $this->getItemGroupId() : $product->getId(),
            'price' => $this->formatPrice( $product->getFinalPrice() ),
            'sale_price' => $this->formatPrice( $product->getFinalPrice() ),
            'gtin' => (string)$this->helper()->gtin($product),
            'require_shipping' => $product->getTypeId() != Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE,
            'taxable' => $product->getTaxClassId() != 0,
            'title' => $product->getName(),
            'item_aux_data' => $product->getData(),
        );
    }
}
