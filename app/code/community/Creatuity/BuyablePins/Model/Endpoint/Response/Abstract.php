<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method int getHttpResponseCode();
 */
abstract class Creatuity_BuyablePins_Model_Endpoint_Response_Abstract extends Varien_Object
{
    protected $meta;
    
    
    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'http_response_code' => Creatuity_BuyablePins_Helper_ResponseCodes::SUCCESS_GENERAL,
        ));
    }
    
    protected function _construct()
    {
        $this->markAsSucceess();
    }
    
    public function responseArray()
    {
        return array(
            'meta' => (array)$this->meta,
            'data' => $this->dataArray(),
        );
    }
    
    public function markAsSucceess()
    {
        $this->responseCodes()->ensureValidEnum( $this->getHttpResponseCode() );
        
        $this->meta = array(
            'success' => true,
            'http_code' => $this->responseCodes()->httpCodeOf( $this->getHttpResponseCode() ),
        );
    }
    
    public function markAsFail($errorEnum, $errorMessage, $errorLocation = null )
    {
        $this->responseCodes()->ensureValidEnum($errorEnum);
        
        $this->meta = array(
            'success' => false,
            'http_code' => $this->responseCodes()->httpCodeOf($errorEnum),
            'error' => array(
                'error_enum' => $errorEnum,
                'error_message' => $errorMessage,
            ),
        );
        
        if ( !is_null($errorLocation) ) {
            $this->meta['error']['error_location'] = $errorLocation;
        }
    }
    
    public function isSuccess() 
    {
        return (bool)$this->meta['success'];
    }
    
    public function httpCode() 
    {
        return (int)$this->meta['http_code'];
    }
    
    protected function formatPrice($price)
    {
        return $this->helper()->formatPrice($price);
    }
    
    protected function onlyEmptyValues($arrayToCheck)
    {
        $filteredArray = array_filter($arrayToCheck);
        return empty($filteredArray);
    }
    
    /**
     * @return array
     */
    protected abstract function dataArray();
    
    /**
     * @return Mage_Core_Model_Date
     */
    protected function date() {
        return Mage::getSingleton('core/date');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_ResponseCodes
     */
    protected function responseCodes() 
    {
        return Mage::helper('buyablepins/responseCodes');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Quotes
     */
    protected function quotes() 
    {
        return Mage::getSingleton('buyablepins/quotes');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() {
        return Mage::helper('buyablepins');
    }
}