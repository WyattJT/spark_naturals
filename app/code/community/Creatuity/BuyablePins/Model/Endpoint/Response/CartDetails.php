<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Response_CartDetails
    extends Creatuity_BuyablePins_Model_Endpoint_Response_CartDetails_Abstract
{
    
    protected function quoteId()
    {
        return $this->getSource()->getId();
    }
    
    protected function getCurrencyCode()
    {
        return $this->getSource()->getQuoteCurrencyCode();
    }

    protected function getShippingMethod()
    {
        return $this->getSource()->getShippingAddress()->getShippingMethod();
    }
    
    protected function shippingOption()
    {
        $shippingMethod = $this->getSource()->getShippingAddress()->getShippingRateByCode(
            $this->getSource()->getShippingAddress()->getShippingMethod()
        );
        
        if ( !$shippingMethod ) {
            return null;
        }

        $shippingOption = array();
        $shippingOption['id'] = $shippingMethod->getCode();
        $shippingOption['price'] = $this->formatPrice( $shippingMethod->getPrice() );
        $shippingOption['title'] = $shippingMethod->getCarrierTitle() . ' ' . $shippingMethod->getMethodTitle();

        $shippingDiscount = $this->shippingDiscountArray();
        if($shippingDiscount) {
            $shippingOption['discount'] = $shippingDiscount;
        }

        return $shippingOption;
    }
    
    protected function itemQty( $item )
    {
        return $item->getQty();
    }
    
    protected function shippingOptions()
    {
        $rates = array();
        foreach ( $this->getSource()->getShippingAddress()->getAllShippingRates() as $rate ) {
            /* @var $rate Mage_Sales_Model_Quote_Address_Rate */
            $rates[] = array(
                'id' => $rate->getCode(),
                'price' => $this->formatPrice( $rate->getPrice() ),
                'title' => $rate->getCarrierTitle() . ' ' . $rate->getMethodTitle(),
            );
        }
        return $rates;
    }
    
    public function getShippingAmount()
    {
        if ( !$this->getSource()->getShippingAddress() ) {
            return 0;
        }
        return $this->getSource()->getShippingAddress()->getShippingAmount();
    }
    
    protected function getTaxAmount()
    {
        if ( !$this->getSource()->getShippingAddress() ) {
            return 0;
        }
        return $this->getSource()->getShippingAddress()->getBaseTaxAmount();
    }

    public function getTotal()
    {
        return $this->formatPrice( $this->getSource()->getBaseGrandTotal() );
    }

    protected function summaryArray()
    {
        $finalAmount = $this->getSource()->getBaseSubtotal() - $this->getDiscountAmountForSummary();
        return array(
            'base_amount' => $this->formatPrice( $this->getSource()->getBaseSubtotal() ),
            'discount_amount' => $this->formatPrice($this->getDiscountAmountForSummary() ),
            'final_amount' => $this->formatPrice( $finalAmount ),
            'tax_amount' => $this->formatPrice( $this->getTaxAmount() ),
            'shipping_cost' => $this->formatPrice( $this->getShippingAmount() ),
            'discount_shipping_cost' => $this->formatPrice( $this->getShippingDiscount() ),
            'grand_total' => $this->formatPrice( $this->getSource()->getBaseGrandTotal() ),
        );
    }
    

    /**
     * @return Mage_Sales_Model_Quote 
     */
    public function getSource()
    {
        if (!$this->getData('source') instanceof Mage_Sales_Model_Quote) {
            Mage::throwException('Missing "quote"');
        }
        return $this->getData('source');
    }

}
