<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Endpoint_Request extends Varien_Object
{
    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'parameters' => array(),
            'data' => array(),
            'store' => Mage::app()->getStore()->getId(),
        ));
    }
    
    /**
     * @return array
     */
    public function data($path = null)
    {
        return $this->arrayData('data', $path);
    }
    
    public function isEmptyData($path)
    {
        $data = $this->data($path);
        return $this->helper()->isEmpty($data);
    }
    
    /**
     * @return bool
     */
    public function hasData($key='') {
        if ($key === '') {
            return parent::hasData();
        }
        return $this->hasArrayData('data', $key);
    }
    
    /**
     * @return array
     */
    public function parameter($path = null)
    {
        return $this->arrayData('parameters', $path);
    }
    
    /**
     * @return bool
     */
    public function hasParameter($path) {
        return $this->hasArrayData('parameters', $path);
    }
    
    /**
     * @return int
     */
    public function storeId() {
        $store = $this->getStore();
        if (empty( $store )) {
            Mage::throwException("Store must be provided");
        }
        
        return $this->helper()->toStoreId($store);
    }
    
    protected function arrayData($name, $path) {
        return $this->getData($path 
            ? "$name/{$path}"
            : "$name"
        );
    }
    
    protected function hasArrayData($name, $path) {
        return $this->arrayData($name, $path) !== null;
    }
    
    /**
     * 
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() {
        return Mage::helper('buyablepins');
    }
    
}