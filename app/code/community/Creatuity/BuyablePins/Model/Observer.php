<?php

/**
 *
 * @category   Creatuity
 * @package    BuyablePins
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_Buyablepins_Model_Observer
{
    
    public function updateCacheAfterOrderCreate($observer) 
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();
                
        $this->quotes()->updateCacheAfterOrderCreate($quote, $order);
        
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Quotes
     */
    protected function quotes() 
    {
        return Mage::getSingleton('buyablepins/quotes');
    }
    
    
}
