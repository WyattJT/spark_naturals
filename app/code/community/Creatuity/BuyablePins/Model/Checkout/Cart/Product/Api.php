<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Checkout_Cart_Product_Api
    extends Mage_Checkout_Model_Cart_Product_Api
{
    
//    public function itemsWithQtys($quoteId, $store = null)
//    {
//        $quote = $this->_getQuote($quoteId, $store);
//
//        if (!$quote->getItemsCount()) {
//            return array();
//        }
//
//        $ret = array();
//        foreach ($quote->getAllItems() as $item) {
//            $ret[$item->getProductId()] = array(
//                'product_id' => $item->getProductId(),
//                'qty' => $item->getQty(),
//            );
//        }
//
//        return $ret;
//    }
//    
    
    /**
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote($quoteId, $store = null)
    {
        return $this->_quotes()->provideQuote($quoteId, $store);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Quotes
     */
    protected function _quotes() {
        return Mage::getSingleton('buyablepins/quotes');
    }
    
}
