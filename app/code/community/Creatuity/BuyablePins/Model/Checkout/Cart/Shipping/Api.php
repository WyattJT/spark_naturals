<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Checkout_Cart_Shipping_Api extends Mage_Checkout_Model_Cart_Shipping_Api
{
    
    /**
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote($quoteId, $store = null)
    {
        return $this->_quotes()->provideQuote($quoteId, $store);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Quotes
     */
    protected function _quotes() 
    {
        return Mage::getSingleton('buyablepins/quotes');
    }
        
}