<?php

/**
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Config extends Varien_Object
{
    const PRODUCT_FEED_FILENAME = 'products.xml';
    
    const GTIN_ATTR_CODE = 'buyablepins_gtin';
    
    const CREDIT_CARD_OTHER_TYPE = 'OT';
    
    const XML_CONFIG_DEBUG_MODE = 'buyablepins/general/debug_mode';
    const XML_CONFIG_AUTHORIZATION_TOKEN = 'buyablepins/general/authorization_token';
    const XML_CONFIG_CC_PAYMENT_OPTION = 'buyablepins/general/cc_payment_option';
    const XML_CONFIG_GTIN_REPRESENTATION = 'buyablepins/general/gtin_represented_by';
    const XML_CONFIG_BRAND_REPRESENTATION = 'buyablepins/general/brand_represented_by';
    const XML_CONFIG_DESCRIPTION_REPRESENTATION = 'buyablepins/general/description_represented_by';

    const XML_CONFIG_MAIN_STORE_CODE = 'buyablepins/general/main_store_code';
    const XML_CONFIG_FEED_APPROVED_STORE_CODE = 'buyablepins/product_feed/approved_store_code';
    const XML_CONFIG_FEED_ENDPOINT = 'buyablepins/product_feed/feed_endpoint';
    const XML_CONFIG_FEED_EXTERNAL_ID = 'buyablepins/product_feed/external_id';
    const XML_CONFIG_FEED_MERCHANT_ID = 'buyablepins/product_feed/merchant_id';
    const XML_CONFIG_FEED_ACCESS_TOKEN = 'buyablepins/product_feed/access_token';
    const XML_CONFIG_FEED_FREQUENCY = 'buyablepins/product_feed/feed_frequency';
    const XML_CONFIG_FEED_DIRECT_OUTPUT = 'buyablepins/product_feed/direct_output';

    const PINTEREST_MERCHANT_URL = 'https://commerce.pinterest.com/v3/commerce/merchants/';
    
    public function __construct(array $data = array())
    {
        return parent::__construct($data + array(
            'store_id' => null,
            'supported_payment_methods' => array(
                'authorizenet' => array(),
                'braintree' => array(
                    'rewrites' => array(
                        'braintree_payments/creditcard' => 'Creatuity_BuyablePins_Model_Payment_Method_Braintree',
                    )
                ),
                'creditcard' => array(
                    'rewrites' => array(
                        'creditcard/paymentLogic' => 'Creatuity_BuyablePins_Model_Payment_Method_Litle',
                    )
                ),
                'paypal_direct' => array(
                    'rewrites' => array(
                        'paypal/direct' => 'Creatuity_BuyablePins_Model_Payment_Method_PaypalWebsite',
                    )
                ),
            ),
        ));
    }
    
    public function debugMode()
    {
        return $this->getStoreConfigFlag(self::XML_CONFIG_DEBUG_MODE);
    }
    
    public function authToken()
    {
        return $this->getStoreConfig(self::XML_CONFIG_AUTHORIZATION_TOKEN);
    }
    
    public function gtinRepresentedByAttribute()
    {
        return $this->getStoreConfig(self::XML_CONFIG_GTIN_REPRESENTATION);
    }

    public function brandRepresentedByAttribute()
    {
        return $this->getStoreConfig(self::XML_CONFIG_BRAND_REPRESENTATION);
    }

    public function descriptionRepresentedByAttribute()
    {
        return $this->getStoreConfig(self::XML_CONFIG_DESCRIPTION_REPRESENTATION);
    }

    public function colorRepresentedByAttribute()
    {
        return 'color';
    }
    
    public function sizeRepresentedByAttribute()
    {
        return ''; //??
    }
    
    public function feedFileName()
    {
        return self::PRODUCT_FEED_FILENAME;
    }
    
    public function feedApprovedStoreCode() 
    {
        return $this->getStoreConfig(self::XML_CONFIG_FEED_APPROVED_STORE_CODE);
    }

    public function feedEndpoint()
    {
        return $this->getStoreConfig(self::XML_CONFIG_FEED_ENDPOINT);
    }

    public function pinterestMerchantUrl()
    {
        return self::PINTEREST_MERCHANT_URL;
    }
    
    public function feedExternalId()
    {
        return $this->getStoreConfig(self::XML_CONFIG_FEED_EXTERNAL_ID);
    }
    
    public function feedDirectOutput()
    {
        return $this->getStoreConfigFlag(self::XML_CONFIG_FEED_DIRECT_OUTPUT);
    }
    
    public function feedMerchantId()
    {
        return $this->getStoreConfig(self::XML_CONFIG_FEED_MERCHANT_ID);
    }
    
    public function feedAccessToken()
    {
        return $this->getStoreConfig(self::XML_CONFIG_FEED_ACCESS_TOKEN);
    }
    
    public function availablePaymentMethods()
    {
        return array_keys($this->getSupportedPaymentMethods());
    }
    
    public function paymentRewrites() {
        $methodsConfig = $this->getSupportedPaymentMethods();
        if (!isset($methodsConfig[$this->paymentMethod()]['rewrites'])) {
            return array();
        }
        return $methodsConfig[$this->paymentMethod()]['rewrites'];
    }
    
    public function paymentMethod()
    {
        return $this->getStoreConfig(self::XML_CONFIG_CC_PAYMENT_OPTION);
    }
    
    public function mainStoreCode() {
        return $this->getStoreConfig(self::XML_CONFIG_MAIN_STORE_CODE);
    }
    
    public function saveAuthToken($token) 
    {
        $this->config()->saveConfig(self::XML_CONFIG_AUTHORIZATION_TOKEN, $token);
        return $this;
    }
    
    protected function getStoreConfig($path)
    {
        return Mage::getStoreConfig($path, $this->getStoreId());
    }
    
    protected function getStoreConfigFlag($path)
    {
        return Mage::getStoreConfigFlag($path, $this->getStoreId());
    }
    
    /**
     * @return Mage_Core_Model_Config;
     */
    protected function config() {
        return Mage::getSingleton('core/config');
    }
}