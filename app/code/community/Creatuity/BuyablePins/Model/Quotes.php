<?php

/**
 *
 * @category   Creatuity
 * @package    BuyablePins
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Quotes
{
    protected $debug = false;
    protected $quoteInstances = array();
    protected $orderInstances = array();
    protected $quote2order = array();
    
    public function __construct()
    {
        $this->debug = $this->config()->debugMode();
    }
    
    
    /**
     * @return Mage_Sales_Model_Quote
     */
    public function provideQuote($quoteOrCartId, $store) 
    {
        $instanceKey = $this->key( $quoteOrCartId, $store );
        if (!isset($this->quoteInstances[$instanceKey])) {
            $this->quoteInstances[$instanceKey] = $this->loadQuote($quoteOrCartId, $store);
        }
        return $this->quoteInstances[$instanceKey];
    }
    
    /**
     * @return Mage_Sales_Model_Quote
     */
    protected function loadQuote($quoteOrCartId, $store) 
    {
        $quoteId = $this->toQuoteId($quoteOrCartId);
        $storeId = $this->toStoreId($store);
        
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = Mage::getModel("sales/quote");

        if ($storeId === null) {
            $quote->loadByIdWithoutStore($quoteId);
        } else {
            $quote->setStoreId($storeId)->load($quoteId);
        }
        
        if (is_null($quote->getId())) {
            throw new Creatuity_BuyablePins_Model_Exception('Cannot load quote.', 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_NOT_FOUND);
        }

        return $quote;
    }
    
    /**
     * @return Mage_Sales_Model_Order
     */
    public function provideOrder($quoteOrCartId, $store) 
    {
        $instanceKey = $this->key( $quoteOrCartId, $store );
        
        if (!isset($this->orderInstances[$instanceKey])) {
            $this->orderInstances[$instanceKey] = $this->loadOrder($quoteOrCartId, $store);
        }
        return $this->orderInstances[$instanceKey];
    }
    
    /**
     * @return Mage_Sales_Model_Order
     */
    protected function loadOrder($quoteOrCartId, $store) 
    {
        $quoteId = $this->toQuoteId($quoteOrCartId);
        $storeId = $this->toStoreId($store);
        
        if (!$this->isOrderCreated( $quoteOrCartId, $store )) {
            throw new Creatuity_BuyablePins_Model_Exception('Cannot load order.', 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_NOT_FOUND);
        }
        
        $order = Mage::getModel('sales/order');
        if ($storeId !== null) {
            $order->setStoreId($storeId);
        }
        $order->load($this->findOrderId( $quoteId, $store ));
        return $order;
    }
    
    public function ensureOrderCreated($quoteOrCartId, $store) 
    {
        if (!$this->isOrderCreated( $quoteOrCartId, $store )) {
            throw new Creatuity_BuyablePins_Model_Exception(
                'Action cannot be performed. Order doesn\'t exist yet for this cart',
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_EXPIRED
            );
        }
    }
    
    public function ensureNoOrderCreated($quoteOrCartId, $store) 
    {
        if ($this->isOrderCreated( $quoteOrCartId, $store )) {
            throw new Creatuity_BuyablePins_Model_Exception(
                'Action cannot be performed. Order has been already placed for this cart',
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_EXPIRED
            );
        }
    }
            
            
    public function isOrderCreated($quoteOrCartId, $store) 
    {
        return (bool)$this->findOrderId($quoteOrCartId, $store);
    }
    
    public function findOrderId($quoteOrCartId, $store)
    {
        $quoteId = $this->toQuoteId($quoteOrCartId);
        $storeId = $this->toStoreId($store);
        
        if ( !isset($this->quote2order[$quoteId]) ) {
            $sql = $this->_connection()->select()
                ->from(array(
                    'order' => $this->tblName('sales/order')
                ), array(
                    'order_id' => 'entity_id'
                ))
                ->joinLeft(
                    array('quote' => $this->tblName('sales/quote')), 
                    'order.quote_id = quote.entity_id AND order.store_id = quote.store_id', 
                    array()
                )->where('order.quote_id = ?', $quoteId)
                ->where('order.store_id = ?', $storeId)
                ->order('order.entity_id ' . Varien_Db_Select::SQL_DESC)
                ->limit(1);
            
            $orderId = $this->_connection()->fetchOne($sql);
            
            $this->quote2order[$quoteId] = $orderId !== null ? $orderId : false;
        }
        return $this->quote2order[$quoteId];
    }
    
    public function updateCacheAfterOrderCreate(  Mage_Sales_Model_Quote $quote, Mage_Sales_Model_Order $order) 
    {
        $this->quote2order[$quote->getId()] = $order->getId();
        
        $key = $this->key($quote->getId(), $quote->getStoreId());
        $this->quoteInstances[$key] = $quote;
        $this->orderInstances[$key] = $order;
    }
    
    public function toQuoteId($quoteOrCartId) 
    {
        return (int)(is_numeric($quoteOrCartId)
            ? $quoteOrCartId
            : $this->cartId2QuoteId($quoteOrCartId));
    }
    
    public function toCartId($quoteOrCartId) 
    {
        return (string)(is_numeric($quoteOrCartId)
            ? $this->quoteId2CartId($quoteOrCartId)
            : $quoteOrCartId);
    }
    
    protected function quoteId2CartId($quoteId) 
    {
        if (!is_numeric( $quoteId ) || (int)$quoteId != $quoteId) {
            throw new Creatuity_BuyablePins_Model_Exception("Invalid cart id", 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_NOT_FOUND);
        }
        
        if ($this->debug) {
            return 'quote' . ($quoteId);
        } else {
            return base_convert(($quoteId * 338) + 14783691, 10, 36);
        }
    }
    
    protected function cartId2QuoteId($cartId) 
    {
        if (is_string($cartId)) {
            if ($this->debug) {
                return (int)substr($cartId, strlen('quote'));
            } else {
                $res = (((int)base_convert($cartId, 36, 10) - 14783691) / 338);
                if (is_integer($res) && !$this->_containUnsupportedChars($cartId)) {
                    return $res;
                }
            }
        }
        
        throw new Creatuity_BuyablePins_Model_Exception("Invalid cart id", 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_NOT_FOUND);
    }
    
    protected function _containUnsupportedChars($value)
    {
        return (bool) preg_replace('/[A-Za-z0-9]/', '', $value);
    }

    protected function toStoreId($store) 
    {
        return $this->_helper()->toStoreId($store);
    }
    
    protected function key($quoteOrCartId, $store) 
    {
        $quoteId = $this->toQuoteId($quoteOrCartId);
        $storeId = $this->toStoreId($store);
        
        return "q{$quoteId}_s{$storeId}";
    }
    
    protected function tblName($tbl) 
    {
        return Mage::getSingleton('core/resource')->getTableName($tbl);
    }
    
    /**
     * @return Varien_Db_Adapter_Interface
     */
    protected function _connection() 
    {
        return Mage::getSingleton('core/resource')->getConnection('read');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Mage_Core_Helper_Data
     */
    protected function _core() 
    {
        return Mage::helper('core');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function _helper() 
    {
        return Mage::helper('buyablepins');
    }
    
}
