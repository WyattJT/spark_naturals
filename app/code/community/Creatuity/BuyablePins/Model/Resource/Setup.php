<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup
{

    public function generateNewAuthToken()
    {
        $this->authToken()->generateNew();
    }

    public function createPinterestWebsite()
    {
        $websiteId = $this->createScopeEntity('core/website', 'code', array(
            'code' => Creatuity_BuyablePins_Helper_Website::PINTEREST_STORE_CODE,
            'name' => 'Pinterest',
        ));
        
        $storeGroupId = $this->createScopeEntity('core/store_group', 'name', array(
            'name' => 'Pinterest',
            'website_id' => $websiteId,
            'root_category_id' => $this->topLevelCategoryId(),
        ));
        
        $storeId = $this->createScopeEntity('core/store', 'code', array(
            'code' => Creatuity_BuyablePins_Helper_Website::PINTEREST_STORE_CODE,
            'name' => 'Pinterest',
            'website_id' => $websiteId,
            'group_id' => $storeGroupId,
            'is_active' => 1,
        ));
    }
    
    protected function topLevelCategoryId() 
    {
        $topId = $this->newCategoriesCollection()
            ->addFieldToFilter('level', 1)
            ->setPageSize( 1 )
            ->getFirstItem()
            ->getId()
        ;
        if (!$topId) {
            Mage::throwException('Cannot find Top Category');
        }
        return $topId;
    }

    protected function createScopeEntity($type, $idField, array $data) 
    {
        $entity = Mage::getModel($type)
                ->load($data[$idField], $idField)
                ->addData($data)
                ->save();
        return $entity->getId();
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Authtoken
     */
    protected function authToken()
    {
        return Mage::getSingleton( 'buyablepins/authtoken' );
    }
    
    /**
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    protected function newCategoriesCollection() 
    {
        return Mage::getResourceModel('catalog/category_collection');
    }

}