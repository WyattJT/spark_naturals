<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Resource_Setup_Catalog extends Mage_Catalog_Model_Resource_Setup
{
    public function installGtinAttribute()
    {
        $this->installAttribute(Creatuity_BuyablePins_Model_Config::GTIN_ATTR_CODE, array(
            'label' => 'BuyablePins GTIN',
            'frontend_input' => 'text',
            'global' => true,
            'required' => false,
        ));
    }
    
    protected function installAttribute($code, array $attributeData, $entity = Mage_Catalog_Model_Product::ENTITY)
    {
        $this->removeAttribute($entity, $code);
        $this->addAttribute($entity, $code, $attributeData);
    }
    
}