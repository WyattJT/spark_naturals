<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Resource_Setup_Sales extends Mage_Sales_Model_Resource_Setup
{
    
    public function installInvalidCouponsField() 
    {
        $this->installAttribute(
            "buyablepins_invalid_coupons", 
            array(
                "order", 
                "quote"
            ), 
            array(
                "type"=>"varchar"
            )
        );
    }
    
    
    protected function installAttribute($code, array $entityIds, array $attr) 
    {
        foreach($entityIds as $entityTypeId) {
            $this->addAttribute($entityTypeId, $code, $attr);
        }
    }
    
    
}