<?php
/**
 *
 * @category   Creatuity
 * @package    BuyablePins
 * @copyright  Copyright (c) 2008-2016 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Activation {



    public function changeBPActivation($activation_value) {
        $pinterest_info = $this->_getPinterestInfo();
        $pinterest_info->data->is_active = $activation_value;
        return $this->_setPinterestInfo($pinterest_info->data);
    }


    public function isActiveOnPinterest() {
        $pinterest_info = $this->_getPinterestInfo();
        if($pinterest_info->status == "failure")
            return $pinterest_info;
        $isActive = $pinterest_info->data->is_active;

        return $isActive;
    }

    protected function _getPinterestInfo() {
        $merchantId = $this->_config()->feedMerchantId();
        $accessToken = $this->_config()->feedAccessToken();
        $endpoint = $this->_config()->pinterestMerchantUrl();

        $curl_endpoint = $endpoint . $merchantId . "/";
        $pinterest_curl = curl_init();
        curl_setopt($pinterest_curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($pinterest_curl, CURLOPT_URL, $curl_endpoint);
        curl_setopt($pinterest_curl,CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $accessToken));
        $json_response = curl_exec($pinterest_curl);
        curl_close($pinterest_curl);
        $response = json_decode($json_response);
        return $response;
    }

    /**
     * Data cannot be from raw _getPinterestInfo response. You need to specify which object/value you are updated
     * Example: $response->data
     */
    protected function _setPinterestInfo($data) {
        $merchantId = $this->_config()->feedMerchantId();
        $accessToken = $this->_config()->feedAccessToken();
        $endpoint = $this->_config()->pinterestMerchantUrl();

        $curl_endpoint = $endpoint . $merchantId . "/";
        $pinterest_curl = curl_init();
        curl_setopt($pinterest_curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($pinterest_curl, CURLOPT_URL, $curl_endpoint);
        curl_setopt($pinterest_curl,CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $accessToken));
        curl_setopt($pinterest_curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($pinterest_curl, CURLOPT_POSTFIELDS,http_build_query($data));

        $json_response = curl_exec($pinterest_curl);
        curl_close($pinterest_curl);
        $response = json_decode($json_response);
        if( $response->status == "success" )
            return true;
        else
            return false;
    }


    public function checkRequirements() {
        return true;
    }


    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function _config() {
        return Mage::getSingleton('buyablepins/config');
    }

}
