<?php

class Creatuity_BuyablePins_Model_Product_Website extends Varien_Object
{

    public function assignAllProducts($fromWebsite, $toWebsite)
    {
        $select = $this->prepareSelect(
            Mage::app()->getWebsite($fromWebsite)->getId(), 
            Mage::app()->getWebsite($toWebsite)->getId()
        );
        
        $query = $this->connection()->insertFromSelect(
            $select, 
            $this->resource()->getTableName('catalog/product_website'),
            array('product_id', 'website_id'), 
            Varien_Db_Adapter_Pdo_Mysql::INSERT_ON_DUPLICATE
        );
        
        $this->connection()->query($query);
    }
    
    /**
     * 
     * @param int $fromWebsiteId
     * @param int $toWebsiteId
     * @return Varien_Db_Select
     */
    protected function prepareSelect($fromWebsiteId, $toWebsiteId)
    {
        return $this->connection()->select()
               ->from(array(
                        'cpw' => $this->resource()->getTableName('catalog/product_website')
                    ),array(
                       'product_id',
                       new Zend_Db_Expr("{$toWebsiteId} as website_id")
                    )
                )
               ->where('website_id = ?', $fromWebsiteId);
    }
    
    /**
     * 
     * @return Varien_Db_Adapter_Interface
     */
    protected function connection()
    {
        return $this->resource()->getConnection('core_write');
    }
    
    /**
     * 
     * @return Mage_Core_Model_Resource
     */
    protected function resource()
    {
        return Mage::getSingleton('core/resource');
    }
}

