<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 * 
 * @method string getQuoteTableName();
 * @method string getOrderTableName();
 * @method string getSelectClassName();
 */
class Creatuity_BuyablePins_Model_Service_Cart extends Creatuity_BuyablePins_Model_Service_Abstract
{
    /**
     * @return int
     */
    public function create($store)
    {
        try {
            $quoteId = $this->apiCart()->create($store);

            return $this->quotes()->toCartId($quoteId);
        } catch (Exception $e) {
            $this->throwException($e, null, $store, Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ERROR);
        }
    }

    public function itemUpdate($cartId, array $cartData, $storeId)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);

            $productsInCartAfter = array();
            foreach ($cartData as $cartItemData) {
                $prodId = $cartItemData['item']['item_id'];
                $qty = $cartItemData['quantity'];

                $this->helper()->ensureProductExists($prodId, $storeId);
                $this->helper()->ensureProductQtyValid($prodId, $qty);

                $productsInCartAfter[$prodId] = array(
                    'product_id' => $prodId,
                    'qty' => $qty
                );
            }

            $quote = $this->quotes()->provideQuote($cartId, $storeId);
            $quote->removeAllItems();
            $quote->getShippingAddress()->setCollectShippingRates(true);

            if ($productsInCartAfter) {
                $this->apiCartProduct()->add($cartId, $productsInCartAfter, $storeId);
            } else {
                $quote->collectTotals()->save();
            }

        } catch (Exception $e) {
            $this->throwException($e, $cartId, $storeId,
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_ERROR);
        }
    }


    /**
     * @return bool
     */
    public function addShippingAddress($cartId, array $addressData, $storeId, $onlyAddMissing = false)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);

            $this->addAddress($cartId, Mage_Checkout_Model_Cart_Customer_Api::ADDRESS_SHIPPING,
                $addressData, $storeId, $onlyAddMissing);

            $this->ensureQuoteHasNoErrors($cartId, $storeId);

        } catch (Exception $e) {
            $this->throwException($e, $cartId, $storeId,
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_INVALID);
        }
    }

    /**
     * @return bool
     */
    public function addBillingAddress($cartId, array $addressData, $storeId, $onlyAddMissing = false)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);

            $this->addAddress($cartId, Mage_Checkout_Model_Cart_Customer_Api::ADDRESS_BILLING,
                $addressData, $storeId, $onlyAddMissing);

            $this->ensureQuoteHasNoErrors($cartId, $storeId);

        } catch (Exception $e) {
            $this->throwException($e, $cartId, $storeId,
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_BILLING_ERROR);
        }
    }


    /**
     * @return bool
     */
    protected function addAddress($cartId, $addressMode, array $addressData, $storeId, $onlyAddMissing)
    {

        if (isset($addressData['address'])) {
            // flatten address 
            $addressData += $addressData['address'];
        }

        $addressData += array(
            // Magento cannot save address without some fields
            // However pinterest can provide them during payment phase
            // We're adding dummy values to allow operate quote

            'first_name' => Creatuity_BuyablePins_Helper_Data::EMPTY_DUMMY_VALUE,
            'last_name' => Creatuity_BuyablePins_Helper_Data::EMPTY_DUMMY_VALUE,
            'street' => Creatuity_BuyablePins_Helper_Data::EMPTY_DUMMY_VALUE,
            'phone' => Creatuity_BuyablePins_Helper_Data::EMPTY_DUMMY_VALUE,
            'company' => null,
            'email' => null,
            'extra_street' => null,
            'locality' => null,
            'region' => null,
            'country' => null,
            'postal_code' => null,
            'fax' => null,
        );

        $quote = $this->quotes()->provideQuote($cartId, $storeId);

        $previousAddress = $addressMode == Mage_Checkout_Model_Cart_Customer_Api::ADDRESS_BILLING
            ? $quote->getBillingAddress()
            : $quote->getShippingAddress();

        $magentoAddressData = array();

        if ($onlyAddMissing) {
            $magentoAddressData = $this->helper()->filterEmpty($previousAddress->getData());
        }

        $magentoShippingAddressData = $quote->getShippingAddress();
        if(strlen($addressData['email']) < 1)
        {
            $addressData['email'] = $magentoShippingAddressData['email'];
        }

        $magentoAddressData += array(
            'mode' => $addressMode,
            'firstname' => $addressData['first_name'],
            'lastname' => $addressData['last_name'],
            'company' => $addressData['company'],
            'street' => array($addressData['street'], $addressData['extra_street']),
            'city' => $addressData['locality'],
            'region' => $addressData['region'],
            'postcode' => $addressData['postal_code'],
            'country_id' => $addressData['country'],
            'telephone' => $addressData['phone'],
            'fax' => $addressData['fax'],
            'email' => $addressData['email'],
        );

        if ($onlyAddMissing && $previousAddress->getId()) {
            $magentoAddressData += $previousAddress->getData();
        }

        $this->helper()->ensurePostcodeIsValid($magentoAddressData['postcode'], $magentoAddressData['country_id']);

        $quote->setDataChanges(true);
        $this->apiCartCustomer()->setAddresses($cartId, array($magentoAddressData), $storeId);

    }

    public function pullShippingOptions($cartId, $storeId)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);
            
            $quote = $this->quotes()->provideQuote($cartId, $storeId);
            
            if (!$quote->hasItems()) {
                Mage::throwException("Cannot pull shipping rates, because cart is empty.");
            }
            
            if (!$quote->getShippingAddress()->getPostcode()) {
                Mage::throwException("Cannot pull shipping rates. Please fullfill address first.");
            } 
            
            $this->apiCartShipping()->getShippingMethodsList($cartId, $storeId);
            
            $this->ensureQuoteHasNoErrors($cartId, $storeId);
            
        } catch ( Exception $e ) {
            if ($e->getMessage() == 'shipping_address_is_not_set') {
                $e = "Shipping address is not set";
            }
            $this->throwException($e, $cartId, $storeId, Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_ERROR);
        }
    }

    public function setShippingOption($cartId, $shippingMethodCode, $storeId)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);
            
            $this->apiCartShipping()->setShippingMethod($cartId, $shippingMethodCode, $storeId);
            
            $this->ensureQuoteHasNoErrors($cartId, $storeId);
        } catch ( Exception $e ) {
            $code =  $e->getMessage() == 'shipping_method_is_not_available'
                ? Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_OPTION_ID_INVALID
                : Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_ERROR;
            $this->throwException($e, $cartId, $storeId, $code);
        }
    }
    
    public function addCoupon($cartId, array $couponsValues, $storeId)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);
            
            $quote = $this->quotes()->provideQuote($cartId, $storeId);
            
            if (!$quote->hasItems()) {
                Mage::throwException("Cannot add coupon, because cart is empty.");
            }
            
            $couponIds = array();
            foreach($couponsValues as $coupon) {
                $couponIds[] = $coupon['id'];
            }
            
            $validCoupon = $this->_findValidCoupon($couponsValues);
            
            if ($validCoupon) {
                $this->apiCartCoupon()->add($cartId, $validCoupon, $storeId);
            } else {
                $this->apiCartCoupon()->remove($cartId);
            }
            
            $quote->setData('buyablepins_invalid_coupons', implode(',', array_diff($couponIds, array($validCoupon))) )->save();                
        
            $this->ensureQuoteHasNoErrors($cartId, $storeId);            
        } catch ( Exception $e ) {
            $this->throwException($e, $cartId, $storeId, Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ERROR);
        }
    }
    
    protected function _findValidCoupon(array $couponIds)
    {
        $coupon = $this->_newCouponsCollection()
            ->addFieldToFilter( 'code', array(
                'in' => $couponIds,
            ) )
            ->setPageSize( 1 )
            ->getFirstItem();
        return $coupon->getCode();
    }
    
    public function createOrder($cartId, $storeId, array $paymentData)
    {
        try {
            $this->quotes()->ensureNoOrderCreated($cartId, $storeId);
            
            $quote = $this->quotes()->provideQuote($cartId, $storeId);
            
            if (!$quote->hasItems()) {
                Mage::throwException("Cannot create order because cart is empty.");
            }
            
            $this->_finalValidation($quote);
            
            $this->paymentMethodReplaces()->replaceOnTheFlyPaymentMethodsForPinterest();
            
            if ( ! empty($paymentData['card']) ) {
                
                $this->apiCartPayment()->setPaymentMethod($cartId, array(
                    0 => null,
                    'po_number' => null,
                    'method' => $this->config()->paymentMethod(),
                    'cc_cid' => null,
                    'cc_owner' => $paymentData['billing_detail']['first_name'] . ' ' . $paymentData['billing_detail']['last_name'],
                    'cc_number' => $paymentData['card'],
                    'cc_type' => Creatuity_BuyablePins_Model_Config::CREDIT_CARD_OTHER_TYPE,
                    'cc_exp_year' => $paymentData['exp_year'],
                    'cc_exp_month' => $paymentData['exp_month'],
                ), $storeId);

            }

            $magentoShippingAddressData = $quote->getShippingAddress();
            if(!isset($paymentData['billing_detail']['email']) || (strlen($paymentData['billing_detail']['email']) < 1))
            {
                $email = $magentoShippingAddressData['email'];
            } else
            {
                $email = $paymentData['billing_detail']['email'];
            }
            
            $quote->setCustomer(Mage::getModel('customer/customer', array(
                'firstname' => $paymentData['billing_detail']['first_name'],
                'lastname' => $paymentData['billing_detail']['last_name'],
                'email' => $email,    
            )))->save();
            
            try{
                $this->apiCart()->createOrderUsingCc($cartId, @$paymentData['card'], $storeId);
            } catch (Exception $e) {
                $this->throwException($e, $cartId, $storeId, 
                        Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_INFO_CREDIT_CARD_INVALID);
            }
            
        } catch ( Exception $e ) {
            $this->throwException($e, $cartId, $storeId, Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_ERROR );
        }
    }
    
    protected function _finalValidation(  Mage_Sales_Model_Quote $quote )
    {
        $this->_finalAddressValidation($quote->getShippingAddress(), 'shipping');
        //$this->_finalAddressValidation($quote->getBillingAddress(), 'billing');
    }
    
    protected function _finalAddressValidation( Mage_Sales_Model_Quote_Address $address, $addressType )
    {
        if ($this->helper()->isEmpty($address->getStreet1())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'street' for {$addressType} address:",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_STREET_INVALID
            );
        }
        if ($this->helper()->isEmpty($address->getFirstname())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'first name' for {$addressType} address",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_FIRST_NAME_INVALID
            );
        }
        if ($this->helper()->isEmpty($address->getLastname())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'last name' for {$addressType} address",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_LAST_NAME_INVALID
            );
        }
        if ($this->helper()->isEmpty($address->getCity())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'locality' for {$addressType} address",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_LOCALITY_INVALID
            );
        }
        if ($this->helper()->isEmpty($address->getRegionCode())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'region' for {$addressType} address",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_REGION_INVALID
            );
        }
        if ($this->helper()->isEmpty($address->getCountryId())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'country' for {$addressType} address",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_COUNTRY_INVALID
            );
        }
        if ($this->helper()->isEmpty($address->getPostcode())) {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Invalid 'postal code' for {$addressType} address",
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_SHIPPING_DETAIL_POSTAL_CODE_INVALID
            );
        }
    }

    public function getTotal($cartId, $storeId)
    {
        $quote = $this->quotes()->provideQuote($cartId, $storeId);
        return round($quote->getBaseGrandTotal(), 2);
    }


    protected function preparePaymentData(array $paymentsData) 
    {
        if ( count($paymentsData) > 1 ) {
            throw new Creatuity_BuyablePins_Model_Exception('Only single payment is supported', 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_PAYMENT_INFO_MULTIPLE_CARDS_NOT_ACCEPPTED
            );
        }
        $paymentData = reset($paymentsData);
        return $paymentData;
    }
    
    protected function ensureOrderIsNotCreated($cartId, $storeId)
    {
        if ( $this->quotes()->isOrderCreated($cartId, $storeId) ) {
            throw new Creatuity_BuyablePins_Model_Exception(
                'Action cannot be performed. Order has been already placed on this cart',
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_EXPIRED
            );
        }
        return $this;
    }
    
    protected function ensureQuoteHasNoErrors($cartId, $store) {
        $quote = $this->quotes()->provideQuote($cartId, $store);
        if ($quote->getErrors()) {
            $msg = '';
            foreach($quote->getErrors() as $errorInfo) {
                if($errorInfo->getType() == 'error') {
                    $msg .= $errorInfo->getCode() . "\n";
                }
            }
            throw new Creatuity_BuyablePins_Model_Exception($msg, 
                Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ERROR);
        }
    }
    
    protected function throwException($msgOrException, $cartId, $store, $generalErrorCode) {
        $msg = $msgOrException;
        $ex = null;
        
        if ( $msgOrException instanceof Mage_Api_Exception
                && $msgOrException->getCustomMessage()) 
        {
            $msg = $msgOrException->getCustomMessage();
            $ex = $msgOrException;
        } elseif ($msgOrException instanceof Creatuity_BuyablePins_Model_Exception) {
            $ex = $msgOrException;
            throw $ex;
        } elseif ($msgOrException instanceof Exception) {
            $msg = $msgOrException->getMessage();
            $ex = $msgOrException;
        }
        
        if ($cartId !== null && $store !== null
                && $quote = $this->quotes()->provideQuote($cartId, $store)) 
        {
            $this->_handleQuoteErrors($ex, $quote);
        }
        
        throw new Creatuity_BuyablePins_Model_Exception($msg, $generalErrorCode, null, $ex);
    }
    
    protected function _handleQuoteErrors(Exception $e, Mage_Sales_Model_Quote $quote) {
        foreach($quote->getAllItems() as $item) {
            foreach ($item->getErrorInfos() as $errorInfo) {
                if ($errorInfo['origin'] == 'cataloginventory'
                        && ($errorInfo['code'] == Mage_CatalogInventory_Helper_Data::ERROR_QTY
                            || $errorInfo['code'] == Mage_CatalogInventory_Helper_Data::ERROR_QTY_INCREMENTS)
                ) {
                    throw new Creatuity_BuyablePins_Model_Exception($errorInfo['message'], 
                        Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_QUANTITY_INVALID,
                        null, $e);
                }
            }
        }
    }
    
    
    /**
     * @return Creatuity_BuyablePins_Model_Checkout_Cart_Api
     */
    protected function apiCart()
    {
        return Mage::getSingleton('buyablepins/checkout_cart_api');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Checkout_Cart_Product_Api
     */
    protected function apiCartProduct()
    {
        return Mage::getSingleton('buyablepins/checkout_cart_product_api');
    }
    
    /**
     * @return Creatuity_Buyablepins_Model_Checkout_Cart_Customer_Api
     */
    protected function apiCartCustomer()
    {
        return Mage::getSingleton('buyablepins/checkout_cart_customer_api');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Checkout_Cart_Shipping_Api
     */
    protected function apiCartShipping()
    {
        return Mage::getSingleton('buyablepins/checkout_cart_shipping_api');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Checkout_Cart_Coupon_Api
     */
    protected function apiCartCoupon()
    {
        return Mage::getSingleton('buyablepins/checkout_cart_coupon_api');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Checkout_Cart_Payment_Api
     */
    protected function apiCartPayment()
    {
        return Mage::getSingleton('buyablepins/checkout_cart_payment_api');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Payment_Replacer
     */
    protected function paymentMethodReplaces() {
        return Mage::getSingleton('buyablepins/payment_replacer');
    }
    
    /**
     * @return Mage_SalesRule_Model_Resource_Coupon_Collection
     */
    protected function _newCouponsCollection() {
        return Mage::getResourceModel('salesrule/coupon_collection');
    }
    
}

