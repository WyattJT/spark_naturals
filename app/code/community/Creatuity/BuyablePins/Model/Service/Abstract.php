<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
abstract class Creatuity_BuyablePins_Model_Service_Abstract extends Varien_Object
{
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() 
    {
        return Mage::helper('buyablepins');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Quotes
     */
    protected function quotes() 
    {
        return Mage::getSingleton('buyablepins/quotes');
    }
    
    /**
     * @return Mage_Core_Model_Date
     */
    protected function date() {
        return Mage::getSingleton('core/date');
    }
   
}