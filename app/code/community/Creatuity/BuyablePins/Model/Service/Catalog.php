<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Service_Catalog extends Creatuity_BuyablePins_Model_Service_Abstract
{
    
    /**
     * @return Mage_Catalog_Model_Product[]
     */
    public function loadProductsByItemGroupId($itemGroupId, $storeId)
    {
        $product = $this->loadProduct($itemGroupId, $storeId);
        
        switch($product->getTypeId()) 
        {
            case 'simple':
                return $this->loadProductSimple($product);
            case 'configurable':
                return $this->loadProductConfigurable($product);
        }
        
        throw new Creatuity_BuyablePins_Model_Exception(
            "Product of type '{$product->getTypeId()}' is not supported",
            Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_ID_NOT_FOUND );
    }
    
    /**
     * @return Mage_Catalog_Model_Product[]
     */
    protected function loadProductSimple(Mage_Catalog_Model_Product $product)
    {
        return array($product);
    }
    
    /**
     * @return Mage_Catalog_Model_Product[]
     */
    protected function loadProductConfigurable(Mage_Catalog_Model_Product $product)
    {
        return $product->getTypeInstance(true)->getUsedProducts(null, $product);
    }
    
    /**
     * @return Mage_Catalog_Model_Product
     */
    protected function loadProduct($id, $storeId) 
    {
        $product = $this->newProduct()->setStoreId($storeId)->load($id);
        if ( $product->isObjectNew() ) 
        {
            throw new Creatuity_BuyablePins_Model_Exception(
                "Product {$id} does not exist", 
                    Creatuity_BuyablePins_Helper_ResponseCodes::ERROR_CART_ITEM_ID_NOT_FOUND
            );
        }
        return $product;
    }
    
    /**
     * @return Mage_Catalog_Model_Product
     */
    protected function newProduct() {
        return Mage::getModel('catalog/product');
    }
}