<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Cron
{
    public function sendFeedRequest(Mage_Cron_Model_Schedule $schedule)
    {
        $this->feedRequest()->sendRequest();
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Feed_Request
     */
    protected function feedRequest() 
    {
        return Mage::getSingleton('buyablepins/feed_request');
    }
}