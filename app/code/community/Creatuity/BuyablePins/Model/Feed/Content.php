<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method Mage_Catalog_Model_Resource_Product_Collection getProducts();
 * @method SimpleXMLElement getXml();
 * @method bool getDirectOutput();
 */
class Creatuity_BuyablePins_Model_Feed_Content extends Varien_Object
{

    protected $productsInFeed = array();
    
    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'products_collection_store' => Creatuity_BuyablePins_Helper_Website::PINTEREST_STORE_CODE,
            'direct_output' => false,
        ));
    }
    
    public function generate()
    {
        $xml = $this->output('<items>');

        $productsFeed = $this->productsFeed();
        
        $infLoopProtector = 10000;
        while ( !$productsFeed->hasReachLimit() ) {
            if ( !$infLoopProtector-- ) {
                Mage::throwException('Infinite loop protection');
            }
            
            foreach ( $productsFeed->getNextChunk() as $product ) {
                $productId = $product->getId();
                if(array_key_exists($productId, $this->productsInFeed) === false &&
                    !isset($this->productsInFeed[$productId]))
                {
                    $this->productsInFeed[$productId] = $productId;
                    switch( $product->getTypeId() )
                    {
                        case 'simple':
                            $xml .= $this->output( $this->addSimpleProduct($product) );
                            break;
                        case 'configurable':
                            $xml .= $this->output( $this->addConfigurableProduct($product) );
                            break;
                    }
                } else {
                    if(Mage::getStoreConfig('buyablepins/general/debug_mode'))
                    {
                        Mage::log("Product " . $product->getSku() . " has already appeared once before in the feed. Not adding it a second time.", null, "creatuity_buyablepins_feed.log");
                    }
                }
            }
        }
        $xml .= $this->output('</items>');
        return $xml;
    }
    
    protected function addSimpleProduct(Mage_Catalog_Model_Product $product)
    {
        return $this->renderProductAsXml($product);
    }
    
    protected function addConfigurableProduct(Mage_Catalog_Model_Product $product)
    {
        // check if there's more than one attribute
        // if more than one attribute, Pinterest won't let us submit it right now
        $configurable = $product->getTypeInstance();
        $attributes = $configurable->getConfigurableAttributes($product);
        $childrensXml = null;
        if(count($attributes) > 1)
        {
            return false;
        }

        foreach ($attributes as $attribute)
        {
            $attributeLabel = $attribute->getLabel();
            $attributeId = $attribute->getId();
        }

        $childIds = $product->getTypeInstance(true)->getUsedProductIds($product);
        foreach($childIds as $childId) {
            $this->productsInFeed[$childId] = $childId;
        }

        $childProductsFeed = $this->productsFeed($childIds);

        // first we add the main configurable product so the children can link to it
        // Pinterest now says not to include the main configurable product
        // $childrensXml = $this->addSimpleProduct($product);
        $infLoopProtector = 1000;
        while ( !$childProductsFeed->hasReachLimit() ) {
            if ( !$infLoopProtector-- ) {
                Mage::throwException('Infinite loop protection');
            }
            foreach ( $childProductsFeed->getNextChunk() as $childProduct) {
                $childrensXml .= $this->renderProductAsXml($childProduct, $product);
            }
        }

        return $childrensXml;
    }
    
    /**
     * @return Creatuity_BuyablePins_Block_Feed_Product
     */
    protected function renderProductAsXml(
        Mage_Catalog_Model_Product $product,
        Mage_Catalog_Model_Product $parentProduct = null
    )
    {
        return Mage::app()->getLayout()
            ->getBlockSingleton('buyablepins/feed_product')
            ->render($product, $parentProduct);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Feed_Products
     */
    protected function productsFeed($productIds = null, array $params = array()) 
    {
        return Mage::getModel('buyablepins/feed_products', $params + array(
            'product_ids' => $productIds,
            'products_collection_store' => $this->getData('products_collection_store'),
        ))->init();
    }
    
    protected function output($content)
    {
        $content .= "\n";
        if ( $this->getDirectOutput() ) {
            echo $content;
            return null;
        }
        return $content;
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Website
     */
    protected function websites() 
    {
        return Mage::helper('buyablepins/website');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() {
        return Mage::helper('buyablepins');
    }
    
}