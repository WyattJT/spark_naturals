<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method Mage_Catalog_Model_Resource_Product_Collection getProducts();
 * @method int getChunkSize();
 * @method Mage_Catalog_Model_Product_Attribute_Backend_Media getMediaGalleryAttributeBackend();
 * @method setMediaGalleryAttributeBackend(Mage_Catalog_Model_Product_Attribute_Backend_Media $backend);
 */
class Creatuity_BuyablePins_Model_Feed_Products extends Varien_Object
{
    protected $page;
    
    protected $size = 0;

    protected $mageworxPresent;
    
    protected $productsInFeed = array();

    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'product_ids' => null,
            'chunk_size' => 3,
            'products_collection_store' => Creatuity_BuyablePins_Helper_Website::PINTEREST_STORE_CODE,
            'media_gallery_attribute_backend' => null,
        ));
        $this->mageworxPresent = Mage::helper('core')->isModuleEnabled('MageWorx_CustomOptions');
    }
    
    public function init()
    {
        if ( !$this->getMediaGalleryAttributeBackend() ) {
            $this->setMediaGalleryAttributeBackend( Mage::getResourceModel('catalog/product')->getAttribute('media_gallery')->getBackend() );
        }
        
        // we need to create collection in admin store
        // to prevent from using flat tables
        $this->setProducts(
            $this->websites()->runInAdminStore(array($this, 'prepareProductsCollection'))
        );
        
        $this->size = $this->getProducts()->getSize();
        $this->page = 1;
        
        if ( $this->getChunkSize() > 0 ) {
            $this->getProducts()->setPageSize( $this->getChunkSize() );
        }
        
        return $this;
    }
    
    /**
     * @return Mage_Catalog_Model_Product[]
     */
    public function getNextChunk() {
        $ret = array();
        foreach($this->loadNextChunk() as $product) {
            if ($this->isProductValid($product)) {
                $ret[] = $product;
            }
        }
        return $ret;
    }
    
    
    protected function loadNextChunk() 
    {
        if ( $this->hasReachLimit() ) {
            return array();
        }
        $this->getProducts()->clear();
        if ( $this->getChunkSize() > 0 ) {
            $this->getProducts()->setCurPage($this->page++);
        } else {
            $this->page = $this->size;
        }
        
        $this->websites()->runInAdminStore(array($this->getProducts(), 'load'));
        $this->afterChunkLoad();
        
        return $this->getProducts()->addOptionsToResult()->getItems();
    }
    
    protected function afterChunkLoad()
    {
        foreach ( $this->getProducts()->getItems() as $product ) {
            $this->websites()->runInAdminStore(array($this->getMediaGalleryAttributeBackend(), 'afterLoad'), array($product));
        }
    }
    
    public function hasReachLimit()
    {
        return $this->getChunkSize() > 0 
            && $this->getChunkSize() * ($this->page - 1) >= $this->size 
            || $this->page == $this->size && $this->size > 1; // TODO: it needs to be adjusted!
    }
    
    protected function isProductValid(Mage_Catalog_Model_Product $product)
    {
        $productId = $product->getId();
        // Make sure each product is only added to the feed once
        if(array_key_exists($productId, $this->productsInFeed) == true ||
        isset($this->productsInFeed[$productId])
        ) {
            if(Mage::getStoreConfig('buyablepins/general/debug_mode'))
            {
                Mage::log("Product " . $product->getSku() . " has already appeared once before in the feed. Not adding it a second time.", null, "creatuity_buyablepins_feed.log");
            }
            return false;
        }

        $this->productsInFeed[$productId] = $productId;

        foreach ( $product->getOptions() as $productOption ) {
            /* @var $productOption Mage_Catalog_Model_Product_Option */

            // If the extension MageWorx/CustomOptions is installed, we have to add true to the getIsRequire call
            // If that extension isn't installed, we need to call getIsRequire with no parameters
            // This is due to a breaking change MageWorx introduced to how getIsRequire works in their extension

            if($this->mageworxPresent)
            {
                $isRequire = $productOption->getIsRequire(true);
            } else
            {
                $isRequire = $productOption->getIsRequire();
            }

            if ( $isRequire ) {
                if(Mage::getStoreConfig('buyablepins/general/debug_mode'))
                {
                    Mage::log("Product " . $product->getSku() . " has required custom options - removing from feed.", null, "creatuity_buyablepins_feed.log");
                }
                return false;
            }
        }
        return true;
    }
    
    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function prepareProductsCollection() 
    {
        $collection = $this->newProductCollection()
            ->setStore($this->getProductsCollectionStore())
            ->addAttributeToSelect('description')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('manufacturer')
            ->addAttributeToSelect('image')
            ->setFlag('require_stock_items')
            ->addFinalPrice()              
        ;
        
        if ($this->getProductIds() !== null) {
            $collection->addIdFilter((array)$this->getProductIds());
        }
        
        return $collection;
    }
    
    /**
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    protected function newProductCollection()
    {
        return Mage::getResourceModel('catalog/product_collection');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Website
     */
    protected function websites() 
    {
        return Mage::helper('buyablepins/website');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() 
    {
        return Mage::helper('buyablepins');
    }
    
}