<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method string getAuthToken();
 * @method string getEndpoint();
 * @method array getPostFields();
 * @method array getExpectedResponseFields();
 */
class Creatuity_BuyablePins_Model_Feed_Request extends Varien_Object
{
    public function __construct(array $data = array())
    {
        parent::__construct($data + array(
            'auth_token' => $this->config()->feedAccessToken(),
            'endpoint' => $this->config()->feedEndpoint(),
            'post_fields' => array(
                'merchant_name' => $this->config()->feedExternalId(),
                'location' => trim(Mage::getUrl('buyablepins/feed/products'), '/'),
                'language' => 'EN',
                'country' => 'US',
            ),
            'expected_response_fields' => array('status', 'message', 'data'),
        ));
    }
    
    public function sendRequest()
    {
        $curl = curl_init();

        $curlOptions = array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->getEndpoint(),
            CURLOPT_POSTFIELDS => $this->getPostFields(),
        );
        
        if ( strlen($this->getAuthToken()) > 0 ) {
            $curlOptions += array(CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . $this->getAuthToken(),
                ),
            );
        }
        
        curl_setopt_array($curl, $curlOptions);
        
        $response = curl_exec($curl);
        curl_close($curl);

        if ( empty($response) ) {
            Mage::throwException('Empty response from endpoint: ' . $this->getEndpoint());
        }
        
        $response = json_decode($response, true);
        $this->ensureReponseIsExpected($response);
        
        if ( $response['status'] != 'success' ) {
            Mage::throwException($response['message']);
        }
        
        return $response['data'];
    }
    
    protected function ensureReponseIsExpected(array $response)
    {
        $commonResponseFields = array_intersect(array_keys($response), $this->getExpectedResponseFields());
        if ( array_values($commonResponseFields) != $this->getExpectedResponseFields() ) {
            Mage::throwException('Improper response from endpoint');
        }
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
}