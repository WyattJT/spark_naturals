<?php

class Creatuity_BuyablePins_Model_Payment_Replacer extends Varien_Object
{
    
    public function __construct(array $data)
    {
        parent::__construct($data + array(
            'payment_rewrites' => $this->config()->paymentRewrites(),
        ));
    }
    
    public function replaceOnTheFlyPaymentMethodsForPinterest()
    {
        foreach ($this->getPaymentRewrites() as $classGroup => $value) {
            $this->dynamicClassRewrite($classGroup, $value);
        }
    }
    
    protected function dynamicClassRewrite($classGroup, $value) 
    {
        list($moduleName, $className) = explode('/', $classGroup);
        Mage::app()->getConfig()->setNode("global/models/$moduleName/rewrite/{$className}", $value);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
}
