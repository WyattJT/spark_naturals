<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Payment_Method_PaypalWebsite extends Mage_Paypal_Model_Direct
{
    public function getConfigData($field, $storeId = null)
    {
        $result = parent::getConfigData($field, $storeId);
        
        if ( $field == 'cctypes' ) {
            $result .= (empty($result) ? '' : ',') . Creatuity_BuyablePins_Model_Config::CREDIT_CARD_OTHER_TYPE;
        }
        return $result;
    }
}