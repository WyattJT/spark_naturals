<?php

class Creatuity_BuyablePins_Model_Payment_Method_Litle 
    extends Litle_CreditCard_Model_PaymentLogic 
{

    public function litleCcTypeEnum(Varien_Object $payment) {
        if ($payment->getCcType() == 'OT') {
            return '';
        }
        return parent::litleCcTypeEnum($payment);
    }

}
