<?php

class Creatuity_BuyablePins_Model_Payment_Method_Braintree
    extends Braintree_Payments_Model_Creditcard
{
    
    protected function adjustTransactionParams(array &$braintreePayment, Varien_Object $magentoPayment)
    {
        $braintreePayment['creditCard'] = array(
            'number' => $magentoPayment->getCcNumber(),
            'expirationMonth' => $magentoPayment->getCcExpMonth(),
            'expirationYear' => $magentoPayment->getCcExpYear(),
        );
    }
    
    
    protected function _authorize (Varien_Object $payment, $amount, $capture, $token = false)
    {
        try {
            $order = $payment->getOrder();
            $orderId = $order->getIncrementId();
            $billing = $order->getBillingAddress();
            $shipping = $order->getShippingAddress();
            $transactionParams = array(
                'channel'   => $this->_getChannel(),
                'orderId'   => $orderId,
                'amount'    => $amount,
                'customer'  => array(
                    'firstName' => $billing->getFirstname(),
                    'lastName'  => $billing->getLastname(),
                    'company'   => $billing->getCompany(),
                    'phone'     => $billing->getTelephone(),
                    'fax'       => $billing->getFax(),
                    'email'     => $order->getCustomerEmail(),
                )
            );

            $customerId = Mage::helper('braintree_payments')
                ->generateCustomerId($order->getCustomerId(), $order->getCustomerEmail());
            
            if ($order->getCustomerId() && $this->exists($customerId)) {
                $transactionParams['customerId'] = $customerId;
                unset($transactionParams['customer']);
            } else {
                $transactionParams['customer']['id'] = $customerId;
            }

            if ($capture) {
                $transactionParams['options']['submitForSettlement'] = true;
            }

            if ($this->_merchantAccountId) {
                $transactionParams['merchantAccountId'] = $this->_merchantAccountId;
            }

            $token = $this->_getMethodSpecificAuthorizeTransactionToken($token);

            if ($token) {
                $nonce = Mage::helper('braintree_payments')->getNonceForVaultedToken($token);
                $transactionParams['customerId'] = $customerId;
            } else {
                $transactionParams['billing']  = $this->_toBraintreeAddress($billing);
                $transactionParams['shipping'] = $this->_toBraintreeAddress($shipping);
                $transactionParams['options']['addBillingAddressToPaymentMethod']  = true;
                $nonce = $payment->getAdditionalInformation('nonce');
            }
            
            $transactionParams['paymentMethodNonce'] = $nonce;

            $transactionParams = array_merge_recursive(
                $transactionParams,
                $this->_addMethodSpecificAuthorizeTransactionParams($payment)
            );

            if (isset($transactionParams['options']['storeInVault']) && 
                !$transactionParams['options']['storeInVault']) {

                $transactionParams['options']['addBillingAddressToPaymentMethod']  = false;
            }

            $this->_debug($transactionParams);
            try {
                // CREATUITY : begin
                $this->adjustTransactionParams($transactionParams, $payment);
                // CREATUITY : end
                $result = Braintree_Transaction::sale($transactionParams);
                $this->_debug($result);
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::throwException(Mage::helper('braintree_payments')->__('Please try again later'));
            }
            if ($result->success) {
                $this->setStore($payment->getOrder()->getStoreId());
                $payment = $this->_processSuccessResult($payment, $result, $amount);
            } else {
                Mage::throwException(Mage::helper('braintree_payments/error')->parseBraintreeError($result));
            }
        } catch (Exception $e) {
            $this->_processMethodSpecificAuthorizeTransactionError();
            throw new Mage_Payment_Model_Info_Exception($e->getMessage());
        }
        return $this;
    }
    
}
