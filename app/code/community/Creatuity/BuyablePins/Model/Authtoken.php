<?php

/**
 *
 * @category   Creatuity
 * @package    BuyablePins
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Model_Authtoken {
    
    
    public function generateNew() {
        $this->_config()->saveAuthToken($this->_newToken());
    }
    
    
    protected function _newToken() {
        return md5(time());
    }
    
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function _config() {
        return Mage::getSingleton('buyablepins/config');
    }
    
}
