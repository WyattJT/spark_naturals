<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 * 
 * @method Mage_Catalog_Model_Product getProduct();
 * @method Mage_Catalog_Model_Product getParentProduct();
 * @method string getImagesUrlsSeparator();
 */
class Creatuity_BuyablePins_Block_Feed_Product extends Mage_Core_Block_Template
{
    public function __construct(array $args = array())
    {
        parent::__construct($args + array(
            'template' => 'creatuity/buyablepins/feed/product.phtml',
            'images_urls_separator' => ',',
        ));
    }
    
    public function render(Mage_Catalog_Model_Product $product, Mage_Catalog_Model_Product $parentProduct = null)
    {
        $this->setProduct($product);
        $this->setParentProduct(!is_null($parentProduct) ? $parentProduct : Mage::getModel('catalog/product'));
        $rendered = $this->toHtml();
        $this->unsProduct();
        $this->unsParentProduct();
        return $rendered;
    }
    
    public function getId()
    {
        return $this->getProduct()->getId();
    }
    
    public function getName()
    {
        return $this->pinsHelper()->htmlToText($this->getParentName() 
            ? $this->getParentName() 
            : $this->getProduct()->getName());
    }
    
    public function getParentName()
    {
        return $this->pinsHelper()->htmlToText($this->getParentProduct()->getName());
    }
    
    public function getParentId()
    {
        return $this->getParentProduct()->getId();
    }
    
    public function getDescription()
    {
        return $this->pinsHelper()->description( $this->getProduct() );
    }
    
    public function getProductUrl()
    {
        if ( $this->getParentProduct()->isObjectNew() ) {
            // this is a stand-alone simple product
            return $this->approvedStoreProductUrl($this->getProduct());
        } else
        {
            // this is a product with a parent, so we need to use the parent's URL
            return $this->getParentUrl();
        }
    }
    
    public function getParentUrl()
    {   
        if ( $this->getParentProduct()->isObjectNew() ) {
            return false;
        }
        return $this->approvedStoreProductUrl($this->getParentProduct());
    }

    public function getSize()
    {
        if ( $this->getParentProduct()->isObjectNew() ) {
            return false;
        }

        $parentProduct = $this->getParentProduct();
        $configurable = $parentProduct->getTypeInstance();
        $attributes = $configurable->getConfigurableAttributes($parentProduct);

        $product = $this->getProduct();

        // for now, we only support one attribute per product - Pinterest limitation

        if(count($attributes) > 1)
        {
            if(Mage::getStoreConfig('buyablepins/general/debug_mode'))
            {
                Mage::log("Product " . $product->getSku() . " has more than one size/color option - removing from feed due to Pinterest limitation.", null, "creatuity_buyablepins_feed.log");
            }
            return false;
        }

        foreach ($attributes as $attribute)
        {
            $attributeLabel = $attribute->getLabel();
            $attributeId = $attribute->getId();
            $attributeCode = $attribute->getProductAttribute()->getAttributeCode();
        }
        $size = Mage::getModel('catalog/product')->load($product->getId())->getAttributeText($attributeCode);
        return $size;
    }
    
    public function getAdditionalImagesUrlsAsString()
    {
        return implode($this->getImagesUrlsSeparator(), $this->getAdditionalImagesUrls());
    }
    
    public function getAdditionalImagesUrls()
    {
        $additonalImagesUrls = array();
        if ( !$this->getProduct()->getMediaGalleryImages() ) {
            return $additonalImagesUrls;
        }
        foreach ( $this->getProduct()->getMediaGalleryImages() as $imageData ) {
            $additonalImagesUrls[] = $imageData->getUrl();
        }
        return array_diff($additonalImagesUrls, array($this->getImageUrl()));
    }
    
    public function getImageUrl()
    {
        if ( $this->getParentProduct()->isObjectNew() ) {
            // simple product - pull images from itself
            $this->getProduct()->setImage( ltrim($this->getProduct()->getImage(), '/') );
            return $this->productHelper()->getImageUrl( $this->getProduct() );
        } else
        {
            // configurable product - pull images from the parent
            $this->getParentProduct()->setImage( ltrim($this->getParentProduct()->getImage(), '/') );
            return $this->productHelper()->getImageUrl( $this->getParentProduct() );
        }


    }
    
    public function getCondition()
    {
        return 'new';
    }
    
    public function getStockStatus()
    {
        return $this->getProduct()->isAvailable() ? 'in stock' : 'out of stock';
    }
    
    public function getPrice()
    {
        $price = $this->getProduct()->getFinalPrice();
        return $this->pinsHelper()->formatPrice($price);
    }
    
    public function getManufacturer()
    {
        return $this->pinsHelper()->brand( $this->getProduct() );
    }
    
    public function getGtin()
    {
        return $this->pinsHelper()->gtin( $this->getProduct() );
    }

    /**
     * Change the product URL so that the URL matches what Pinterest has.
     * A new product has to be created to get the correct store URL since there is not a more reliable method.
     * We know it's not great, but this is the solution we found.
     */

    protected function approvedStoreProductUrl($product) 
    {
        $url_store_id = $this->pinsHelper()->toStoreId($this->config()->feedApprovedStoreCode());
        $product_url = Mage::getModel('catalog/product')->setStoreId($url_store_id)->load($product->getId())->getProductUrl();
        $product_url = explode("?", $product_url);
        $product_url = $product_url[0];
    
        return $product_url;
    }
    
    /**
     * @return Mage_Catalog_Helper_Product
     */
    protected function productHelper()
    {
        return Mage::helper('catalog/product');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function pinsHelper()
    {
        return Mage::helper('buyablepins');
    }

    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
}