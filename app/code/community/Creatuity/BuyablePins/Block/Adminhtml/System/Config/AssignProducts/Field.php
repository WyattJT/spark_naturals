<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Block_Adminhtml_System_Config_AssignProducts_Field extends Mage_Adminhtml_Block_System_Config_Form_Field 
{

    protected function _construct() 
    {
        parent::_construct();
        $this->setTemplate('creatuity/buyablepins/assignproducts_field.phtml');
    }
    
    public function render(\Varien_Data_Form_Element_Abstract $element) 
    {
        $this->setElement($element);
        return parent::render($element);
    }
    
    protected function renderElement($style) 
    {
        if (!$this->getElement()) {
            return '';
        }
        return $this->getElement()->setStyle($style)->getElementHtml();
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
    {
        return $this->_toHtml();
    }

    protected function renderButtonHtml($label, $onClick) 
    {
        $buttonOptions = array(
            'id' => 'buyablepins_assign_products_button',
            'label' => $this->__($label),
            'onclick' => $onClick,
        );
        
        return $this->getLayout()->createBlock('adminhtml/widget_button', 'buyablepins_authkey_button', $buttonOptions)->toHtml();
    }
    
    protected function getAssignProductsUrl() 
    {
        return $this->getUrl('*/buyablepins/assignProducts');
    }
}
