<?php
class Creatuity_BuyablePins_Block_Adminhtml_System_Config_Form_Fieldset_Support
    extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    protected $_dummyElement;
    protected $_fieldRenderer;
    protected $_values;

    public function render(Varien_Data_Form_Element_Abstract $element)
    {

        $html = "<div style='margin-left: 10px; margin-top: 10px'>";
        $html .= "</div><h4>90 days of support is included with your extension purchase. Additional support available for purchase.</h4>";
        $html .= "<h4>For support, please email <a href='pbpsupport@creatuity.com'>pbpsupport@creatuity.com</a>, including your merchant ID number listed below. No phone support is offered.</h4>";
        $html .= "<h4>Creatuity also provides <a href='http://creatuity.com/pinterest-marketing-packages/'>Pinterest Marketing Packages</a> to help drive more traffic to your Buyable Pins.</h4>";
        $html .= "<h4>Creatuity is also available for your general Magento support needs on an hourly basis with no minimums. Learn more at <a href='http://creatuity.com'>Creatuity.com</a></h4>";
        $html .= "<h4>View the <a href='http://creatuity.com/pbpmanual'>extension manual</a> and the <a href='http://creatuity.com/pbpkb'>extension knowledge base.</a></h4>";
        $html .= "</div>";

        return $html;
    }
}

