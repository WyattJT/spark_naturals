<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Block_Adminhtml_System_Config_IsActive_Field extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _construct() 
    {
        parent::_construct();
        $this->setTemplate('creatuity/buyablepins/is_active.phtml');
    }
    
    public function render(\Varien_Data_Form_Element_Abstract $element) 
    {
        $this->setElement($element);
        return parent::render($element);
    }
    
    public function renderElement($style) 
    {
        if ($this->getElement()) {
            return $this->getElement()->setStyle($style)->getElementHtml();
        }
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
    {
        return $this->_toHtml();
    }

    public function renderDeactivateButtonHtml($label, $confirmMsg)
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button', 'buyablepins_disable_button', array(
            'label' => $this->__($label),
            'onclick' => "if (confirm('{$this->__($confirmMsg)}')) { window.location='{$this->_activatePins(0)}' }; return false;",
        ))->toHtml();
    }

    public function renderActivateButtonHtml($label, $confirmMsg)
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button', 'buyablepins_disable_button', array(
            'label' => $this->__($label),
            'onclick' => "if (confirm('{$this->__($confirmMsg)}')) { window.location='{$this->_activatePins(1)}' }; return false;",
        ))->toHtml();
    }
    
    protected function _activatePins($active = 1)
    {
        Mage::log('activate value in _activatePins: ' . $active);
        return $this->getUrl('*/buyablepins/changeActivation',array('activate_pins'=> $active));
    }



    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config()
    {
        return Mage::getSingleton('buyablepins/config');
    }

    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function activation()
    {
        return Mage::getSingleton('buyablepins/activation');
    }
}
