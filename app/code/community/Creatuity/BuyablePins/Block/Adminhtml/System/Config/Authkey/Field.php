<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Block_Adminhtml_System_Config_Authkey_Field extends Mage_Adminhtml_Block_System_Config_Form_Field 
{

    protected function _construct() 
    {
        parent::_construct();
        $this->setTemplate('creatuity/buyablepins/authkey_field.phtml');
    }
    
    public function render(\Varien_Data_Form_Element_Abstract $element) 
    {
        $this->setElement($element);
        return parent::render($element);
    }
    
    public function renderElement($style) 
    {
        if ($this->getElement()) {
            return $this->getElement()->setStyle($style)->getElementHtml();
        }
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
    {
        return $this->_toHtml();
    }

    public function renderButtonHtml($label, $confirmMsg) 
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button', 'buyablepins_authkey_button', array(
            'label' => $this->__($label),
            'onclick' => "if (confirm('{$this->__($confirmMsg)}')) { window.location='{$this->_rebuildAuthTokenUrl()}' }; return false;",
        ))->toHtml();
    }
    
    protected function _rebuildAuthTokenUrl() 
    {
        return $this->getUrl('*/buyablepins/generateAuthcode');
    }
}
