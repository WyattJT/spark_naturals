<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_Block_Adminhtml_System_Config_FeedRequest_Field
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _construct() 
    {
        parent::_construct();
        $this->setTemplate('creatuity/buyablepins/feed_request.phtml');
    }
    
    public function render(\Varien_Data_Form_Element_Abstract $element) 
    {
        $this->setElement($element);
        return parent::render($element);
    }
    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
    {
        return $this->_toHtml();
    }

    public function renderButtonHtml($label) 
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button', 'buyablepins_feed_request_button', array(
            'label' => $this->__($label),
            'onclick' => "window.location='{$this->sendFeedRequestUrl()}'; return false;",
        ))->toHtml();
    }
    
    protected function sendFeedRequestUrl() 
    {
        return $this->getUrl('*/buyablepins/sendFeedRequest');
    }
}