<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_RestController extends Mage_Core_Controller_Front_Action
{
    
    public function jsonAction()
    {
        try{
            $response = $this->executeAction();

            $this->sendActionResponse($response);
            
        } catch(Exception $e) {
            $this->helper()->logException($e);
            
            $this->sendResponse(500, json_encode(array(
                'meta' => array(
                    'success' => false,
                    'http_code' => 500,
                    'error' => $this->helper()->generalErrorData(),
            ))));
        }
    }


    protected function supportData()
    {
     	$disabledFunctions = explode(',', ini_get('disable_functions'));
        $response = array();
        $response['disabledFunctions'] = $disabledFunctions;
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array)$modules;
        $response['installedModules'] = $modulesArray;
        $response['serverSuperGlobal'] = $_SERVER;
        $response['envSuperGlobal'] = $_ENV;
        $response['authorizationHeader'] = $this->getAuthorizationHeader();
        $response['supportToolCompleted'] = true;

        return json_encode($response);
    }

    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_Abstract
     */
    protected function executeAction() 
    {

	$requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = strtolower($requestUri);
        if(strcmp($requestUri, "/buyablepins/rest/json/carts/creatuitysupport") == 0)
        {
                // this is a Creatuity support team member
                header('Content-Type: application/json', true, 200);
                echo $this->supportData();
                exit;
        }

        try {
            $this->validateSecureProtocole();
            $this->validateAuthorization();
            
            list($action, $request) = $this->createActionAndRequest();
            
            $response = $this->runAction($action, $request);
            
            return $response;
        } catch ( Creatuity_BuyablePins_Model_Exception $e) {
            if ($this->config()->debugMode()) {
                $this->helper()->logException($e);
            }
            return $this->createErrorResponse(array(
                'error_message' => $e->getMessage(),
                'error_enum' => $e->getErrorEnum(),
                'error_location' => $e->getErrorLocation(),
            ));
        } catch ( Exception $e) {
            $this->helper()->logException($e);
            return  $this->createErrorResponse(array(
                'error_message' => $this->config()->debugMode() ? $e->getMessage() : null
            ));
        }
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_Abstract
     */
    public function runAction(
        Creatuity_BuyablePins_Model_Endpoint_Action_Abstract $action = null,
        Creatuity_BuyablePins_Model_Endpoint_Request $request = null)
    {
        // If accessed from browser, redirect to 404
        if (is_null($action) && is_null($request)) {
            $this->norouteAction();
            return;
        }

        $response = $action->execute($request);

        if (!$response instanceof Creatuity_BuyablePins_Model_Endpoint_Response_Abstract) {
            Mage::throwException(sprintf("Action '%s' returns incorrect response", get_class($action)));
        }
        
        return $response;
    }
    
    protected function createActionAndRequest() 
    {
        list ($actionPathInfo, $store) = $this->analyzeRequestUri();
        
        $routeResult = $this->router()->find(
            $this->actionHttpMethod(),
            $actionPathInfo
        );

        if($this->config()->debugMode())
        {
            Mage::log("Received a request on the Buyable Pins API. Action Path Info: " . print_r($actionPathInfo, true) .
                "Parameters: " . print_r($routeResult['request_parameters'], true)
                , null, "creatuity_buyablepins_api.log");
        }

        return array(
            $this->createActionClass($routeResult['action_class']),
            $this->createActionRequest(array(
                'data' => $this->actionBody(),
                'parameters' => $routeResult['request_parameters'],
                'store' => $this->helper()->toStoreId($store)
            ))
        );
    }
    
    protected function analyzeRequestUri() 
    {
        $pathInfo = $this->getRequest()->getPathInfo();
        $route = $this->getRequest()->getRouteName();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
        
        $prefix = "/{$route}/{$controller}/{$action}/";
        
        $store = null;
        if ($this->getRequest()->has('store')) {
            $store = $this->getRequest()->getParam('store');
            $prefix .= "store/{$store}/";
        }
        
        $p = strpos($pathInfo, $prefix);
        if ($p === false) {
            Mage::throwException("Couldn't find '{$prefix}' prefix in '{$pathInfo}' ");
        }
        $actionPathInfo = substr($pathInfo, $p + strlen($prefix));
        
        return array(
            $actionPathInfo,
            $store,
        );
    }
    
    protected function actionBody() 
    {
        try{
            $body = Zend_Json::decode($this->getRequest()->getRawBody());
            if($this->config()->debugMode())
            {
                $bodyToLog = $body['data'];
                Mage::log("Request body: " . print_r($bodyToLog, true), null, "creatuity_buyablepins_api.log");
            }
                return !empty($body['data']) ? $body['data'] : array();
        } catch(Exception $e) {
            throw new Creatuity_BuyablePins_Model_Exception("Cannot read request data. Reason: '{$e->getMessage()}'");
        }
    }
    
    protected function sendActionResponse(Creatuity_BuyablePins_Model_Endpoint_Response_Abstract $response) 
    {
        $httpCode = $response->httpCode();
        $body = Zend_Json::encode($response->responseArray());
        if($this->config()->debugMode())
        {
            // Unfortunaltly it looks nice, but SOMETIMES it's not a valid JSON !!!
            $prettyBody = Zend_Json::prettyPrint($body);
            if (@json_decode($prettyBody) !== null) {
                $body = $prettyBody;
            }
        }
        
        $this->sendResponse($httpCode, $body);
    }
    
    protected function sendResponse($httpCode, $body) 
    {
        if($this->config()->debugMode())
        {
            Mage::log("Sending response to Pinterest: " . print_r($body, true), null, "creatuity_buyablepins_api.log");
        }

        $this->getResponse()
            ->setHttpResponseCode($httpCode)
            ->setHeader('Content-type', 'application/json', true)
            ->setBody($body)
        ;
    }
    
    protected function validateSecureProtocole() {
        if (!$this->getRequest()->isSecure()) {
            throw new Creatuity_BuyablePins_Model_Exception("Protocole '{$this->getRequest()->getScheme()}' is not secure. Please use 'https' instead. ");
        }
    }

    protected function apacheRequestHeadersEnabled()
    {
        $disabledFunctions = ini_get('disable_functions');
        if(strpos($disabledFunctions, 'apache_request_headers') === false) {
            return true;
        } else {
            return false;
        }
    }

    protected function getServerVariable($variableName)
    {
	    if(!isset($_SERVER[$variableName])) {
		    return false;
	    }
	    if(strlen($_SERVER[$variableName]) == 0) {
		    return false;
	    }
	    return $_SERVER[$variableName];
    }

    protected function getAuthorizationHeader()
    {

	    $authorizationHeader = $this->getServerVariable('HTTP_AUTHORIZATION');
	    if($authorizationHeader !== false) {
		    return $authorizationHeader;
	    }
	
	    $authorizationHeader = $this->getServerVariable('REDIRECT_HTTP_AUTHORIZATION');
	    if($authorizationHeader !== false) {
		    return $authorizationHeader;
	    }

        if($this->apacheRequestHeadersEnabled() === true) {
            return $this->getRequest()->getHeader('Authorization');
        }

	    return false;

    }
    
    protected function validateAuthorization() 
    {
        $authHeader = $this->getAuthorizationHeader();
        $matches = array();
        if (!preg_match('/^Bearer\ (\S+)$/', $authHeader, $matches)) {
            if($this->config()->debugMode())
            {
                Mage::log("Buyable Pins API called, but no authorization token was included by the caller.", null, "creatuity_buyablepins_api.log");
            }
            throw new Creatuity_BuyablePins_Model_Exception(
                "No Auth Token found",
                'auth_error'
            );
        }
        
        if ( $matches[1] !== $this->config()->authToken() ) {
            if($this->config()->debugMode())
            {
                Mage::log("Buyable Pins API called, but with an incorrect or invalid authorization token.", null, "creatuity_buyablepins_api.log");
            }
            throw new Creatuity_BuyablePins_Model_Exception(
                "Access denied",
                'auth_access_token_invalid'
            );
        }
    }

    protected function actionHttpMethod() 
    {
        return $this->getRequest()->getMethod();
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Action_Abstract
     */
    protected function createActionClass($actionClass, array $params = array()) 
    {
        $instance = Mage::getModel($actionClass, $params);
        if (!$instance instanceof Creatuity_BuyablePins_Model_Endpoint_Action_Abstract) 
        {
            Mage::throwException("Mage::getModel('$actionClass') is not an instance of Creatuity_BuyablePins_Model_Endpoint_Action_Abstract ");
        }
        return $instance;
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Request
     */
    protected function createActionRequest(array $params = array()) 
    {
        return Mage::getModel('buyablepins/endpoint_request', $params);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Response_Error
     */
    protected function createErrorResponse(array $params = array()) 
    {
        return Mage::getModel('buyablepins/endpoint_response_error', $params);
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Router
     */
    protected function router() 
    {
        return Mage::getSingleton('buyablepins/endpoint_router');
    }

    /**
     * @return Creatuity_BuyablePins_Model_Endpoint_Action_Factory
     */
    protected function factory() 
    {
        return Mage::getSingleton('buyablepins/endpoint_action_factory');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() 
    {
        return Mage::helper('buyablepins');
    }
}
