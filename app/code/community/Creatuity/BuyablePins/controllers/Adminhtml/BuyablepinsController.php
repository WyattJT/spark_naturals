<?php

/**
 *
 * @category   Creatuity
 * @package    bp
 * @copyright  Copyright (c) 2008-2014 Creatuity Corp. (http://www.creatuity.com)
 * @license    http://creatuity.com/license
 */
class Creatuity_BuyablePins_Adminhtml_BuyablepinsController extends Mage_Adminhtml_Controller_Action 
{
    
    public function generateAuthcodeAction() 
    {
        try {
            $this->authToken()->generateNew();
            
            $this->_getSession()->addSuccess($this->__('Authorization token has been generated successfully'));
        } catch ( Exception $e ) {
            $this->helper()->logException($e);
            $this->_getSession()->addError($this->__('Error during token generate: %s', $e->getMessage()));
        }
        
        return $this->_redirectReferer();
    }
    
    public function sendFeedRequestAction()
    {
        try {
            $response = $this->feedRequest()->sendRequest();
            
            $this->_getSession()->addSuccess($this->__('Your request has been sent successfully. It\'s id is: %s', $response['request_id']));
        } catch ( Exception $e ) {
            $this->helper()->logException($e);
            $this->_getSession()->addError($this->__('Error during send request: %s', $e->getMessage()));
        }
        $this->_redirectReferer();
    }
    
    public function assignProductsAction()
    {
        try {
            if (!$this->getRequest()->isAjax()) {
                $this->_forward('noRoute');
                return;
            }
            
            $fromWebsiteCode = $this->getRequest()->getParam('origWebsite');
            $toWebsiteCode = Creatuity_BuyablePins_Helper_Website::PINTEREST_STORE_CODE;
            
            $fromWebsiteName = Mage::app()->getWebsite($fromWebsiteCode)->getName();
            $toWebsiteName = Mage::app()->getWebsite($toWebsiteCode)->getName();
            
            if (empty($fromWebsiteCode) || empty($toWebsiteCode)) {
                Mage::throwException('Missing parameters');
            }
            
            $this->productsAssigner()->assignAllProducts(
                $fromWebsiteCode, 
                $toWebsiteCode
            );
            
            $this->sendJsonResponse(200, array(
                'message' => "Successfuly assigned all products from website '$fromWebsiteName' to '$toWebsiteName'",
            ));
        } catch (Exception $e) {
            $this->helper()->logException($e);
            $this->sendJsonResponse(500, array(
                'message' => $e->getMessage(),
            ));
        }
    }

    public function changeActivationAction()
    {
        $activate = $this->getRequest()->getParam('activate_pins');
        if(!$this->activation()->checkRequirements() && $activate == 1)
            $this->_getSession()->addError($this->__('Server Requirements not active. Cannot activate'));
        else {
            try {
                $this->activation()->changeBPActivation($activate);
                if ($activate == 1) {
                    $this->_getSession()->addSuccess($this->__('Buyable Pins has been activated'));
                } else {
                    $this->_getSession()->addSuccess($this->__('Buyable Pins has been deactivated'));
                }
            } catch (Exception $e) {
                $this->helper()->logException($e);
                $this->_getSession()->addError($this->__('Error during activation setting: %s', $e->getMessage()));
            }
        }

        return $this->_redirectReferer();
    }


    protected function sendJsonResponse($code, array $response) {
        $this->getResponse()->setHttpResponseCode($code);
        $this->getResponse()->setHeader('Content-Type', 'application/json');
        echo json_encode($response);
    }
    
    /**
     * @return  Creatuity_BuyablePins_Model_Product_Website
     */
    protected function productsAssigner() {
        return Mage::getSingleton('buyablepins/product_website');
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Authtoken
     */
    protected function authToken()
    {
        return Mage::getSingleton('buyablepins/authtoken');
    }

    /**
     * @return Creatuity_BuyablePins_Model_Authtoken
     */
    protected function activation()
    {
        return Mage::getSingleton('buyablepins/activation');
    }

    
    /**
     * @return Creatuity_BuyablePins_Model_Feed_Request
     */
    protected function feedRequest() 
    {
        return Mage::getModel('buyablepins/feed_request');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() 
    {
        return Mage::helper('buyablepins');
    }
}
