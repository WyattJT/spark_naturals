<?php

/**
 * @category Creatuity
 * @package bp
 * @copyright Copyright (c) 2008-2015 Creatuity Corp. (http://www.creatuity.com)
 * @license http://www.creatuity.com/license
 */
class Creatuity_BuyablePins_FeedController extends Mage_Core_Controller_Front_Action
{
    public function productsAction()
    {
        $this->websites()->runInPinterestStore(array($this, 'productsFeed'));
    }
    
    public function productsFeed() 
    {
        if($this->config()->debugMode())
        {
            Mage::log("Generating product feed.", null, "creatuity_buyablepins_feed.log");
        }
        try {
            $isDirectOutput = $this->config()->feedDirectOutput();

            $this->getResponse()->setHeader('Content-type', 'application/xml', true);

            if($this->config()->debugMode())
            {
                $this->getResponse()->setHeader('Content-Disposition', sprintf('attachment; filename=%s', $this->config()->feedFileName()));
            }
            
            if ( $isDirectOutput ) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();
                $this->newFeedContent(true)->generate();
            } else {
                $feedOutput = $this->newFeedContent(false)->generate();
                
                $this->getResponse()
                    ->setHeader('Content-Length', strlen($feedOutput))
                    ->setBody($feedOutput);
            }
            
        } catch ( Exception $e ) {
            $this->helper()->logException($e);
            
            $this->getResponse()
                ->setHttpResponseCode( 500 )
                ->setBody('An error has occured. Please try again later.');
        }
    }


    /**
     * @return Creatuity_BuyablePins_Model_Feed_Content
     */
    protected function newFeedContent($withDirectOutput = false)
    {
        return Mage::getModel('buyablepins/feed_content', array('direct_output' => $withDirectOutput));
    }
    
    /**
     * @return Creatuity_BuyablePins_Model_Config
     */
    protected function config() 
    {
        return Mage::getSingleton('buyablepins/config');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Website
     */
    protected function websites() 
    {
        return Mage::helper('buyablepins/website');
    }
    
    /**
     * @return Creatuity_BuyablePins_Helper_Data
     */
    protected function helper() 
    {
        return Mage::helper('buyablepins');
    }
}
