<?php
/**
 * @namespace   Crimson
 * @module      Aanalytics
 * @file        Observer.php
 * @author      icoast@crimsonagility.com
 * @date        7/29/2015 7:53 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_Analytics_Model_Observer
 */
class Crimson_Analytics_Model_Observer
{
    public function addPaymentEvent(Varien_Event_Observer $observer)
    {
        $this->_getHelper()->addSessionEvent('add_payment_info');
    }

    public function addToCart(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Quote_Item[] $items */
        $items = $observer->getItems();

        /** @var Mage_Sales_Model_Quote_Item $item */
        $item = reset($items);

        $eventData = array(
            'currency' => $item->getQuote()->getBaseCurrencyCode(),
        );
        foreach ($items as $item) {
            $eventData['items'][] = array(
                'sku' => $item->getSku(),
                'amount' => $item->getBasePrice()*((float) $item->getQtyToAdd()),
            );
        }

        $this->_getHelper()->addSessionEvent($eventData,'add_to_cart');
    }

    public function addToWishlist(Varien_Event_Observer $observer)
    {
        $this->_getHelper()->addSessionEvent('add_to_wishlist');
    }

    public function completeRegistration(Varien_Event_Observer $observer)
    {
        $this->_getHelper()->addSessionEvent('complete_registration');
    }

    public function initiateCheckout(Varien_Event_Observer $observer)
    {
        $this->_getHelper()->addSessionEvent('initiate_checkout');
    }

    public function purchaseComplete(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order[] $orders */
        $orders = $observer->getOrders();

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        if ($order) {
            $orders = array($order);
        }

        $orderIds = array();
        foreach ($orders as $order) {
            $orderIds[] = $order->getId();
        }

        $this->_getHelper()->addSessionEvent($orderIds,'purchase');
    }

    protected function _getSession()
    {
        return Mage::getSingleton('core/session');
    }

    /**
     * @return Crimson_Analytics_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('crimson_analytics');
    }
}