<?php
/**
 * @namespace   Crimson
 * @module      Analytics
 * @file        Abstract.php
 * @author      icoast@crimsonagility.com
 * @date        7/28/2015 6:31 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Analytics_Block_Analytics_Abstract
 */
abstract class Crimson_Analytics_Block_Analytics_Abstract extends Mage_Core_Block_Template
{
    abstract public function getCode();

    public function getCacheKeyInfo()
    {
        $key = parent::getCacheKeyInfo();
        $key = array_merge($key,array(
            'ANALYTIC_CODE_'.$this->getCode(),
        ));

        if ($actionKey = $this->helper('crimson_analytics')->getFullActionName()) {
            $key = array_merge($key,array(
                'ACTION_'.$actionKey
            ));
        }

        return $key;
    }
}