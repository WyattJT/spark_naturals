<?php
/**
 * @namespace   Crimson
 * @module      Analytics
 * @file        Facebook.php
 * @author      icoast@crimsonagility.com
 * @date        7/28/2015 6:15 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Analytics_Block_Facebook
 */
class Crimson_Analytics_Block_Facebook extends Crimson_Analytics_Block_Analytics_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('crimson/analytics/facebook.phtml');
    }

    public function getCode()
    {
        return 'facebook';
    }

    public function getPixelCode()
    {
        return Mage::getStoreConfig(
            Crimson_Analytics_Helper_Data::XPATH_SYSTEM_CONFIG_FACEBOOK_PIXEL_ID
        );
    }

    protected function _toHtml()
    {
        if (!Mage::getStoreConfigFlag(
            Crimson_Analytics_Helper_Data::XPATH_SYSTEM_CONFIG_FACEBOOK_ENABLED
        )
        ) {
            return '';
        }

        return parent::_toHtml();
    }

    /**
     * ref: https://www.facebook.com/help/402791146561655
     *
     * @return array
     */
    public function getTrackEvents()
    {
        if (!$this->_getData('track_events')) {
            $actionKey     = $this->helper('crimson_analytics')->getFullActionName();
            $sessionEvents = $this->helper('crimson_analytics')->getSessionEvents();
            if (!$sessionEvents) {
                $sessionEvents = array();
            }

            $events = array();
            $eventData = array();

            //catalog category & product pages
            $contentKeys = array(
                'catalog_product_view',
                'catalog_category_view',
                'quickshop_index_view',
            );
            if ($additional = Mage::getStoreConfig(
                Crimson_Analytics_Helper_Data::XPATH_SYSTEM_CONFIG_FACEBOOK_ADDITIONAL_CONTENT_PAGES
            )
            ) {
                $contentKeys = array_merge($contentKeys, explode(',', $additional));
            }

            if (in_array($actionKey, $contentKeys)) {
                $helper = Mage::helper('catalog');
                switch ($actionKey) {
                    case 'catalog_product_view':
                    case 'quickshop_index_view':
                        $contentIds = array();
                        if ($helper->getProduct() && $helper->getProduct()->getId()) {
                            $contentIds[] = $helper->getProduct()->getSku();
                        }
                        $eventData = array(
                            'content_type' => 'product',
                            'content_ids' => $contentIds,
                            'content_name' => $helper->getProduct()->getName(),
                        );
                        break;
                    case 'catalog_category_view':
                        $contentIds = array();
                        if ($helper->getCategory() && $helper->getCategory()->getId()) {
                            $contentIds[] = $helper->getCategory();
                        }
                        $eventData = array(
                            'content_type' => 'category',
                            'content_ids' => $contentIds,
                            'content_name' => $helper->getCategory()->getName(),
                        );
                        break;
                }
                $events['ViewContent'] = $eventData;
            }

            //search - check known search actions.
            if (in_array('search', $sessionEvents)
                || in_array(
                    $actionKey, array(
                        'catalogsearch_result_index',
                        'catalogsearch_advanced_result',
                    )
                )
            ) {
                $events[] = 'Search';
            }

            //add to cart
            if (isset($sessionEvents['add_to_cart']) && ($eventData = $sessionEvents['add_to_cart'])) {
                $contentIds = array();
                $amount = 0;
                foreach ($eventData['items'] as $item) {
                    $contentIds[] = $item['sku'];
                    $amount += $item['amount'];
                }
                $events['AddToCart'] = array(
                    'content_name'     => 'Shopping Cart',
                    'content_ids'      => $contentIds,
                    'content_type'     => 'product',
                    'value'            => $amount,
                    'currency'         => $eventData['currency']
                );
            }

            //add to wishlist
            if (in_array('add_to_wishlist', $sessionEvents)) {
                $events[] = 'AddToWishlist';
            }

            //initiate checkout
            if (in_array('initiate_checkout', $sessionEvents)) {
                $events[] = 'InitiateCheckout';
            }

            //add payment info
            if (in_array('add_payment_info', $sessionEvents)) {
                $events[] = 'AddPaymentInfo';
            }

            //complete registration
            if (in_array('complete_registration', $sessionEvents)) {
                $events[] = 'CompleteRegistration';
            }

            //purchase
            if (isset($sessionEvents['purchase']) && ($orderIds = $sessionEvents['purchase'])) {
                $orders    = $this->_getOrderCollection($orderIds);

                if ($orders) {
                    $purchases = array();
                    foreach ($orders as $order) {
                        foreach ($order->getAllItems() as $item) {
                            /** @var Mage_Sales_Model_Order_Item $item */
                            if ($item->getBaseRowTotal() < 0.01) {
                                continue;
                            }

                            $purchases[] = array(
                                'content_name' => $item->getName(),
                                'content_ids'  => array($item->getSku()),
                                'content_type' => 'product',
                                'value'        => round($item->getRowTotal(), 2),
                                'currency'     => $order->getOrderCurrencyCode(),
                            );
                        }
                    }

                    if (!empty($purchases)) {
                        $events['Purchase'] = $purchases;
                    }
                }
            }

            $this->setData('track_events', $events);
        }

        return $this->_getData('track_events');
    }

    protected function _getOrderIds()
    {
        $sessionEvents = $this->helper('crimson_analytics')->getSessionEvents();
        if (!$sessionEvents) {
            $sessionEvents = array();
        }

        if (isset($sessionEvents['purchase']) && ($orderIds = $sessionEvents['purchase'])) {
            return $orderIds;
        }

        return array();
    }

    public function getOrderTotal()
    {
        $orders    = $this->_getOrderCollection();
        if (!$orders) {
            return 0;
        } else {
            return array_sum($orders->getColumnValues('base_grand_total'));
        }
    }

    public function getOrderCurrency()
    {
        $orders    = $this->_getOrderCollection();
        if (!$orders) {
            return Mage::app()->getStore()->getCurrentCurrencyCode();
        } else {
            return $orders->getFirstItem()->getStoreCurrencyCode();
        }
    }

    /**
     * @param null $orderIds
     *
     * @return bool|Mage_Sales_Model_Resource_Order_Collection
     */
    protected function _getOrderCollection($orderIds = null)
    {
        if (is_null($orderIds)) {
            $orderIds = $this->_getOrderIds();
        }

        if (!$orderIds) {
            return false;
        }

        $key = 'order_collection_'.md5(implode(',',$orderIds));

        if (!$this->_getData($key)) {
            $this->setData($key,Mage::getResourceModel('sales/order_collection')
                ->addFieldToFilter('entity_id', $orderIds));
        }

        return $this->_getData($key);
    }

    /**
     * @return Mage_Core_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('core/session');
    }

    public function getCacheKeyInfo()
    {
        $key = parent::getCacheKeyInfo();

        if ($trackEvents = $this->getTrackEvents()) {
            $eventKeyInfo = array();
            foreach ($trackEvents as $key => $eventData) {
                if (is_array($eventData)) {
                    $eventKeyInfo[] = $key;
                } else {
                    $eventKeyInfo[] = $eventData;
                }
            }

            if (!empty($eventKeyInfo)) {
                $key = array_merge($key, $eventKeyInfo);
            }
        }

        return $key;
    }

    /**
     * Retrieve checkout session model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckoutSession()
    {
        return Mage::getSingleton('checkout/session');
    }
}