<?php
/**
 * @namespace   Crimson
 * @module      Analytics
 * @file        Data.php
 * @author      icoast@crimsonagility.com
 * @date        7/28/2015 6:14 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_Analytics_Helper_Data
 */
class Crimson_Analytics_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XPATH_SYSTEM_CONFIG_FACEBOOK_ENABLED = 'crimson_analytics/facebook/enabled';
    const XPATH_SYSTEM_CONFIG_FACEBOOK_PIXEL_ID = 'crimson_analytics/facebook/pixel_id';
    const XPATH_SYSTEM_CONFIG_FACEBOOK_ADDITIONAL_CONTENT_PAGES = 'crimson_analytics/facebook/additional_content_pages';

    protected $_sessionEvents = array();

    /**
     * @return string
     */
    public function getFullActionName()
    {
        if (Mage::app()->getFrontController()->getAction()) {
            return Mage::app()->getFrontController()->getAction()->getFullActionName();
        }

        return '';
    }

    /**
     * @return array
     */
    public function getSessionEvents()
    {
        if (empty($this->_sessionEvents)) {
            //clear
            $this->_sessionEvents = $this->_getSession()->getData('analytic_events', true);
            if (!$this->_sessionEvents) {
                $this->_sessionEvents = array();
            }
        }

        return $this->_sessionEvents;
    }

    public function addSessionEvent($event, $key = '')
    {
        $sessionEvents = $this->getSessionEvents();
        if (!in_array($event, $sessionEvents)) {
            if ($key) {
                $sessionEvents[$key] = $event;
            } else {
                $sessionEvents[] = $event;
            }
        }

        $this->_getSession()->setData('analytic_events', $sessionEvents);

        return $this;
    }

    /**
     * @return Mage_Core_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('core/session');
    }
}