<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Observer.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Model_Observer
 */
class Crimson_All_Model_Observer
{
    const XPATH_TOKENBASE_FAILED_ALERT_EMAIL = "payment_services/tokenbase/alert_email";
    const XPATH_TOKENBASE_FAILED_NOTIFY_CUSTOMER = "payment_services/tokenbase/alert_customer";
    //Event: adminhtml_controller_action_predispatch_start
    public function controllerFrontInitBefore(Varien_Event_Observer $observer)
    {
        include_once('Crimson/All/Functions.php');
    }

    public function overrideTheme()
    {
        Mage::getDesign()->setArea('adminhtml')
            ->setTheme((string)Mage::getStoreConfig('design/admin/theme'));
    }


    public function sendNotifications($observer){
        $profile = $observer->getEvent()->getProfile();
        $this->sendAlertEmail($observer);
        if($profile->getOrderInfo()['customer_email'] && Mage::getStoreConfig(self::XPATH_TOKENBASE_FAILED_NOTIFY_CUSTOMER)){
            $this->notifyCustomer($profile);

        }
        return true;

    }

    private function sendAlertEmail($observer)
    {
        /** @var Mage_Core_Model_Email_Template $mailTemplate */
        $profile = $observer->getEvent()->getProfile();
        $reason = $observer->getEvent()->getReason();
        $mailTemplate = Mage::getModel('core/email_template');
        $recipients = explode(',', Mage::getStoreConfig(self::XPATH_TOKENBASE_FAILED_ALERT_EMAIL));
        $data = $profile;
        foreach ($recipients as $recipient) {
            $mailTemplate->sendTransactional(
                'auth_tokenbase_failed_admin',
                'general',
                $recipient,
                Mage::getStoreConfig('trans_email/inotificdent_general/name'),
                array(
                    'data' => $data,
                    'reason' => $reason,
                    'order_info' => new Varien_Object($profile->getOrderInfo()),
                    'phone' =>  Mage::getModel('customer/customer')->load($profile->getOrderInfo()['customer_id'])->getTelephone()
                )
            );
        }
    }

    private function notifyCustomer($profile)
    {
        /** @var Mage_Core_Model_Email_Template $mailTemplate */
        $mailTemplate = Mage::getModel('core/email_template');
        $recipient = $profile->getOrderInfo()['customer_email'];
        $data = $profile;
            $mailTemplate->sendTransactional(
                'auth_tokenbase_failed_customer',
                'custom1',
                $recipient,
                $profile->getOrderInfo()['customer_firstname'],
                array(
                    'data' => $data,
                    'order_info' => new Varien_Object($profile->getOrderInfo())
                )
            );
    }
}