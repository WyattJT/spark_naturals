<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Attributes.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Model_System_Config_Source_Attributes
 */
class Crimson_All_Model_System_Config_Source_Attributes
{
    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {
            $sortedArray = array();

            /** @var $_attributes Mage_Eav_Model_Resource_Entity_Attribute_Collection */
            $_attributes = Mage::getSingleton('eav/config')
                ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
                ->getAttributeCollection();

            /** @var $attribute Mage_Eav_Model_Entity_Attribute */
            $this->_options = array();
            foreach ($_attributes as $attribute) {
                $tmpArray = array(
                    'value' => $attribute->getAttributeCode(),
                    'label' => $attribute->getFrontend()->getLabel(),
                );

                $this->_options[] = $tmpArray;
            }

            ksort($sortedArray);

            foreach ($sortedArray as $tmpArray) {
                $this->_options[] = $tmpArray;
            }
        }

        return $this->_options;
    }
}
