<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Category.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Model_System_Config_Source_Category
 */
class Crimson_All_Model_System_Config_Source_Category
{
    public function toOptionArray(
        $addEmpty = true, $categoryIdFilter = null, $includeTreeOffsets = true, $includeId = true
    ) {
        $options = array();

        if ($addEmpty) {
            $options[] = array(
                'label' => Mage::helper('adminhtml')->__('-- Please Select a Category --'),
                'value' => ''
            );
        }

        foreach ($this->getCollection($categoryIdFilter) as $category) {
            $label = $category->getName();

            if ($includeTreeOffsets) {
                $label = str_repeat('...|', $category->getLevel()) . $label;
            }

            if ($includeId) {
                $label .= ' (Id: ' . $category->getId() . ')';
            }

            $options[] = array(
                'label' => $label,
                'value' => $category->getId()
            );
        }

        return $options;
    }

    protected function getCollection($categoryIdFilter = null)
    {

        /** @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
        $collection = Mage::getResourceModel('catalog/category_collection');
        $collection->addAttributeToSort('path');

        if ($categoryIdFilter) {
            $categoryId = Mage::getResourceSingleton('catalog/category')->verifyIds(
                array($categoryIdFilter)
            );

            if (!empty($categoryId)) {

                /** @var $category Mage_Catalog_Model_Category */
                $category = Mage::getModel('catalog/category')->load($categoryId[0]);

                $collection->addPathsFilter($category->getPath() . '/');
            }
        }

        $collection->addAttributeToSelect('name')
            ->load();

        return $collection;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray($categoryIdFilter = null)
    {
        $array = array();

        foreach ($this->getCollection($categoryIdFilter) as $category) {
            $array[$category->getId()] = $category->getName();
        };

        return $array;
    }
}