<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Config.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Model_Paypal_Config
 */
class Crimson_All_Model_Paypal_Config extends Mage_Paypal_Model_Config
{
    const EE_BN_CODE = 'Levementum_SI_MagentoEnt_PPS';
    const CE_BN_CODE = 'Levementum_SI_MagentoComm_PPS';

    /**
     * BN code getter
     * override method
     *
     * @param string $countryCode ISO 3166-1
     *
     * @return string
     */
    public function getBuildNotationCode($countryCode = null)
    {
        if (Mage::helper('crimson_all')->isMageEnterprise()) {
            $newBnCode = self::EE_BN_CODE;
        } else {
            $newBnCode = self::CE_BN_CODE;
        }

        /**
         * if you would like to retain the product and country code
         * E.g., Company_Test_EC_US
         */
        //        $bnCode = parent::getBuildNotationCode($countryCode);
        //        $newBnCode = str_replace('Varien_Cart',$newBnCode,$bnCode);
        return $newBnCode;
    }

    /**
     * @return bool
     */
    public function isMageEnterprise()
    {
        return Mage::getConfig()->getModuleConfig('Enterprise_Enterprise');
    }
}