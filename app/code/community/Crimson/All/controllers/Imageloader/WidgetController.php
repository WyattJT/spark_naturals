<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        WidgetController.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Imageloader_WidgetController
 */
class Crimson_All_Imageloader_WidgetController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Chooser Source action
     */
    public function chooserAction()
    {
        $uniqId        = $this->getRequest()->getParam('uniq_id');
        $massAction    = $this->getRequest()->getParam('use_massaction', false);
        $productTypeId = $this->getRequest()->getParam('product_type_id', null);

        $productsGrid = $this->getLayout()->createBlock(
            'crimson_all/imageloader_widget_chooser', '', array(
                'id'              => $uniqId,
                'use_massaction'  => $massAction,
                'product_type_id' => $productTypeId
            )
        );

        $html = $productsGrid->toHtml();

        $this->getResponse()->setBody($html);
    }
}
