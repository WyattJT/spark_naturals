<?php

/**
 * Data helper
 *
 * @namespace All
 * @package   Crimson_All
 * @author    Matheus Gontijo <mgontijo@crimsonagility.com>
 * @date      12/2/2014 7:07 PM
 */

/**
 * Class Crimson_All_Helper_Data
 */
class Crimson_All_Helper_Data extends Mage_Core_Helper_Abstract
{
    const SYSTEM_TEST_MODE_XPATH = 'crimson/general/system_test_mode';

    protected $_tables = array();

    /**
     * Determine if the system configuration for 'test mode' is enabled.
     *
     * @return bool
     */
    public function isInTestMode()
    {
        return (bool)Mage::getStoreConfig(self::SYSTEM_TEST_MODE_XPATH);
    }

    public function getAttributeOptions($code, $asDropdown = true)
    {
        $_options = array();

        /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $code);

        if ($attribute->usesSource()) {
            if ($asDropdown) {
                foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                    $_options[$option['value']] = $option['label'];
                }
            } else {
                $_options = $attribute->getSource()->getAllOptions(false);
            }
        }

        return $_options;
    }

    /**
     * return true if passed needle(s) is in handles.
     *  - If needle is array return true if ANY needle is present.
     *
     * @param array|string $needle
     *
     * @return bool
     */
    public function inCurrentHandles($needle)
    {
        $currentHandles = Mage::app()->getLayout()->getUpdate()->getHandles();

        if (is_array($needle)) {
            foreach ($needle as $subNeedle) {
                if (in_array($subNeedle, $currentHandles)) {
                    return true;
                }
            }
        } else {
            return in_array($needle, $currentHandles);
        }

        return false;
    }

    public function getTable($tableName)
    {
        if (!isset($this->_tables[$tableName])) {
            $this->_tables[$tableName] = Mage::getSingleton('core/resource')->getTableName(
                $tableName
            );
        }

        return $this->_tables[$tableName];
    }

    /**
     * Checks to see if the current user is a developer.
     * Developer is assigned by IP listed in dev/restrict/allow_ips
     *
     * @return bool
     */
    public function isDeveloper()
    {
        return (strstr(
            Mage::getStoreConfig('dev/restrict/allow_ips'),
            Mage::helper('core/http')->getRemoteAddr()
        )) ? true : false;
    }

    public function isCategoryPage()
    {
        return (bool)Mage::registry('current_category');
    }

    public function isProductPage()
    {
        return (Mage::registry('current_product') || Mage::registry('product'));
    }

    public function isAdmin()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }

        if (Mage::getDesign()->getArea() == 'adminhtml') {
            return true;
        }

        return false;
    }

    /**
     * @param $collection array|Mage_Core_Model_Resource_Db_Collection_Abstract
     *
     * @return array
     */
    public function outputCsvFromCollection($collection, $hasHeaders = true)
    {
        $isCollection = false;
        if ($collection instanceof Mage_Core_Model_Resource_Db_Collection_Abstract) {
            $isCollection = true;
        }

        $io = new Varien_Io_File();

        $path = Mage::getBaseDir('tmp');
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);

        //Capture headers
        if ($isCollection) {
            $_headers = array_keys($collection->getFirstItem()->getData());
        } else {
            $_headers = array_keys($collection);
        }

        if ($hasHeaders) {
            $io->streamWriteCsv($_headers);
        }

        //write all items
        foreach ($collection as $item) {
            if ($isCollection) {
                $io->streamWriteCsv($item->getData());
            } else {
                $io->streamWriteCsv($item);
            }
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => false
            // can delete file after use
        );
    }

    public function sendDeveloperEmail(
        $subject, $message, $filepath = null, $filename = null, $developerEmails = null
    ) {
        if (!$developerEmails
            && !($developerEmails = Mage::getStoreConfig(
                'crimson/general/developer_email_addresses'
            ))
        ) {
            return $this;
        }

        try {
            //process and send email with exception csv
            /** @var $mail Mage_Core_Model_Email_Template */
            $mail = Mage::getModel('core/email_template');
            $mail->setReplyTo(Mage::getStoreConfig('trans_email/ident_general/email'))
                ->setDesignConfig(
                    array(
                        'area'  => 'frontend',
                        'store' => 0
                    )
                );
            $mail->getMail()->addTo($developerEmails);
            $mail->getMail()->setSubject($subject);
            $mail->getMail()->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'));

            if (is_array($message) || is_object($message)) {
                $message = print_r($message, true);
            }
            $mail->getMail()->setBodyHtml($message);

            if ($filepath && file_exists($filepath)) {
                $fileContents = file_get_contents($filepath);

                if (!$filename) {
                    $filename = basename($filepath);
                }

                $mail->getMail()->createAttachment($fileContents)->filename = $filename;
            }

            $mail->getMail()->send();
        } catch (Exception $e) {
            //do nothing, just log it and move on.
            Mage::log($e);
        }

        return $this;
    }

    /**
     * @param        $message
     * @param string $type
     * @param bool   $notifyDeveloper - notify developer of log (if debug mode is on)
     * @param bool   $force           - force logging to occur regardless of debug settings
     * @param string $filename
     *
     * @return $this
     */
    public function log(
        $message, $type = 'General', $notifyDeveloper = false, $force = false,
        $filename = 'crimson.log'
    ) {
        if ($this->isInTestMode() || $force) {
            Mage::log($type, null, $filename, true);
            Mage::log($message, null, $filename, true);

            if ($notifyDeveloper) {
                $this->sendDeveloperEmail('Dev Log Notification: ' . $type, $message);
            }
        }

        return $this;
    }

    /**
     * try to extract id and entity type.
     *
     * @param $idPath
     *
     * @return bool|array
     */
    public function extractIdPathData($idPath)
    {
        if (strpos($idPath, '/') === false) {
            return false;
        }

        $idPath     = explode('/', $idPath);
        $entityType = array_shift($idPath);
        $entityId   = array_shift($idPath);

        return array(
            'entity_type' => $entityType,
            'entity_id'   => $entityId
        );
    }

    /**
     * Build base message with commonly requested information from Exception, useful for logging.
     *
     * @param Exception $e
     *
     * @return string
     */
    public function buildExceptionMessage(Exception $e)
    {
        $message = 'Message: ' . $e->getMessage() . '<br/>' . PHP_EOL;
        $message .= 'File: ' . $e->getFile() . '<br/>' . PHP_EOL;
        $message .= 'Line: ' . $e->getLine() . '<br/>' . PHP_EOL . '<br/>' . PHP_EOL;
        $message .= $e->getTraceAsString();

        return $message;
    }

    /**
     * @return bool
     */
    public function isMageEnterprise()
    {
        return Mage::getConfig()->getModuleConfig('Enterprise_Enterprise');
    }

    /**
     * @param           $string
     * @param Zend_Date $storeDate
     *
     * @return Zend_Date
     *
     * @example:
     * $storeDate = Mage::app()->getLocale()->date();
     * $dateString = $storeDate->toString(Varien_Date::DATE_INTERNAL_FORMAT . ' 05:00:00');
     * $storeOpenGmtDate = Mage::helper('crimson_all')
     *                              ->convertTimestringToGmtDate($dateString, $storeDate);
     */
    public function convertTimestringToGmtDate($string, Zend_Date $storeDate = null)
    {
        if (is_null($storeDate)) {
            $storeDate = Mage::app()->getLocale()->date();
        }

        $storeOpenDate = Mage::app()->getLocale()->date(
            $string, Varien_Date::DATETIME_INTERNAL_FORMAT
        );
        $storeOpenDate->addSecond($storeDate->getGmtOffset());
        $storeOpenGmtDate = clone $storeOpenDate;
        $storeOpenGmtDate->addSecond($storeDate->getGmtOffset());

        return $storeOpenGmtDate;
    }
}