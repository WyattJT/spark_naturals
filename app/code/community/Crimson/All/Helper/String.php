<?php

/**
 * @namespace   Crimson
 * @module      All
 * @file        String.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */
class Crimson_All_Helper_String extends Mage_Core_Helper_Abstract
{
    const REDACT_CHARACTER = '*';

    /**
     * This removes any extraneous spaces surrounding commas.
     * (e.g.) "this,is, a , list ,you,like?," becomes "this,is,a,list,you,like?"
     *
     * @param      $list
     * @param bool $returnArray
     *
     * @return array|string
     */
    public function prepareCommaList(&$list, $returnArray = false)
    {
        $list = trim(preg_replace('/\ ?,\ ?/', ',', $list), ' ,');

        if ($returnArray) {
            if (strpos($list, ',') === false) {
                $list = array($list);
            } else {
                $list = explode(',', $list);
            }
        }

        return $list;
    }

    /**
     * Redact a 15 or 16 digit credit card number.
     * The first 6 and last 4 digits are always safe to display.
     * First digit signifies system:
     *  - 4: Visa
     *  - 5: MasterCard
     *  - 6: Discover
     *
     * PCI DSS v2, section 3.3, https://www.pcisecuritystandards.org/documents/pci_dss_v2.pdf
     *
     * @param $cardNumber
     *
     * @return string
     */
    public function redactCreditCard($cardNumber)
    {
        try {
            $cardLength = strlen($cardNumber);

            //if it's less than 15 digits, it isn't a credit card
            if ($cardLength < 15) {
                return '';
            }

            return substr($cardNumber, 0, 6) . $this->redactValue(
                substr($cardNumber, 6, $cardLength - 6 - 4)
            ) . substr($cardNumber, $cardLength - 4);
        } catch (Exception $e) {
            //PCI-DSS Vulnerability possible, do not log error.
            return '';
        }
    }

    /**
     * @param            $value
     * @param bool|int   $showLastDigitCount - if false, full redaction.
     *
     * @return string
     */
    public function redactValue($value, $showLastDigitCount = false)
    {
        if (!$showLastDigitCount) {
            return str_repeat(self::REDACT_CHARACTER, strlen($value));
        } else {
            return str_repeat(self::REDACT_CHARACTER, max(0, strlen($value) - $showLastDigitCount)) . substr(
                $value, -$showLastDigitCount
            );
        }
    }

    /**
     * Extract the domain from a passed email address.
     * e.g. "icoast@crimsonagility.com" returns "crimsonagility.com"
     * Properly validates the passed email address.
     *
     * @param string $email
     *
     * @return bool|string
     */
    public function extractEmailDomain($email)
    {
        if (!Mage::helper('crimson_all/validate')->validateEmail($email)) {
            return false;
        }

        return substr($email, strpos($email, '@') + 1);
    }

    /**
     * version of sprintf for cases where named arguments are desired (php syntax)
     *
     * with sprintf: sprintf('second: %2$s ; first: %1$s', '1st', '2nd');
     *
     * with sprintfn: sprintfn('second: %second$s ; first: %first$s', array(
     *  'first' => '1st',
     *  'second'=> '2nd'
     * ));
     *
     * @param string $format sprintf format string, with any number of named arguments
     * @param array  $args   array of [ 'arg_name' => 'arg value', ... ] replacements to be made
     *
     * @return string|false result of sprintf call, or bool false on error
     */
    function sprintfn($format, array $args = array())
    {
        // map of argument names to their corresponding sprintf numeric argument value
        $arg_nums = array_slice(array_flip(array_keys(array(0 => 0) + $args)), 1);

        // find the next named argument. each search starts at the end of the previous replacement.
        for (
            $pos = 0;
            preg_match('/(?<=%)([a-zA-Z_]\w*)(?=\$)/', $format, $match, PREG_OFFSET_CAPTURE, $pos);
        ) {
            $arg_pos = $match[0][1];
            $arg_len = strlen($match[0][0]);
            $arg_key = $match[1][0];

            // programmer did not supply a value for the named argument found in the format string
            if (!array_key_exists($arg_key, $arg_nums)) {
                user_error("sprintfn(): Missing argument '${arg_key}'", E_USER_WARNING);

                return false;
            }

            // replace the named argument with the corresponding numeric one
            $format = substr_replace($format, $replace = $arg_nums[$arg_key], $arg_pos, $arg_len);
            $pos    = $arg_pos + strlen($replace); // skip to end of replacement for next iteration
        }

        return vsprintf($format, array_values($args));
    }
}