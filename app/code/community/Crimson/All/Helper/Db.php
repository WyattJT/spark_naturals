<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Db.php
 * @author      icoast@crimsonagility.com
 * @date        6/3/2015 6:39 PM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Helper_Db
 */
class Crimson_All_Helper_Db extends Mage_Core_Helper_Abstract
{
    /**
     * Remove duplicate rows in preparation for unique key.
     *
     * @param      $table
     * @param      $duplicateFieldKey
     * @param null $idField
     *
     * @throws Zend_Db_Statement_Exception
     *
     * @example
     *  $table = 'shipusa_shipboxes';
     *  $duplicateFieldKey = array('sku', 'length', 'width', 'height', 'weight', 'declared_value',
     * 'quantity', 'num_boxes');
     *
     *  removeDuplicatesFromTable($table, $duplicateFieldKey);
     */
    public function removeDuplicatesFromTable($table, $duplicateFieldKey, $idField = null)
    {
        /** @var Varien_Db_Adapter_Pdo_Mysql $connection */
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');

        if (!$idField) {

            $indexes = $connection->getIndexList($table);

            //we can't identify id field
            if (!isset($indexes['PRIMARY'])) {
                return;
            } else {
                $idField = $indexes['PRIMARY']['fields'][0];
            }
        }

        $select = $connection->select()
            ->from(
                $table,
                array(
                    'ids' => new Zend_Db_Expr('GROUP_CONCAT(' . $idField . ')')
                )
            )
            ->group($duplicateFieldKey)
            ->having(new Zend_Db_Expr('COUNT(*) > 1'));

        $query = $connection->query($select);
        while ($row = $query->fetch()) {
            $ids   = explode(',', $row['ids']);
            $count = count($ids);
            $sql   = sprintf(
                'DELETE FROM `%s` WHERE %s LIMIT %d',
                $table,
                $connection->quoteInto($idField . ' IN (?)', $ids),
                $count - 1
            );
            $connection->raw_query($sql);
        }
    }
}