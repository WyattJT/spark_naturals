<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Io.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Helper_Io
 */
class Crimson_All_Helper_Io extends Mage_Core_Helper_Abstract
{
    /**
     * Loop through each file inside of the import folder and process as necessary and archive into the processed folder.
     *  If debug mode is enabled, do not move the file so we can allow repeated iterations of the file.
     */
    /**
     * Loop through each file in passed directory.  Return array of files, with full path.
     */
    public function ls($directory)
    {
        $directory = $this->verifySlashes($directory, true);
        $files     = array();

        if (is_dir($directory)) {
            if ($dh = opendir($directory)) {
                while (($_currentFile = readdir($dh)) !== false) {
                    /**
                     * Skip processing if $file is a directory.
                     */
                    if (is_dir($directory . $_currentFile) || $_currentFile == "."
                        || $_currentFile == ".."
                    ) {
                        continue;
                    }

                    $files[] = array(
                        'filename' => $_currentFile,
                        'full_path' => $directory . $_currentFile,
                        'directory' => $directory
                    );
                }

                closedir($dh);
            }
        }

        return $files;
    }

    /**
     * Check to see if _processedFolder is a real folder, if not create it.
     *
     * @param string $folder
     *
     * @return bool
     */
    public function checkFolder($folder = '')
    {
        if (empty($folder)) {
            return false;
        }

        /**
         * If it is not a directory try to make it.
         */
        if (!is_dir($folder)) {
            if (!mkdir($folder, 0775, true)) {
                return false;
            }
        }

        /**
         * If it is a folder, see if it is writable, if not try to make it so.
         */
        if (!is_dir_writeable($folder)) {
            try {
                if (!chmod($folder, 0775)) {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param        $url
     * @param        $fileName
     * @param string $directory
     *
     * @return bool - return true on success, false on failure.
     */
    public function downloadFile($url, $fileName, $directory)
    {
        try {

            $ch = curl_init($url);

            if (!$this->checkFolder($directory)) {
                return false;
            }

            $fullPath = $this->verifySlashes($directory, true) . $fileName;

            $fp = fopen($fullPath, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);

            return true;
        } catch (Exception $e) {
            Mage::log($e);

            return false;
        }
    }

    /**
     * Verify it has leading and trailing slashes.
     *
     * @param string $folder
     *
     * @return string
     */
    public function verifySlashes($folder = '', $trailingOnly = false)
    {
        if (empty($folder)) {
            $folder = '/';

            return $folder;
        }

        if (!$trailingOnly && substr($folder, 0, 1) != '/') {
            $folder = '/' . $folder;
        }

        if (substr($folder, strlen($folder) - 1, 1) != '/') {
            $folder .= '/';
        }

        return $folder;
    }
}