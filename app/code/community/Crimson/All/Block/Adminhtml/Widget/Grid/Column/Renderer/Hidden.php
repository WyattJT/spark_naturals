<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Hidden.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Hidden
 */
class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Hidden
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     *
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());

        $id = Mage::helper('core')->uniqHash($this->getColumn()->getIndex() . '_');

        $fieldNameTemplate = $this->_getFieldNameTemplate($row);
        $name              = sprintf($fieldNameTemplate, $this->_getId($row));

        $html = '<input type="hidden" ';
        $html .= 'id="' . ($id) . '"';
        $html .= 'name="' . $name . '"';
        $html .= 'value="' . $value . '" ' . ($this->getDisabled() ? 'disabled' : '') . '/>';

        return $html;
    }

    protected function _getId($row)
    {
        return ($this->getColumn()->getAlternateIdField() ? $row->getData(
            $this->getColumn()->getAlternateIdField()
        ) : $row->getId());
    }

    protected function _getFieldNameTemplate($row)
    {
        return ($this->getColumn()->getFieldNameTemplate() ? $this->getColumn()
            ->getFieldNameTemplate() : '%1$s[%2$s]');
    }
}