<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Block.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Checkbox
 */
class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Checkbox
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Checkbox
{
    protected $_defaultWidth = 55;
    protected $_values;

    /**
     * Returns values of the column
     *
     * @return array
     */
    public function getValues()
    {
        if (is_null($this->_values)) {
            $this->_values = $this->getColumn()->getData('values') ? $this->getColumn()->getData(
                'values'
            ) : array();
        }

        return $this->_values;
    }

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     *
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $this->setData('row', $row);

        return parent::render($row);
    }

    /**
     * @param string $value Value of the element
     * @param bool   $checked Whether it is checked
     *
     * @return string
     */
    protected function _getCheckboxHtml($value, $checked)
    {
        $id = Mage::helper('core')->uniqHash($this->getColumn()->getIndex() . '_');

        $fieldNameTemplate = $this->_getFieldNameTemplate($this->getData('row'));
        $name              = sprintf($fieldNameTemplate, $this->_getId($this->getData('row')));

        $html = '<input type="checkbox" ';
        $html .= 'id="' . $id . '" ';
        $html .= 'name="' . $name . '" ';
        $html .= 'value="' . ($this->getColumn()->getValue() ? $this->getColumn()->getValue()
                : $this->escapeHtml($value)) . '" ';
        $html .= 'class="checkbox ' . $this->getColumn()->getInputClasses() . '"';
        $html .= $checked . ($this->getDisabled() ? 'disabled' : '') . '/>';

        return $html;
    }

    /**
     * @return bool
     */
    protected function getDisabled()
    {
        return ($this->_getData('disabled') || $this->getColumn()->getDisabled());
    }

    protected function _getId($row)
    {
        return ($this->getColumn()->getAlternateIdField() ? $row->getData(
            $this->getColumn()->getAlternateIdField()
        ) : $row->getId());
    }

    protected function _getFieldNameTemplate($row)
    {
        return ($this->getColumn()->getFieldNameTemplate() ? $this->getColumn()
            ->getFieldNameTemplate() : '%1$s[%2$s]');
    }
}