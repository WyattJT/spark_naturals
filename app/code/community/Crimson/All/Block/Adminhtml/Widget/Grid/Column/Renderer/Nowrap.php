<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Nowrap.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Nowrap
 */
class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Nowrap
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     *
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $line = parent::_getValue($row);

        return '<span class="nobr">' . $line . '</span>' . print_r($row->getData(), true);
    }
}