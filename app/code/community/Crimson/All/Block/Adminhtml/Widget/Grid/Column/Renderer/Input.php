<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Input.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Input
 */
class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Input
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Input
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     *
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());

        if ($applyFunction = $this->getColumn()->getApplyFunction()) {
            $value = call_user_func($applyFunction, $value);
        }

        $id = Mage::helper('core')->uniqHash($this->getColumn()->getIndex() . '_');

        $fieldNameTemplate = $this->_getFieldNameTemplate($row);
        $name              = sprintf($fieldNameTemplate, $this->_getId($row));

        $html = '<input type="text" ';
        $html .= 'id="' . ($id) . '"';
        $html .= 'name="' . $name . '"';
        $html .= 'value="' . $value . '"';
        $html .= 'class="input-text ' . $this->getColumn()->getInputClasses() . '" ';
        $html .= ($this->getDisabled() ? 'disabled' : '');
        $html .= ($this->getColumn()->getReadonly() ? 'readonly' : '');
        $html .= '/>';

        return $html;
    }

    protected function _getId($row)
    {
        return ($this->getColumn()->getAlternateIdField() ? $row->getData(
            $this->getColumn()->getAlternateIdField()
        ) : $row->getId());
    }

    protected function _getFieldNameTemplate($row)
    {
        return ($this->getColumn()->getFieldNameTemplate() ? $this->getColumn()
            ->getFieldNameTemplate() : '%1$s[%2$s]');
    }

    /**
     * @return bool
     */
    protected function getDisabled()
    {
        return ($this->_getData('disabled') || $this->getColumn()->getDisabled());
    }
}