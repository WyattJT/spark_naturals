<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Select.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Select
 */
class Crimson_All_Block_Adminhtml_Widget_Grid_Column_Renderer_Select
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Select
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     *
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $id = Mage::helper('core')->uniqHash($this->getColumn()->getIndex() . '_');

        $fieldNameTemplate = $this->_getFieldNameTemplate($row);
        $name              = sprintf($fieldNameTemplate, $this->_getId($row));

        $value = $row->getData($this->getColumn()->getIndex());

        $readonly = '';
        if ($this->getColumn()->getReadonlyValues()
            && in_array(
                $value, $this->getColumn()->getReadonlyValues()
            )
        ) {
            $readonly = ' disabled="true"';
        }

        $optionTemplate = '<option value="%1$s" %3$s %4$s>%2$s</option>';
        $html           = '<select id="' . $id . '" name="' . $this->escapeHtml($name) . '" class="'
            . $this->getColumn()->getInputClasses() . '"' . $readonly . ' ' . ($this->getDisabled()
                ? 'disabled' : '') . '>';
        foreach ($this->getColumn()->getOptions() as $val => $label) {
            $selected = '';
            if ($val == $value && (!is_null($value))) {
                $selected = ' selected="selected"';
            }

            $disabled = '';
            if ($this->getColumn()->getDisabledValues()
                && in_array(
                    $val, $this->getColumn()->getDisabledValues()
                )
            ) {
                $disabled = ' disabled="true"';
            }

            $html .= sprintf(
                $optionTemplate, $this->escapeHtml($val), $this->escapeHtml($label), $selected,
                $disabled
            );
        }
        $html .= '</select>';

        if ($this->getColumn()->getReadonlyValues()
            && in_array(
                $value, $this->getColumn()->getReadonlyValues()
            )
        ) {
            $html .= '<input type="hidden" ';
            $html .= 'id="' . $id . '_hidden"';
            $html .= 'name="' . $this->escapeHtml($name) . '"';
            $html .= 'value="' . $value . '"/>';
        }

        return $html;
    }

    protected function _getId($row)
    {
        return ($this->getColumn()->getAlternateIdField() ? $row->getData(
            $this->getColumn()->getAlternateIdField()
        ) : $row->getId());
    }

    protected function _getFieldNameTemplate($row)
    {
        return ($this->getColumn()->getFieldNameTemplate() ? $this->getColumn()
            ->getFieldNameTemplate() : '%1$s[%2$s]');
    }
}