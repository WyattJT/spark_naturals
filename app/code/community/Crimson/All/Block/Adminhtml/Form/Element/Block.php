<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Block.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Example Usage:
 * $fieldset = $form->addFieldset('fieldset_id', array(
 * 'legend' => Mage::helper('adminhtml')->__('Fieldset Title')
 * ));
 *
 * //Block you wish to add in the form.
 * $block = $this->getLayout()->createBlock('core/template');
 *
 * $element = new Crimson_All_Block_Adminhtml_Form_Element_Block();
 * $element->setId('block_id');
 * $element->setBlock($block);
 *
 * $fieldset->addElement($element);
 */
class Crimson_All_Block_Adminhtml_Form_Element_Block extends Varien_Data_Form_Element_Abstract
{
    protected $_block;

    /**
     * Init Element
     *
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->setType('block');
    }

    /**
     * Retrieve HTML
     *
     * @return string
     */
    public function getElementHtml()
    {
        return $this->getBlock();
    }

    public function setBlock($block)
    {
        $this->_block = $block;

        return $this;
    }

    public function getBlock()
    {
        if (!$this->_block) {
            return '';
        }

        return $this->_block->toHtml();
    }
}
