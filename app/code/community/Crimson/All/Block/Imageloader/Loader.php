<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Loader.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Imageloader_Loader
 */
class Crimson_All_Block_Imageloader_Loader
    extends Mage_Core_Block_Abstract
    implements Mage_Widget_Block_Interface
{

    public function _toHtml()
    {
        $sku = $this->getData('sku');
        $imageSelection = $this->getData('img');
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
        $width = $this->getData('width');
        $height = $this->getData('height');
        $altText = $this->getData('alt');
        $cssClass = $this->getData('class');

        if (!is_numeric($height)) {
            $height = null;
        }

        if (!is_numeric($width)) {
            $width = null;
        }

        $imageURL = (string)Mage::helper('catalog/image')->init($product, $imageSelection)->resize(
            $width, $height
        );

        return
            '<img src="' . $imageURL . '" alt="' . $altText . '" title="' . $altText . '" class="'
            . $cssClass . '">';
    }
}
