<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Chooser.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Imageloader_Widget_Chooser
 */
class Crimson_All_Block_Imageloader_Widget_Chooser
    extends Mage_Adminhtml_Block_Catalog_Product_Widget_Chooser
{

    public function getSelectedProducts()
    {
        if ($selectedProducts = $this->getRequest()->getParam('selected_products', null)) {
            $this->setSelectedProducts($selectedProducts);
        }

        return $this->_selectedProducts;
    }

    public function setSelectedProducts($selectedProducts)
    {
        $this->_selectedProducts = $selectedProducts;

        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl(
            '*/crimson/imageloader_widget/chooser', array(
                'products_grid'   => true,
                '_current'        => true,
                'uniq_id'         => $this->getId(),
                'use_massaction'  => $this->getUseMassaction(),
                'product_type_id' => $this->getProductTypeId()
            )
        );
    }

    protected function _prepareColumns()
    {
        if ($this->getUseMassaction()) {
            $this->addColumn(
                'in_products', array(
                    'header_css_class' => 'a-center',
                    'type'             => 'checkbox',
                    'name'             => 'in_products',
                    'inline_css'       => 'checkbox entities',
                    'field_name'       => 'in_products',
                    'values'           => $this->getSelectedProducts(),
                    'align'            => 'center',
                    'index'            => 'entity_id',
                    'use_index'        => true,
                )
            );
        }

        $this->addColumn(
            'entity_id', array(
                'header'   => Mage::helper('catalog')->__('ID'),
                'sortable' => true,
                'width'    => '60px',
                'index'    => 'entity_id'
            )
        );
        $this->addColumn(
            'chooser_sku', array(
                'header' => Mage::helper('catalog')->__('SKU'),
                'name'   => 'chooser_sku',
                'width'  => '80px',
                'index'  => 'sku'
            )
        );
        $this->addColumn(
            'chooser_name', array(
                'header' => Mage::helper('catalog')->__('Product Name'),
                'name'   => 'chooser_name',
                'index'  => 'name'
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowClickCallback()
    {

        if (!$this->getUseMassaction()) {
            $chooserJsObject = $this->getId();

            return '
                function (grid, event) {
                    var trElement = Event.findElement(event, "tr");
                    var productSku = trElement.down("td").next().innerHTML;
                    var productName = trElement.down("td").next().innerHTML;
                    var optionLabel = productName;
                    var optionValue = productSku.trim();

                    ' . $chooserJsObject . '.setElementValue(optionValue);
                    ' . $chooserJsObject . '.setElementLabel(optionLabel);
                    ' . $chooserJsObject . '.close();
                }
            ';
        }
    }

    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $uniqId    = Mage::helper('core')->uniqHash($element->getId());
        $sourceUrl = $this->getUrl(
            '*/imageloader_widget/chooser', array(
                'uniq_id'        => $uniqId,
                'use_massaction' => false,
            )
        );

        $chooser = $this->getLayout()->createBlock('widget/adminhtml_widget_chooser')
            ->setElement($element)
            ->setTranslationHelper($this->getTranslationHelper())
            ->setConfig($this->getConfig())
            ->setFieldsetId($this->getFieldsetId())
            ->setSourceUrl($sourceUrl)
            ->setUniqId($uniqId);

        if ($element->getValue()) {
            $value     = explode('/', $element->getValue());
            $productId = false;
            if (isset($value[0]) && isset($value[1]) && $value[0] == 'product') {
                $productId = $value[1];
            }
            $label = '';
            if ($productId) {
                $label .= Mage::getResourceSingleton('catalog/product')
                    ->getAttributeRawValue($productId, 'name', Mage::app()->getStore());
            }
            $chooser->setLabel($label);
        }

        $element->setData('after_element_html', $chooser->toHtml());

        return $element;
    }
}