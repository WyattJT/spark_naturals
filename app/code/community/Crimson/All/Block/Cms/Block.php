<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Block.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Cms_Block
 */
class Crimson_All_Block_Cms_Block extends Mage_Cms_Block_Block
{
    /**
     * Prepare Content HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $blockId = $this->getBlockId();
        $html    = '';
        if ($blockId) {
            $block = Mage::getModel('cms/block')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($blockId);
            if ($block->getIsActive()) {
                /* @var $helper Mage_Cms_Helper_Data */
                $helper = Mage::helper('cms');
                $processor = $helper->getBlockTemplateProcessor();
                $processor->setVariables(
                    array('isloggedin' => Mage::helper('customer')->isLoggedIn())
                );
                $html = $processor->filter($block->getContent());
                $this->addModelTags($block);
            }
        }

        return $html;
    }
}