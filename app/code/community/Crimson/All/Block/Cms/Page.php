<?php
/**
 * @category    Crimson
 * @package     All
 * @file        Page.php
 * @auther      jjuleff@crimsonagility.com
 * @date        8/21/14 12:44 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Cms_Page
 */
class Crimson_All_Block_Cms_Page extends Mage_Cms_Block_Page
{
    /**
     * Prepare HTML content
     *
     * @return string
     */
    protected function _toHtml()
    {
        /* @var $helper Mage_Cms_Helper_Data */
        $helper    = Mage::helper('cms');
        $processor = $helper->getPageTemplateProcessor();
        $variables = array('isloggedin' => (Mage::helper('customer')->isLoggedIn() ? 1 : 0));
        $processor->setVariables($variables);
        $html = $processor->filter($this->getPage()->getContent());
        $html = $this->getMessagesBlock()->toHtml() . $html;

        return $html;
    }
}