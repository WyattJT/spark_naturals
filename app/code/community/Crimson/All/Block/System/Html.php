<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Html.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_System_Html
 */
class Crimson_All_Block_System_Html extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $html    = '';
        $version = Mage::getConfig()->getNode('modules/Crimson_All/version');

        $modules = Mage::getConfig()->getNode('modules');

        $levModules = array();
        foreach ($modules->asArray() as $key => $valueArray) {
            if (strpos($key, 'Crimson') !== false && $key != 'Crimson_All') {
                if (isset($valueArray['version'])) {
                    $levModules[] = array(
                        'name'    => $key,
                        'version' => $valueArray['version']
                    );
                }
            }
        }

        $html .= "
            <div style=\" margin-bottom: 12px; width: 100%;\">
            <p>You are currently using the following Crimson Agility Modules:</p>
            <ul>
                <li>Core - v{$version}</li>";
        foreach ($levModules as $module) {
            $html .= "<li>{$module['name']} - v{$module['version']}</li>";
        }

        $html
            .= "
            </ul>
            <br/>
            <p>Check out  <a href='http://www.crimsonagility.com/' target='_blank'>Crimson Agility, LLC</a>";

        $html
            .= "
            </div>
		";

        return $html;
    }
}
