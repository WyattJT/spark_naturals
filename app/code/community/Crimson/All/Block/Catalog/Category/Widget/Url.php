<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Url.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Catalog_Category_Widget_Url
 */
class Crimson_All_Block_Catalog_Category_Widget_Url
    extends Crimson_All_Block_Catalog_Widget_Url
{
}
