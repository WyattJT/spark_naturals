<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Url.php
 * @author      icoast@crimsonagility.com
 * @date        12/6/2014 10:57 AM
 * @brief
 * @details
 */

/**
 * Class Crimson_All_Block_Catalog_Widget_Url
 */
class Crimson_All_Block_Catalog_Widget_Url
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    /**
     * Render block HTML
     * or return empty string if url can't be prepared
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->hasStoreId()) {
            $store = Mage::app()->getStore($this->getStoreId());
        } else {
            $store = Mage::app()->getStore();
        }

        /* @var $store Mage_Core_Model_Store */
        if ($this->getData('id_path')) {
            //try to extract category id.
            $idPathData = Mage::helper('crimson_all')->extractIdPathData($this->getData('id_path'));
            if (!$idPathData) {
                return '';
            }

            $entityId = $idPathData['entity_id'];
            $entityType = $idPathData['entity_type'];

            if ($entityType != 'product' && $entityType != 'category') {
                Mage::log(
                    get_class($this) . ', Line ' . __LINE__ . '.  Invalid entity type passed: "'
                    . $entityType . '".'
                );

                return '';
            } elseif (!is_numeric($entityId)) {
                Mage::log(
                    get_class($this) . ', Line ' . __LINE__ . '.  Non-numeric entity ID passed: "'
                    . $entityId . '".'
                );

                return '';
            }

            $entity = Mage::getModel('catalog/' . $entityType)->load($entityId);
            if ($entity->getId()
                && ($entity->getUrl() || $entity->getProductUrl()
                    || $entity->getCategoryUrl())
            ) {
                if ($entity->getUrl()) {
                    $url = $entity->getUrl();
                } elseif ($entity->getProductUrl()) {
                    $url = $entity->getProductUrl();
                } elseif ($entity->getCategoryUrl()) {
                    $url = $entity->getCategoryUrl();
                }
            } else {
                Mage::log(
                    get_class($this) . ', Line ' . __LINE__ . '.  Entity ID, ' . $entityId
                    . ' for entity type "' . $entityType . '" passed no longer exists.'
                );

                return '';
            }
        } else {
            Mage::log(get_class($this) . ', Line ' . __LINE__ . '.  No id_path defined.');

            return '';
        }

        if (strpos($url, "___store") === false) {
            $symbol = (strpos($url, "?") === false) ? "?" : "&";
            $url    = $url . $symbol . "___store=" . $store->getCode();
        }

        return $url;
    }
}
