<?php
/**
 * @namespace   Crimson
 * @module      All
 * @file        Functions.php
 * @author      icoast@crimsonagility.com
 * @date        8/11/14 4:58 PM
 * @brief       
 * @details     
 */

if (!function_exists('getMemoryUsage')) {
    function getMemoryUsage()
    {
        $unit = array(
            'b',
            'kb',
            'mb',
            'gb',
            'tb',
            'pb'
        );
        $size = memory_get_usage(true);

        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }
}

if (!function_exists('microtime_float')) {
    function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());

        return ((float) $usec + (float) $sec);
    }
}

